﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
using DataAccessLibrary.Parser;
using DataAccessLibrary.RepositoryConfiguration;

namespace DataAccessLibrary.DataAccess
{
    internal class NoaaWeatherRespository : WeatherRepository, IWeatherRepository
    {
        public NoaaWeatherRespository(Guid requestGuid):base(new NoaaWeatherRepositoryConfiguration(), 50, requestGuid){}
        
        private static readonly HashSet<string> MissingZipCodes = new HashSet<string>()
        {
         "01062","01772","02145","02458","02461","03442","03823","04276","05408","06027","06042","06061","06357","06461","08330","08502","08720","11223","11224",
         "11228","11379","11436","11701","17015","17202","17408","19060","19342","20111","21742","21746","21750","21757","21758","21769","21770","21771","21774",
         "21810","21841","22025","22655","25545","25557","28312","28759","29058","29406","29420","29423","29424","29431","29437","29440","29452","29466","29488",
         "29631","29707","30536","32081","33449","33545","33578","33596","33812","33966","33967","33973","33974","33976","34291","34638","34715","35652","35746",
         "35747","35749","35755","36035","36251","36421","37934","39189","39191","39192","39193","39201","39885","41663","42029","42459","43318","43320","43340",
         "43460","43464","44483","45416","45419","45426","46037","47336","47974","48033","48168","48193","48638","49037","49519","49534","49922","53548","53599",
         "53702","53703","55130","56127","60404","60428","60484","60487","60502","60503","60585","60642","60958","61931","62711","62712","65259","65262","65264",
         "65336","67843","68379","74019","75932","75954","76002","77407","77523","78542","80113","80924","80927","80938","80939","81403","83414","83646","84005",
         "84045","84081","84096","84124","84180","84302","84710","84713","85083","85118","85119","85120","85121","85122","85123","85128","85131","85132","85135",
         "85137","85138","85139","85140","85141","85142","85143","85145","85147","85172","85173","85192","85193","85194","85209","85286","85295","85298","85388",
         "85658","85755","85756","85757","86315","89002","89044","89054","89081","89085","89086","89161","89166","89169","89178","89179","89183","89441","90090",
         "92011","92617","92637","93314","93619","93723","93730","94158","95467","95757","95811","97086","97317","97471","98077","99354"
        }; 

        protected override void GenerateUrls()
        {
            int endIndex = Math.Min(maxLocationsInQuery, locations.Count - 1);
            int startIndex  = 0;
            HashSet<string> uniqueLocationsByZip = new HashSet<string>();
            do
            {
                var query = GenerateQuery(startIndex, endIndex, uniqueLocationsByZip);
                urls.Add(repositoryConfig.GetConnectionString() + query);
                //advance the start and the end index
                startIndex += endIndex + 1;
                //advance the end index by 200 or take the locations count select which ever one is the lowest
                endIndex = Math.Min((endIndex + maxLocationsInQuery), locations.Count - 1);

            } while (startIndex < locations.Count);
        }

        protected string GenerateQuery(int startIndex,
                                       int endIndex,
                                       HashSet<string> uniqueLocations)
        {
            StringBuilder finalQuery = new StringBuilder();
            var locationsPresentInQuery = new List<LocationDataObject>();

            //loop through all the cities between start and end index and generate the query
            for (; startIndex <= endIndex; startIndex++)
            {
                if (this.locations[startIndex] == null)
                {
                    this.logger.Warn("Location is null. Skipping");
                    continue;
                }

                if (string.IsNullOrWhiteSpace(this.locations[startIndex].Zipcode))
                {
                    this.logger.Warn("Empty zipcode. Skipping");
                    continue;
                }

                if (string.IsNullOrWhiteSpace(this.locations[startIndex].Zipcode))
                {
                    this.logger.Warn("Not unique zip {0}. Skipping", this.locations[startIndex].Zipcode);
                    continue;
                }

                if (MissingZipCodes.Contains(this.locations[startIndex].Zipcode))
                {
                    this.logger.Warn("One among the missing zip {0}. Skipping", this.locations[startIndex].Zipcode);
                    continue;
                }

                var zip = locations[startIndex].Zipcode;
                finalQuery.Append(zip + "+");

                //add it to the hash set. This is so that we do not query same location twice
                uniqueLocations.Add(this.locations[startIndex].Zipcode);

                locationsPresentInQuery.Add(locations[startIndex]);
            }

            locationsPerQuery.Enqueue(locationsPresentInQuery);
            return System.Web.HttpUtility.UrlDecode(finalQuery.ToString());
        }

        protected override void HandleResponse(Task<Stream>[] httpTasks)
        {
            bool firstResponse = true;

            for (int i = 0 ; i < httpTasks.Length; i++)
            {
                var httpResponse = httpTasks[i];
                var url = urls[i];
                if (!httpResponse.IsCompleted)
                {
                    //TODO log 
                    continue;
                }

                //now loop through each of the responses and parse it
                IWeatherParser parser = new NoaaWeatherParser(httpResponse.Result, locationsPerQuery.Dequeue(),url, this.logger.RequestGuid);

                var parsedWeatherForLocations = parser.ParseWeatherXml();
                if (!firstResponse)
                {
                    //need to merge it with the previous list
                    foreach (var weatherLocation in parsedWeatherForLocations)
                    {
                        //an exception may occur if the key already exists. But we have taken care of this while building the list of locations.
                        //so this case will never occur
                        parsedResult.Add(weatherLocation.Key, weatherLocation.Value);
                    }
                }
                else
                {
                    //if there is only one response then why do the heavy lifting
                    parsedResult = parsedWeatherForLocations;
                    firstResponse = false;
                }
            }
        }

        public async Task<Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>>> Fetch(List<LocationDataObject> locationObjs)
        {
            locations = locationObjs;
            GenerateUrls();

            if (urls.Count == 0)
            {
                //todo something went wrong. log and return 
                return null;
            }
            await PerformHttpTask();
            return parsedResult;
        }
    }
}
