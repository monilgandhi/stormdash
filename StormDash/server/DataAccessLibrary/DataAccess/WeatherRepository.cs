﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;

namespace DataAccessLibrary.DataAccess
{
    internal abstract class WeatherRepository
    {
        protected readonly IRepositoryConfiguration repositoryConfig;
        private HttpRequestClient httpRequestClient;
        protected readonly UInt16 maxLocationsInQuery = 1;
        protected List<LocationDataObject> locations;
        protected ILogger logger;
        /// <summary>
        /// This list is for mapping urls list with each map location. We can only send 200 map locations at once.
        /// When we recieve the response we know which response belongs to which location object
        /// NOTE: Assuming response has the same order of points.
        /// </summary>
        protected Queue<List<LocationDataObject>> locationsPerQuery;

        protected Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>> parsedResult;
        /// <summary>
        /// Create url list based on WeatherDataObjects. each url can only have 200 locations (NOAA limit)
        /// </summary>
        protected List<String> urls;
        protected abstract void GenerateUrls();
        protected abstract void HandleResponse(Task<Stream>[] httpTasks);

        protected WeatherRepository(IRepositoryConfiguration repoConfig, Guid requestGuid)
        {
            if(repoConfig == null)
            {
                throw new ArgumentNullException("repoConfig");
            }
            this.httpRequestClient = new HttpRequestClient();
            repositoryConfig = repoConfig;
            urls = new List<string>();
            locationsPerQuery = new Queue<List<LocationDataObject>>();
            logger = LogWrapper.GetLogger(this.GetType(), requestGuid);
        }

        protected WeatherRepository(IRepositoryConfiguration repoConfig, UInt16 maxLocationsInQuery, Guid requestGuid) :
            this(repoConfig, requestGuid)
        {
            if(maxLocationsInQuery == 0)
            {
                throw new ArgumentException("Maximum locations in query needs to be atleast 1");
            }
            this.maxLocationsInQuery = maxLocationsInQuery;
        }

        protected async Task PerformHttpTask()
        {
            var httpTasks = new Task<Stream>[urls.Count];
            var index = 0;
            var allUrlsSw = Stopwatch.StartNew();
            foreach (var url in urls)
            {
                if (String.IsNullOrEmpty(url))
                {
                    //log an empty url
                    continue;
                }
                var task = httpRequestClient.GetResponse(url);

                this.logger.Performance("Fetching url {0}", url);
                httpTasks[index++] = task;
            }

            await Task.WhenAll(httpTasks);
            logger.Performance("Time taken for fetching {0} urls is {1}", urls.Count, allUrlsSw.ElapsedMilliseconds);

            //initialize the results now
            parsedResult = new Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>>();
            HandleResponse(httpTasks);
        }
    }
}
