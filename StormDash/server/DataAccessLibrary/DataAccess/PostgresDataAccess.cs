﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.Interface;
using Npgsql;

namespace DataAccessLibrary.DataAccess
{
    internal class PostgresDataAccess
    {
        protected IRepositoryConfiguration dbConfig;
        protected ILogger logger;
        

        protected PostgresDataAccess(IRepositoryConfiguration dbConfig, Guid requestGuid)
        {
            if(dbConfig == null)
            {
                throw new ArgumentNullException("dbConfig");
            }

            this.dbConfig = dbConfig;
            this.logger = LogWrapper.GetLogger(this.GetType(), requestGuid);
        }
        
        protected string ConnectionString
        {
            get 
            {
                if(this.dbConfig == null)
                {
                    throw new NullReferenceException("dbConfig is not defined");
                }
                return this.dbConfig.GetConnectionString(); 
            }
        }


        protected async Task<DbDataReader> ExecuteReadAsync(NpgsqlCommand command, NpgsqlConnection connection)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            command.Connection = connection;
            DbDataReader reader = await command.ExecuteReaderAsync();
            return reader;

        }
    }
}