﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
using DataAccessLibrary.RepositoryConfiguration;
using StackExchange.Redis;

namespace DataAccessLibrary.DataAccess
{
    internal class RedisCacheRepository : ICacheRepository
    {
        private IRepositoryConfiguration repositoryConfiguration;
        private static ConnectionMultiplexer client;
        private ILogger logger;

        public RedisCacheRepository(Guid requestGuid)
        {
            this.repositoryConfiguration = new RedisRepositoryConfiguration();
            this.logger = LogWrapper.GetLogger(GetType(), requestGuid);
        }
        private ConnectionMultiplexer Client
        {
            get
            {
                if (client == null)
                {
                    client = ConnectionMultiplexer.Connect(this.repositoryConfiguration.GetConnectionString());
                }
                return client;
            }
        }

        public async Task<CacheDataObject<T>> Get<T>(string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            var result = new CacheDataObject<T>();

            IDatabase redisDb = Client.GetDatabase(0);
            var redisByteArray = (byte[])await Task.Run(() => redisDb.StringGetAsync(key));

            if (redisByteArray == null || redisByteArray.Length == 0)
            {
                this.logger.Debug("Object not present key:{0}", key);
                return result;
            }

            var ms = new MemoryStream(redisByteArray);

            var binaryFormatter = new BinaryFormatter();
            result = (CacheDataObject<T>)binaryFormatter.Deserialize(ms);

            return result;

        }

        public async Task<bool> Set<T>(string key, CacheDataObject<T> obj, TimeSpan? timeOut = null)
        {
            bool success = false;
            if (String.IsNullOrEmpty(key) || obj == null)
            {
                throw new ArgumentNullException("key");
            }

            var binaryFormatter = new BinaryFormatter();
            var ms = new MemoryStream();
            binaryFormatter.Serialize(ms, obj);

            if (!ms.CanRead || ms.Length == 0)
            {
                this.logger.Error("Unable to read the stream Key:{0}", key);
                return false;
            }

            IDatabase redisDb = Client.GetDatabase(0);
            success = await redisDb.StringSetAsync(key, ms.ToArray(), timeOut);
            
            if(success)
            {
                this.logger.Debug("Save Successful key:{0}", key);
            }
            else
            {
                this.logger.Debug("Save unsuccessful key:{0}", key);
            }
            return success;
        }
    }
}
