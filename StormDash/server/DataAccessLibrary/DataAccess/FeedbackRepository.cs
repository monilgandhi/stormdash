﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using WeatherApp.Communication;

namespace DataAccessLibrary.DataAccess
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private static CloudTable tableClient = InitializeTableClient();

        private readonly ILogger logger;
        public FeedbackRepository()
        {
            this.logger = LogWrapper.GetLogger(this.GetType(), Guid.NewGuid());
        }

        public async Task<bool> PostFeedbackAsync(FeedbackPostRequest feedback)
        {
            if (string.IsNullOrWhiteSpace(feedback?.Feedback))
            {
                throw new ArgumentNullException(nameof(feedback));
            }

            if (tableClient == null)
            {
                this.logger.Error("Table Client is null");
                return false;
            }

            var partitionKey = string.IsNullOrWhiteSpace(feedback.FeedbackEmail) ? "anonymous" : feedback.FeedbackEmail;
            var feedBackEntity = new FeedbackTableEntity(partitionKey, Guid.NewGuid().ToString(), feedback.Feedback);
            var operation = TableOperation.Insert(feedBackEntity);

            try
            {
                await tableClient.ExecuteAsync(operation);
            }
            catch (StorageException stex)
            {
                this.logger.Error("Exception while posting feedback: {0}, {1}", feedback.Feedback, stex.Message);
                return false;
            }

            return true;
        }

        private static CloudTable InitializeTableClient()
        {
            if (tableClient != null)
            {
                return null;
            }

            var connectionsString = ConfigurationManager.ConnectionStrings["TableStorage"];

            CloudStorageAccount account = null;

            if (string.IsNullOrWhiteSpace(connectionsString?.ConnectionString) ||
                !CloudStorageAccount.TryParse(connectionsString.ConnectionString, out account))
            {
                return null;
            }
            var cloudTableClient = account.CreateCloudTableClient();
            return cloudTableClient.GetTableReference("stormdashfeedback");
        }
    }
}
