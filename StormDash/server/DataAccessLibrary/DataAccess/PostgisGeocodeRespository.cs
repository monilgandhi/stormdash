﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Exceptions;
using DataAccessLibrary.Interface;
using DataAccessLibrary.RepositoryConfiguration;
using Npgsql;
using NpgsqlTypes;

namespace DataAccessLibrary.DataAccess
{
    internal class PostgisGeocodeRespository : PostgresDataAccess,IGeocoderRepository
    {

        internal PostgisGeocodeRespository(Guid requestGuid) : base(new GeocoderRespositoryConfiguration(), requestGuid) {}

        private bool HandleNpgSqlExceptions(Exception x)
        {
            this.logger.Debug("Npgsql exception occurred Message: {0}",x.Message);
            return false;
        }

        public async Task<GeoCoordinateDataObject> GeocodeAsync(String address)
        {
            string query = @"SELECT ST_X(g.geomout) As longitude, ST_Y(g.geomout) As latitude
                             FROM geocode(:address) As g;";

            var addressParameter = new NpgsqlParameter("address", NpgsqlDbType.Varchar)
            {
                Value = address
            };

            var command = new NpgsqlCommand(query);
            command.Parameters.Add(addressParameter);

            //create the LocationDataAccess object
            GeoCoordinateDataObject obj = null;
            using (var connection = new NpgsqlConnection(this.ConnectionString))
            {
                try
                {
                    await connection.OpenAsync();
                    DbDataReader reader = await ExecuteReadAsync(command, connection);
                    if (reader == null)
                    {
                        throw new NullReferenceException("Reader is null");
                    }

                    if (reader.FieldCount == 0)
                    {
                        //nothing found
                        return null;
                    }

                    while (reader.Read())
                    {
                        float longitude = (float) Math.Round((double) reader["longitude"], 2);
                        float latitude = (float) Math.Round((double) reader["latitude"], 2);

                        obj = new GeoCoordinateDataObject(longitude, latitude);
                    }

                    reader.Close();
                    connection.Close();
                }
                finally
                {
                    connection.Close();
                }
            }
            return obj;
        }

        /// <summary>
        /// This function calls reverse geocoding queries as task. 
        /// This will return null in case if we could not reverse geocode a specific point. 
        /// Hence the number of items in result will be equal to number of coordinates
        /// </summary>
        /// <param name="coordinates">List of coordinates to reverse geocode</param>
        /// <returns>Returns list of repository result with result data as location data object</returns>
        public async Task<List<RepositoryResult<LocationDataObject>>> ReverseGeocodeAsync(List<GeoCoordinateDataObject> coordinates)
        {
            List<LocationDataObject>  reverseGeocodedPoints = null;
            try
            {
                reverseGeocodedPoints = await this.ReverseGeocodeArrayPointsAsync(coordinates);
            }
            catch (AggregateException ae)
            {
                this.logger.Error("Exception while Reverse Geocoding");
                //ae.Handle(HandleNpgSqlExceptions);
            }

            var resultList = new List<RepositoryResult<LocationDataObject>>();
            int count = -1;
            foreach(var point in reverseGeocodedPoints)
            {
                ++count;
                var result = new RepositoryResult<LocationDataObject>();
                if (point == null)
                {
                    this.logger.Debug( "Unable to retrieve point for lon,lat: {0},{1}", coordinates[count].Longitude, coordinates[count].Latitude);
                    result.Status = RepositoryResultEnum.Failure;
                    resultList.Add(result);
                    continue;
                }

                result.ResultData = point;
                resultList.Add(result);
            }

            return resultList;
        }

        private async Task<List<LocationDataObject>> ReverseGeocodeArrayPointsAsync(
            List<GeoCoordinateDataObject> coordinates)
        {
            var longitudeArrayParameter = new double[coordinates.Count];
            var latitudeArrayParameter = new double[coordinates.Count];

            for (int i = 0; i < coordinates.Count; i++)
            {
                longitudeArrayParameter[i] = coordinates[i].Longitude;
                latitudeArrayParameter[i] = coordinates[i].Latitude;
            }

            string query =
    "Select location as city, stateabbrev as state, zip as zipcode FROM unnest(creverse_geocode_array(:lat,:lon))";

            var latitudeParameter = new NpgsqlParameter("lat", NpgsqlDbType.Array | NpgsqlDbType.Double)
            {
                Value = latitudeArrayParameter
            };

            var longitudeParameter = new NpgsqlParameter("lon", NpgsqlDbType.Array | NpgsqlDbType.Double)
            {
                Value = longitudeArrayParameter
            };

            var command = new NpgsqlCommand(query);
            command.Parameters.Add(longitudeParameter);
            command.Parameters.Add(latitudeParameter);

            List<LocationDataObject> reverseGeocodedPoints = new List<LocationDataObject>();
            using (var connection = new NpgsqlConnection(this.ConnectionString))
            {
                try
                {
                    await connection.OpenAsync();
                    DbDataReader reader = await ExecuteReadAsync(command, connection);

                    int latlonIterator = 0;
                    while (reader.Read())
                    {
                        string city = null, state = null, zip = null;

                        GeoCoordinateDataObject coordinate = coordinates[latlonIterator];
                        if (reader["city"] == DBNull.Value &&
                            reader["state"] == DBNull.Value)
                        {
                            this.logger.Error("City and state not returned", coordinate.Longitude,
                                coordinate.Latitude);
                            this.logger.Debug("Failed Query {0}", query);
                            reverseGeocodedPoints.Add(null);
                            continue;
                        }

                        city = (string)reader["city"];
                        state = (string)reader["state"];
                        

                        if (reader["zipcode"] == DBNull.Value)
                        {
                            this.logger.Debug("Null zip for {0},{1}", coordinate.Longitude,
                                coordinate.Latitude);
                            reverseGeocodedPoints.Add(null);
                            continue;
                        }

                        zip = (String)reader["zipcode"];
                        
                        reverseGeocodedPoints.Add(new LocationDataObject(coordinate, city, state, zip));

                        //close the reader to avoid "Data reader open connection"
                        //Refer http://blogs.msdn.com/b/spike/archive/2009/08/20/there-is-already-an-open-datareader-associated-with-this-command-which-must-be-closed-first-explained.aspx
                    }
                }
                catch (Exception ex)
                {
                    this.logger.Error("Exception while reverse geocoding type: {0}, Message: {1} ", ex.GetType().ToString(), ex.Message);
                    return null;
                }
                finally
                {
                    connection.Close();
                }
            }

            return reverseGeocodedPoints;
        }

        /// <summary>
        /// Function that actually creates and sends the query to the db..
        /// Function does not catch any exceptions. It is the job of wrapper function to do this and log it as required
        /// NOTE: This function does not even open connection or close connection. Again it is the job of wrapper function.
        /// </summary>
        /// <param name="coordinate">Coordinate to be reverse geocoded</param>
        /// <returns>Location dataobject that is returned by the DB</returns>
        private async Task<LocationDataObject> ReverseGeocodeIndividualPointsAsync(GeoCoordinateDataObject coordinate)
        {
            string query = "Select location as city, stateabbrev as state, zip as zipcode FROM creverse_geocode(ST_Transform(ST_SetSRID(ST_MakePoint(:longitude,:latitude),4269),4269))";

            var longitudeParameter = new NpgsqlParameter("longitude", NpgsqlDbType.Double)
            {
                Value = coordinate.Longitude
            };

            var latitudeParameter = new NpgsqlParameter("latitude", NpgsqlDbType.Double)
            {
                Value = coordinate.Latitude
            };


            var command = new NpgsqlCommand(query);
            command.Parameters.Add(longitudeParameter);
            command.Parameters.Add(latitudeParameter);

            //create the LocationDataAccess object
            LocationDataObject obj = null;
            string city = null;
            string state = null;
            string zip = null;

            using (var connection = new NpgsqlConnection(this.ConnectionString))
            {
                try
                {
                    await connection.OpenAsync();
                    DbDataReader reader = await ExecuteReadAsync(command, connection);

                    while (reader.Read())
                    {

                        if (reader["city"] == DBNull.Value &&
                            reader["state"] == DBNull.Value)
                        {
                            this.logger.Error("Null value returned for {0},{1}", coordinate.Longitude,
                                coordinate.Latitude);
                            this.logger.Debug("Failed Query {0}", query);
                            throw new ReverseGeocodingException("Unable to fetch city/state/zip");
                        }

                        city = (string) reader["city"];
                        state = (string) reader["state"];


                        if (reader["zipcode"] == DBNull.Value)
                        {
                            this.logger.Debug("Null zip for {0},{1} city:{2} state:{3}", coordinate.Longitude,
                                coordinate.Latitude, city, state);
                        }
                        else
                        {
                            zip = (String) reader["zipcode"];
                        }

                        //close the reader to avoid "Data reader open connection"
                        //Refer http://blogs.msdn.com/b/spike/archive/2009/08/20/there-is-already-an-open-datareader-associated-with-this-command-which-must-be-closed-first-explained.aspx
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    this.logger.Error("Exception while reverse geocoding type: {0}, Message: {1} ", ex.GetType().ToString(), ex.Message);
                    return null;
                }
                finally
                {
                    connection.Close();
                }
            }

            obj = new LocationDataObject(coordinate, city, state, zip);

            if (String.IsNullOrEmpty(city))
            {
                this.logger.Error("Null city returned for {0},{1}", coordinate.Longitude, coordinate.Latitude);
                //we cannot have a empty city and state
                throw new ReverseGeocodingException(String.Format("City is empty for {0},{1}", coordinate.Longitude, coordinate.Latitude));
            }

            if (String.IsNullOrEmpty(state))
            {
                this.logger.Error("Null state returned for {0},{1}", coordinate.Longitude, coordinate.Latitude);
                //we cannot have a empty state
                throw new ReverseGeocodingException(String.Format("State is empty for {0},{1}", coordinate.Longitude, coordinate.Latitude));

            }

            if (String.IsNullOrEmpty(zip))
            {
                this.logger.Error("No zip returned for {0},{1}", coordinate.Longitude, coordinate.Latitude);
            }

            return obj;
        }

        /// <summary>
        /// This is an extremely expensive query. Use it wisely
        /// </summary>
        /// <returns></returns>
        internal async Task<List<LocationDataObject>> GetAllLocation()
        {
            string query = @"SELECT zcta5ce AS zip,state.stusps AS state,place.name AS city,place.intptlat AS lat,place.intptlon AS lon from zcta5 
                             INNER JOIN place ON place.statefp = zcta5.statefp 
                             INNER JOIN state on place.statefp = state.statefp 
                             WHERE ST_INTERSECTS(zcta5.the_geom, place.the_geom)
                             ORDER BY zip;";
            var command = new NpgsqlCommand(query);
            command.CommandTimeout = 1800;
            List<LocationDataObject> objList = new List<LocationDataObject>();

            using (var connection = new NpgsqlConnection(this.ConnectionString))
            {
                try
                {
                    await connection.OpenAsync();
                    DbDataReader reader = await ExecuteReadAsync(command, connection);

                    if (reader == null || reader.FieldCount == 0)
                    {
                        //TODO error and log
                        Debugger.Break();
                    }

                    while (reader.Read())
                    {
                        string city = null;
                        string state = null;
                        string zip = null;
                        float lat;
                        float lon;

                        if (reader["city"] != DBNull.Value)
                        {
                            city = (string) reader["city"];
                        }

                        if (reader["state"] != DBNull.Value)
                        {
                            state = (string) reader["state"];
                        }

                        if (reader["zip"] != DBNull.Value)
                        {
                            zip = (String) reader["zip"];
                        }

                        if (reader["lat"] != DBNull.Value)
                        {
                            //invalid one so that client can ignore it
                            float.TryParse((String) reader["lat"], out lat);
                        }
                        else
                        {
                            //Log Warning
                            //Debugger.Break();
                            continue;
                        }

                        if (reader["lon"] != DBNull.Value)
                        {
                            //invalid one so that client can ignore it
                            float.TryParse((String) reader["lon"], out lon);

                        }
                        else
                        {
                            //Log Warning
                            //Debugger.Break();
                            continue;
                        }
                        reader.Close();
                        connection.Close();
                        var coordinateobject = new GeoCoordinateDataObject(lat, lon);
                        var obj = new LocationDataObject(coordinateobject, city, state, zip);
                        objList.Add(obj);
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
            return objList;
        }
    }
}