﻿using System;
using DataAccessLibrary.DataAccessObject;
using ServiceSchemas.Schemas;

namespace DataAccessLibrary
{
    internal static class DataAccessUtils
    {
        public static LocationDataObject ToLocationRepositoryObject(LocationInformationServiceObject locationInfoObj)
        {
            GeoCoordinateDataObject geocoordinateobj = null;

            if (locationInfoObj.Coordinates != null)
            {
                geocoordinateobj = ToGeoCoordinateDataObject(locationInfoObj.Coordinates);
            }

            return new LocationDataObject(geocoordinateobj, locationInfoObj.City, locationInfoObj.State, locationInfoObj.Zipcode);
        }

        public static LocationInformationServiceObject ToLocationServiceObject(LocationDataObject locationDataObject)
        {
            if (locationDataObject == null)
            {
                throw new ArgumentNullException("locationDataObject");
            }

            GeocoordinateServiceObject serviceCoordinates = null;

            if (locationDataObject.Coordinates != null)
            {
                serviceCoordinates = new GeocoordinateServiceObject(locationDataObject.Coordinates.Latitude, locationDataObject.Coordinates.Longitude);
            }
            return new LocationInformationServiceObject()
            {
                Coordinates = serviceCoordinates,
                City = locationDataObject.City,
                State = locationDataObject.State,
                Zipcode = locationDataObject.Zipcode
            };
        }

        public static GeoCoordinateDataObject ToGeoCoordinateDataObject(GeocoordinateServiceObject serviceObject)
        {
            GeoCoordinateDataObject dataObject = null;

            if (serviceObject != null)
            {
                dataObject = new GeoCoordinateDataObject(serviceObject.Latitude, serviceObject.Longitude);
            }

            return dataObject;
        }

        public static GeocoordinateServiceObject ToGeoCoordinateServiceObject(GeoCoordinateDataObject dataObject)
        {
            GeocoordinateServiceObject serviceObject = null;

            if (dataObject != null)
            {
                serviceObject = new GeocoordinateServiceObject(dataObject.Latitude, dataObject.Longitude);
            }

            return serviceObject;
        }

        public static bool TryPopulateServiceObject(LocationDataObject dataObject, ref LocationInformationServiceObject serviceObject)
        {
            if (dataObject == null)
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(serviceObject.City))
            {
                serviceObject.City = dataObject.City;
            }
            if (String.IsNullOrWhiteSpace(serviceObject.State))
            {
                serviceObject.State = serviceObject.State;
            }

            if (String.IsNullOrWhiteSpace(serviceObject.Zipcode))
            {
                serviceObject.Zipcode = dataObject.Zipcode;
            }

            if (serviceObject.Coordinates == null || serviceObject.Coordinates.Latitude == 0 || serviceObject.Coordinates.Longitude == 0)
            {
                serviceObject.Coordinates = ToGeoCoordinateServiceObject(dataObject.Coordinates);
            }

            return true;
        }
    }
}
