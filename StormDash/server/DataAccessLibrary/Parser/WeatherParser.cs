﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonUtils;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Parser
{
    internal abstract class WeatherParser
    {
        protected readonly List<LocationDataObject> locations;
        protected readonly Stream xmlStream;
        protected ILogger logger;
        protected String url;
        public WeatherParser(Stream xmlStream, List<LocationDataObject> locations, String url, Guid requestGuid)
        {
            this.xmlStream = xmlStream;
            this.locations = locations;
            this.logger = LogWrapper.GetLogger(GetType(), requestGuid);
            this.url = url;
        }
    }
}
