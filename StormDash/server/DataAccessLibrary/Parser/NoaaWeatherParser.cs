﻿using System;
using System.Collections.Generic; 
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using CommonUtils;
using CommunicationSchemas.Schemas.Protobuf;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Exceptions;
using DataAccessLibrary.Interface;

namespace DataAccessLibrary.Parser
{
    internal class NoaaWeatherParser : WeatherParser,IWeatherParser
    {
        private Dictionary<string, List<ulong>> timeLayoutDictionary;
        public NoaaWeatherParser(Stream xmlStream, List<LocationDataObject> locations, String url, Guid requestGuid) : base(xmlStream, locations,url, requestGuid) 
        {
            this.timeLayoutDictionary = new Dictionary<string, List<ulong>>();
        }
        public Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>> ParseWeatherXml()
        {
            var sw = Stopwatch.StartNew();
            Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>> result = null;
            try
            {
                var root = XElement.Load(this.xmlStream);

                if (root.Name == "error" || root.Descendants("error").Any())
                {
                    this.logger.Error("Xml contains error for url " + this.url );
                    throw new WeatherParserException("Error present in xml");
                }

                var count = 0;
                var locationsXml = (from locations in root.Descendants("location")
                    //first get all the locations and loop through them
                    where locations != null && locations.HasElements
                    //get the location key which represents the name of the point e.g. point2
                    from pointKey in locations.Elements("location-key")
                    where pointKey != null && !String.IsNullOrEmpty(pointKey.Value)
                    //get the xml that is refered by the point Key
                    from pointXml in root.Descendants("parameters")
                    where pointXml != null && pointXml.Attribute("applicable-location").Value == pointKey.Value
                    select new KeyValuePair<LocationDataObject, XElement>(this.locations[count++], pointXml))
                    .ToDictionary(k => k.Key, v => v.Value);

                ParseTimeLayoutKey(root);

                result = new Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>>();
                foreach(var locationXml in locationsXml)
                {
                    //for each location we create a sorted dictionary of time and weather object
                    var flatWeatherTimeLayout = GetFlattenTimeLayoutDictionary();

                    ParseMaxTemperature(locationXml.Value, flatWeatherTimeLayout);

                    ParseMinTemperature(locationXml.Value, flatWeatherTimeLayout);

                    ParseHourlyTemperature(locationXml.Value, flatWeatherTimeLayout);

                    ParsePercipitation(locationXml.Value, flatWeatherTimeLayout);

                    ParseSnow(locationXml.Value, flatWeatherTimeLayout);

                    ParseCloudCover(locationXml.Value, flatWeatherTimeLayout);

                    ParseWeatherType(locationXml.Value, flatWeatherTimeLayout);

                    result.Add(locationXml.Key, flatWeatherTimeLayout);
                };
            }
            catch
            {
                //TODO log the exception
                throw;
            }
            finally
            {
                this.logger.Performance("Time taken to parse weather xml: {0} ms", sw.ElapsedMilliseconds);
            }
            return result;
        }

        private void ParseHazards(XElement root,
                                  SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {
        }

        private void ParseWeatherType(XElement root,
                                      SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {
            try
            {
                var weatherElement = root.Descendants("weather").FirstOrDefault();
                List<ulong> currentTimeLayout = null;
                int timeCount = 0;

                if (weatherElement == null)
                {
                    this.logger.Error("No weather element present");
                    return;
                }

                // this should usually be 1 in number
                if (!weatherElement.HasElements ||
                    !this.timeLayoutDictionary.TryGetValue(weatherElement.Attribute("time-layout").Value,
                        out currentTimeLayout))
                {
                    this.logger.Error("Time-layout not present or weather element not present");
                    return;
                }
                var threeHourWeatherSummaryDictionary = new Dictionary<ulong, ThreeHourlyWeatherSummary>();

                var weatherConditionDescendants = root.Descendants("weather-conditions").ToList();
                if (weatherConditionDescendants.Count == 0)
                {
                    return;
                }

                // loop through weather-conditions
                /* eg xml
                 * <weather-conditions>
                        <value coverage="likely" intensity="light" weather-type="snow" qualifier="none">
                            <visibility xsi:nil="true"/>
                        </value>
                    </weather-conditions>
                 **/

                foreach (var weatherConditionElement in weatherConditionDescendants)
                {
                    var timekey = currentTimeLayout[timeCount++];

                    // loop through value  
                    var valueElements = weatherConditionElement.Descendants("value");
                    var weatherConditions = new List<WeatherConditionDataObject>();
                    var weatherAdditives = new List<WeatherAdditive>();

                    if (!weatherConditionElement.HasElements)
                    {
                        // in case there is an empty weather condition add it. Because it signifies clear weather
                        threeHourWeatherSummaryDictionary.Add(timekey, new ThreeHourlyWeatherSummary());
                        continue;
                    }

                    foreach(var valueElement in valueElements)
                    {
                        if (valueElement == null || !valueElement.HasAttributes)
                        {
                            continue;
                        }

                        WeatherConditionDataObject weatherConditionDataObject = null;

                        var parsedWeatherType = ParseWeatherType(valueElement);
                        if (parsedWeatherType == WeatherType.None)
                        {
                            continue;
                        }
                            
                        weatherConditionDataObject = new WeatherConditionDataObject()
                        {
                            weatherType = parsedWeatherType
                        };

                        var weatherAdditive = ParseWeatherAdditive(valueElement);
                        if (weatherAdditive != WeatherAdditive.None)
                        {
                            weatherAdditives.Add(weatherAdditive);                            
                        }

                        weatherConditionDataObject.weatherCoverage = ParseWeatherCoverage(valueElement);
                        weatherConditionDataObject.weatherIntensity = ParseWeatherIntensity(valueElement);

                        weatherConditions.Add(weatherConditionDataObject);

                    };

                    try
                    {
                        if (weatherConditions.Count > 0)
                        {
                            var threeHourlyWeatherSummary = new ThreeHourlyWeatherSummary()
                            {
                                WeatherConditions = weatherConditions,
                                WeatherAddtives = weatherAdditives
                            };

                            logger.Debug("Weather type {0} present for {1}", Utils.ConvertUnixTimeStampToUniversalDateTime(timekey), weatherConditions.First().weatherType);
                            threeHourWeatherSummaryDictionary.Add(timekey, threeHourlyWeatherSummary);
                        }
                    }
                    catch (Exception)
                    {
                        System.Diagnostics.Debugger.Break();
                        throw;
                    }
                }

                ThreeHourlyWeatherSummary currentTimeWeatherSummary = null, previousTimeWeatherSummary = null;

                foreach (var time in flatWeatherDictionary)
                {

                    if (threeHourWeatherSummaryDictionary.TryGetValue(time.Key, out currentTimeWeatherSummary) && currentTimeWeatherSummary != null)
                    {
                        previousTimeWeatherSummary = time.Value.ThreeHourlyWeatherSummary = currentTimeWeatherSummary;
                    }
                    else
                    {
                        time.Value.ThreeHourlyWeatherSummary = previousTimeWeatherSummary;
                    }
                }

            }
            catch (Exception)
            {
                Debugger.Break();
                throw;
            }
        }

        private WeatherIntensity ParseWeatherIntensity(XElement valueElement)
        {
            var weatherIntensityString = valueElement.Attribute("intensity").Value;
            WeatherIntensity? weatherIntensityEnum;
            if (!TryParseEnumFromString(weatherIntensityString, out weatherIntensityEnum))
            {
                // unable to parse. log it
                this.logger.Warn("Unable to parse the intensity with value {0}", weatherIntensityString);
            }

            return weatherIntensityEnum == null
                ? WeatherIntensity.None
                : weatherIntensityEnum.Value;

        }

        private WeatherType ParseWeatherType(XElement valueElement)
        {
            var weatherTypeString = valueElement.Attribute("weather-type").Value;
            WeatherType? weatherTypeEnum;
            if (!TryParseEnumFromString(weatherTypeString, out weatherTypeEnum))
            {
                // unable to parse. log it
                this.logger.Warn("Unable to parse the weather-type with value {0}", weatherTypeString);
            }

            return weatherTypeEnum == null
                ? WeatherType.None
                : weatherTypeEnum.Value;

        }

        private WeatherCoverage ParseWeatherCoverage(XElement valueElement)
        {
            var weatherCoverageString = valueElement.Attribute("coverage").Value;

            WeatherCoverage? weatherCoverageEnum;
            if (!TryParseEnumFromString(weatherCoverageString, out weatherCoverageEnum))
            {
                // unable to parse. log it
                this.logger.Warn("Unable to parse the coverage with value {0}", weatherCoverageString);
            }

            return weatherCoverageEnum == null
                ? WeatherCoverage.None
                : weatherCoverageEnum.Value;
        }

        private WeatherAdditive ParseWeatherAdditive(XElement valueElement)
        {
            WeatherAdditive? weatherAdditiveEnum = null;
            var weatherAdditiveString = valueElement.Attribute("additive") == null
                ? null
                : valueElement.Attribute("additive").Value;
            if (!string.IsNullOrWhiteSpace(weatherAdditiveString))
            {
                if (!TryParseEnumFromString(weatherAdditiveString, out weatherAdditiveEnum))
                {
                    // unable to parse. log it
                    this.logger.Warn("Unable to parse the weather additive with value {0}", weatherAdditiveString);
                }
            }


            return weatherAdditiveEnum == null
                ? WeatherAdditive.None
                : weatherAdditiveEnum.Value;
        }

        private static bool TryParseEnumFromString<TEnum>(string value, out TEnum? result) where TEnum : struct
        {
            result = null;

            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("T must be an enum type");
            }

            var valueNoWhiteSpace = value.Replace(" ", "_");

            TEnum parsedEnumValue;

            if (!Enum.TryParse(valueNoWhiteSpace, true, out parsedEnumValue))
            {
                return false;
            }

            result = parsedEnumValue;
            return true;
        }

        /// <summary>
        /// Parse the cloud cover of the given xml
        /// </summary>
        /// <param name="root">Root of the xml element</param>
        /// <param name="flatWeatherDictionary">The dictionary which consists of weather objects</param>
        /// <returns>Boolean (Always true)</returns>
        private void ParseCloudCover(XElement root,
                                     SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {
            List<ulong> currentKeyLayout = null;
            ushort currentCloudCoverValue = 0;
            var count = 0;
            var cloudElements = root.Descendants("cloud-amount");
            var cloudCoverFlatDictionary = new Dictionary<ulong, ushort>();
            foreach(var cloudElement in cloudElements)
            {
                if (!cloudElement.HasElements || !this.timeLayoutDictionary.TryGetValue(cloudElement.Attribute("time-layout").Value, out currentKeyLayout))
                {
                    //Something went wrong. log
                    this.logger.Error("Time-layout not present or cloud element not present");
                    continue;
                }

                var cloudElementValues = cloudElement.Elements("value");

                foreach(var value in cloudElementValues)
                {
                    if(value == null || value.IsEmpty )
                    {
                        this.logger.Debug("Value is empty in cloud element. Skipping");
                        continue;
                    }

                    if(!ushort.TryParse(value.Value, out currentCloudCoverValue))
                    {
                        this.logger.Debug("Unable to parse the cloud value");
                        continue;
                    }
                    cloudCoverFlatDictionary.Add(currentKeyLayout[count++],currentCloudCoverValue);

                }
            }


            ushort? previosTimeCloudCover = null;
            ushort currentTimeCloudCover;
            foreach (var time in flatWeatherDictionary)
            {

                if (cloudCoverFlatDictionary.TryGetValue(time.Key, out currentTimeCloudCover))
                {
                    previosTimeCloudCover = time.Value.CloudCoverPercent = currentTimeCloudCover;
                }
                else
                {
                    time.Value.CloudCoverPercent = previosTimeCloudCover;
                }
            }
        }

        /// <summary>
        /// Parse the precipitation of the given xml
        /// </summary>
        /// <param name="root">Root of the xml element</param>
        /// <param name="flatWeatherDictionary">The dictionary which consists of weather objects</param>
        /// <returns>Boolean (Always true)</returns>
        private void ParsePercipitation(XElement root,
                                        SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {

            List<ulong> currentKeyLayout = null;
            ushort currentPercipitationValue = 0;
            var count = 0;
            var percipitationFlatDictionary = (from p in root.Descendants("probability-of-precipitation")
                                               //get only the type required
                                               where p.HasElements
                                               from pValue in p.Elements("value")
                                               where pValue != null && !pValue.IsEmpty &&
                                                   //make sure you can parse the value. nil cannot be parsed 
                                                     ushort.TryParse(pValue.Value, out currentPercipitationValue) &&
                                                   //get the current key list so that you can assign the values
                                                     this.timeLayoutDictionary.TryGetValue(p.Attribute("time-layout").Value, out currentKeyLayout) &&
                                                     currentKeyLayout.Count > 0
                                               select new KeyValuePair<ulong, ushort>(currentKeyLayout[count++], currentPercipitationValue))
                                 .ToDictionary(k => k.Key, v => v.Value);

            ushort? previosTimePercipitation = null;
            ushort currentTimePercipitation;
            foreach (var time in flatWeatherDictionary)
            {

                if (percipitationFlatDictionary.TryGetValue(time.Key, out currentTimePercipitation))
                {
                    previosTimePercipitation = time.Value.PrecipitationChances = currentTimePercipitation;
                }
                else
                {
                    time.Value.PrecipitationChances = previosTimePercipitation;
                }
            }
        }

        /// <summary>
        /// Parse the snow xml element from the given xml document
        /// </summary>
        /// <param name="root">Root of the xml element</param>
        /// <returns>Boolean (Always true)</returns>
        private void ParseSnow(XElement root,
                               SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {

            List<ulong> currentKeyLayout = null;
            float currentSnowValue = 0f;
            var count = 0;
            var snowFlatDictionary = (from s in root.Descendants("precipitation")
                                      //get only the type required
                                      where s.HasElements && s.HasAttributes && !String.IsNullOrEmpty(s.Attribute("type").Value)
                                            && s.Attribute("type").Value == "snow"
                                      from sValue in s.Elements("value")
                                      where sValue != null && !sValue.IsEmpty &&
                                          //make sure you can parse the value. nil cannot be parsed 
                                          float.TryParse(sValue.Value, out currentSnowValue) &&
                                          //get the current key list so that you can assign the values
                                          this.timeLayoutDictionary.TryGetValue(s.Attribute("time-layout").Value, out currentKeyLayout) &&
                                              currentKeyLayout.Count > 0
                                      select new KeyValuePair<ulong, float>(currentKeyLayout[count++], currentSnowValue))
                                 .ToDictionary(k => k.Key, v => v.Value);

            float? previosTimeSnow = null;
            float currentTimeSnow;
            foreach (var time in flatWeatherDictionary)
            {

                if (snowFlatDictionary.TryGetValue(time.Key, out currentTimeSnow))
                {
                    previosTimeSnow = time.Value.SnowfallValue = currentTimeSnow;
                }
                else
                {
                    time.Value.SnowfallValue = previosTimeSnow;
                }
            }
        }

        /// <summary>
        /// Parse any type of temperature (hourly, minimum, maximum)
        /// </summary>
        /// <param name="root">Root of the xml element</param>
        /// <param name="type">Type of temperature</param>
        /// <returns>Boolean (Always true)</returns>
        private Dictionary<ulong, Int16> ParseTemperature(XElement root,
                                                          string type)
        {
            if (String.IsNullOrEmpty(type))
            {
                return null;
            }

            List<ulong> currentKeyLayout = null;
            Int16 currentTemperatureValue = 0;
            var count = 0;
            var temperatureFlatDictionary = (from t in root.Descendants("temperature")
                                             //get only the type required
                                             where t.HasElements && t.Attribute("type").Value == type
                                             from tValue in t.Elements("value")
                                             where tValue != null && !tValue.IsEmpty &&
                                                 //make sure you can parse the value. nil cannot be parsed 
                                                   Int16.TryParse(tValue.Value, out currentTemperatureValue) &&
                                                 //get hte current key list so that you can assign the values
                                                   this.timeLayoutDictionary.TryGetValue(t.Attribute("time-layout").Value, out currentKeyLayout) &&
                                                   currentKeyLayout.Count > 0
                                             select new KeyValuePair<ulong, Int16>(currentKeyLayout[count++], currentTemperatureValue))
                                 .ToDictionary(k => k.Key, v => v.Value);
            return temperatureFlatDictionary;

        }

        private bool ParseHourlyTemperature(XElement root,
                                            SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {
            var temperatureFlatDictionary = ParseTemperature(root, "hourly");
            Int16? previousTimeTemp = null;
            Int16 currentTimeTemp;
            foreach (var time in flatWeatherDictionary)
            {

                if (temperatureFlatDictionary.TryGetValue(time.Key, out currentTimeTemp))
                {
                    previousTimeTemp = time.Value.Temperature = currentTimeTemp;
                }
                else
                {
                    time.Value.Temperature = previousTimeTemp;
                }
            }
            return true;
        }

        private bool ParseMinTemperature(XElement root,
                                         SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {
            var temperatureFlatDictionary = ParseTemperature(root, "minimum");
            Int16? previousTimeTemp = null;
            Int16 currentTimeTemp;
            foreach (var time in flatWeatherDictionary)
            {

                if (temperatureFlatDictionary.TryGetValue(time.Key, out currentTimeTemp))
                {
                    previousTimeTemp = time.Value.MinTemperature = currentTimeTemp;
                }
                else
                {
                    time.Value.MinTemperature = previousTimeTemp;
                }
            }
            return true;
        }
        private void ParseMaxTemperature(XElement root,
                                         SortedDictionary<ulong, WeatherDataObject> flatWeatherDictionary)
        {
            var temperatureFlatDictionary = ParseTemperature(root, "maximum");
            Int16? previousTimeTemp = null;
            Int16 currentTimeTemp;
            foreach (var time in flatWeatherDictionary)
            {

                if (temperatureFlatDictionary.TryGetValue(time.Key, out currentTimeTemp))
                {
                    previousTimeTemp = time.Value.MaxTemperature = currentTimeTemp;
                }
                else
                {
                    time.Value.MaxTemperature = previousTimeTemp;
                }
            }
        }

        private void ParseTimeLayoutKey(XElement root)
        {
            var timeLayoutKeys = root.Descendants("time-layout").ToList();
            if (timeLayoutKeys.Count == 0)
            {
                this.logger.Error("Time xml is missing for url {0}",this.url);
                throw new WeatherParserException("Time layout is missing from the xml");
            }

            this.timeLayoutDictionary = new Dictionary<string, List<ulong>>();
            foreach (var key in timeLayoutKeys)
            {
                if (key.HasElements)
                {
                    var layoutKey = key.Element("layout-key");
                    //check if key has layout key. if not skip it
                    if (layoutKey == null)
                    {
                        this.logger.Error("Layout key is null");
                        throw new NullReferenceException("layout-key is null");
                    }

                    var timeLayoutKeyElements = new List<ulong>();

                    var startKeyElements = key.Elements("start-valid-time");

                    if(startKeyElements == null || !startKeyElements.Any())
                    {
                        //unable to find any start key elements
                        this.logger.Warn("unable to find any start key elements for {0}", layoutKey.Value);
                        continue;
                    }

                    foreach(var startKeyElement in startKeyElements)
                    {
                        //get the time in the time zone the region is in
                        //here the first part is (before '-') is local time zone and after '-' is in UTC of the point
                        //so in below example 2014-09-04T08:00:00 is in local time zone whereas 2014-09-04T08:00:00-04:00 = 2014-09-04T04:00:00 is in UTC
                        //eg 2014-09-04T08:00:00-04:00
                        if(startKeyElement == null || String.IsNullOrEmpty(startKeyElement.Value))
                        {
                            //start key element value is null
                            this.logger.Debug("start key element value is null {0}");
                            continue;
                        }
                        var dtsPosEnd = startKeyElement.Value.LastIndexOf('-');
                        DateTime timeLocalZone;
                        if(!DateTime.TryParse(startKeyElement.Value.Substring(0, dtsPosEnd), out timeLocalZone))
                        {
                            this.logger.Debug("Unable to parse time for key {0} and startkey value {1}", layoutKey.Value, startKeyElement.Value);
                            continue;
                        }
                        
                        DateTime utcHoursToAdd;
                        // convert to UTC by taking everything after '-'
                        if (!DateTime.TryParse(
                                startKeyElement.Value.Substring(dtsPosEnd + 1), out utcHoursToAdd))
                        {
                            this.logger.Debug("Unable to parse utc time span for key {0} and startkey value {1}", layoutKey.Value, startKeyElement.Value);
                            continue;
                        }

                        //this time is in local time zone
                        timeLayoutKeyElements.Add(Utils.ConverttoUnixTimeStampInSameTimeZone(timeLocalZone.AddHours(utcHoursToAdd.Hour)));
                    }

                    if (timeLayoutKeyElements.Count == 0)
                    {
                        this.logger.Error("Unable to parse time layout key elements in key {0}" , layoutKey.Value);
                        throw new WeatherParserException("Unable to parse time layout key elements in key" + layoutKey.Value);
                    }

                    this.timeLayoutDictionary.Add(layoutKey.Value, timeLayoutKeyElements);
                }
            }
        }

        /// <summary>
        /// This function flattens the time so that we have a sorted list of unix time stamp and corresponding weather object 
        /// </summary>
        /// <param name="timeLayout"></param>
        /// <returns></returns>
        private SortedDictionary<ulong, WeatherDataObject> GetFlattenTimeLayoutDictionary()
        {
            var flatWeatherDictionary = new SortedDictionary<ulong, WeatherDataObject>();
            foreach (var timeList in this.timeLayoutDictionary)
            {
                //loop through all the elements in the list
                foreach (var dateTimeValue in timeList.Value)
                {
                    if (!flatWeatherDictionary.ContainsKey(dateTimeValue))
                    {
                        flatWeatherDictionary.Add(dateTimeValue, new WeatherDataObject(dateTimeValue));
                    }
                }
            }

            return flatWeatherDictionary;
        }
    }
}
