﻿using System;
using System.Configuration;

namespace DataAccessLibrary.RepositoryConfiguration
{
    /// <summary>
    /// This class is abstract since we do not want to instantiate this class.
    /// Class provides a basic implementation of setting the connection string from the config
    /// </summary>
    internal abstract class RepositoryConfiguration
    {
        protected string connectionString;

        protected RepositoryConfiguration(string connectionString)
        {
            this.connectionString = connectionString;
        }

        protected bool IsTestingTurnedOn { get; private set; }
        
        protected RepositoryConfiguration(string productionStringName, string developerStringName)
        {
            if(String.IsNullOrWhiteSpace(productionStringName))
            {
                throw new ArgumentNullException("productionStringName");
            }

            this.connectionString = ConfigurationManager.AppSettings[productionStringName];
            if (String.IsNullOrEmpty(this.connectionString))
            {
                throw new NullReferenceException("Configuration does not exist");
            }

#if DEBUG
           if(!String.IsNullOrWhiteSpace(developerStringName))
           {
               var debugConnectionString = ConfigurationManager.AppSettings[developerStringName];
               if (!String.IsNullOrEmpty(debugConnectionString))
               {
                    this.connectionString = debugConnectionString;
               }
           }
#endif

            //check if test is turned on
            var testSettings = ConfigurationManager.AppSettings["Test"];
            IsTestingTurnedOn = false;
            if(testSettings != null)
            {
                IsTestingTurnedOn = true;
            }
        }
    }
}
