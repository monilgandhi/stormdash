﻿using DataAccessLibrary.Interface;

namespace DataAccessLibrary.RepositoryConfiguration
{
    internal class NoaaWeatherRepositoryConfiguration:RepositoryConfiguration,IRepositoryConfiguration
    {
        public NoaaWeatherRepositoryConfiguration() : base("NoaaWeatherBaseUrl", "") { }
        public string GetConnectionString()
        {
            return this.connectionString;
        }

        public bool IsTestTurnedOn()
        {
            return IsTestingTurnedOn;
        }
    }
}
