﻿using DataAccessLibrary.Interface;

namespace DataAccessLibrary.RepositoryConfiguration
{
    internal class RedisRepositoryConfiguration : RepositoryConfiguration, IRepositoryConfiguration
    {
        public RedisRepositoryConfiguration() : base("ProdRedisCache","LocalRedisCache"){}
        public string GetConnectionString()
        {
            return this.connectionString;
        }

        public bool IsTestTurnedOn()
        {
            return false;
        }
    }
}
