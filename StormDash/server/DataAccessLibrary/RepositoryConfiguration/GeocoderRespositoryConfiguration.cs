﻿using System;
using DataAccessLibrary.Interface;

namespace DataAccessLibrary.RepositoryConfiguration
{
    internal class GeocoderRespositoryConfiguration : RepositoryConfiguration, IRepositoryConfiguration
    {
        public GeocoderRespositoryConfiguration() : base("ProdGeocoder", "LocalGeocoder") { }
        public string GetConnectionString()
        {
            return connectionString;
        }

        public bool IsTestTurnedOn()
        {
            return false;
        }
    }
}
