﻿namespace DataAccessLibrary.RepositoryConfiguration
{
    internal class GoogleMapsApiConfiguration : RepositoryConfiguration
    {
        public GoogleMapsApiConfiguration()
            : base("GoogleMapsApiKey", "GoogleMapsApiKey")
        {
        }

        public string ApiKey
        {
            get { return this.connectionString; }
        }
    }
}
