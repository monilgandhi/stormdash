﻿using System;
using System.Configuration;
using DataAccessLibrary.DataAccess;
using DataAccessLibrary.Interface;
using DataAccessLibrary.Parser;

namespace DataAccessLibrary
{
    internal static class DataAccessFactory
    {
        public static string AreTestsRunning = ConfigurationManager.AppSettings["Test"];
        public static IWeatherRepository GetWeatherRepository(Guid requestGuid)
        {
            return new NoaaWeatherRespository(requestGuid);
        }

        public static ICacheRepository GetCacheRepository(Guid requestGuid)
        {
            return new RedisCacheRepository(requestGuid);
        }

        public static IGeocoderRepository GetGeocoderRepository(Guid requestGuid)
        {
            return new PostgisGeocodeRespository(requestGuid);
        }
    }
}
