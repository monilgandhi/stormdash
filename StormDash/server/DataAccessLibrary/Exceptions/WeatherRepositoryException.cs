﻿using System;
using System.Runtime.Serialization;

namespace DataAccessLibrary.Exceptions
{
    [Serializable]
    public class WeatherRepositoryException :Exception
    {
        public WeatherRepositoryException()
        {
           
        }

        public WeatherRepositoryException(string message):base(message)
        {

        }

        public WeatherRepositoryException(string message, Exception innerException):base(message, innerException)
        {

        }

        protected WeatherRepositoryException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}