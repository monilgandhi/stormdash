﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Exceptions
{
    [Serializable]
    internal class ReverseGeocodingException : Exception
    {
        public ReverseGeocodingException()
        {
           
        }

        public ReverseGeocodingException(string message):base(message)
        {

        }

        public ReverseGeocodingException(string message, Exception innerException):base(message, innerException)
        {

        }

        protected ReverseGeocodingException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
