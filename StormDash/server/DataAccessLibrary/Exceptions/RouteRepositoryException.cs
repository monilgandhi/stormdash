﻿using System;
using System.Runtime.Serialization;

namespace DataAccessLibrary.Exceptions
{
    [Serializable]
    internal class RouteRepositoryException : Exception
    {
        public RouteRepositoryException()
        {
           
        }

        public RouteRepositoryException(string message):base(message)
        {

        }

        public RouteRepositoryException(string message, Exception innerException):base(message, innerException)
        {

        }

        protected RouteRepositoryException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
