﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Exceptions
{
    [Serializable]
    internal class WeatherParserException : Exception
    {
        public WeatherParserException()
        {
           
        }

        public WeatherParserException(string message):base(message)
        {

        }

        public WeatherParserException(string message, Exception innerException):base(message, innerException)
        {

        }

        protected WeatherParserException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
