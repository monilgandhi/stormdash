﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccess;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Exceptions;
using DataAccessLibrary.Interface;
using DataAccessLibrary.RepositoryAccessor;
using ServiceSchemas.Schemas;
using WeatherApp.Communication;

namespace DataAccessLibrary
{

    /// <summary>
    /// This class is the one that faces to other libraries/application. We use the accessors to do caching, data type conversion etc so that 
    /// the client is completely abstracted out
    /// </summary>
    public class Repository
    {
        private readonly IGeocoderRepositoryAccessor gRepositoryAccessor;

        private readonly IWeatherRepositoryAccessor wRepositoryAccessor;

        private readonly IFeedbackRepository feedBackRepository;

        private ILogger logger;
         
        public ErrorCode ErrorCode { get; private set; }

        private List<LocationInformationServiceObject> locationServiceObjList;
        private List<LocationDataObject> reverseGeocodeResponse;
        private Dictionary<LocationDataObject, LocationInformationServiceObject> dataObjtoSvcObjMap;
        public Repository(Guid requestGuid)
        {
            this.gRepositoryAccessor = new GeocoderRepositoryAccessor(requestGuid);
            this.wRepositoryAccessor = new WeatherRepositoryAccessor(requestGuid, DataAccessFactory.GetWeatherRepository(requestGuid));
            this.feedBackRepository = new FeedbackRepository();
            this.logger = LogWrapper.GetLogger(this.GetType(), requestGuid);
        }

        public async Task<bool> PostFeedbackAsync(FeedbackPostRequest FeedbackPostRequest)
        {
            if (FeedbackPostRequest == null)
            {
                throw new ArgumentNullException(nameof(FeedbackPostRequest));
            }

            if (string.IsNullOrWhiteSpace(FeedbackPostRequest.Feedback))
            {
                throw new ArgumentException("Feedback is null");
            }

            return await this.feedBackRepository.PostFeedbackAsync(FeedbackPostRequest);
        }

        /// <summary>
        /// Reverse geocode location objects. Provide source and destination to look in the cache
        /// </summary>
        /// <param name="locationObj">List of location objects to be reversegeocoded</param>
        /// <param name="source">Source location information</param>
        /// <param name="destination">Destination location information</param>
        /// <returns></returns>
        public async Task<bool> ReverseGeocodeAsync(List<LocationInformationServiceObject> locationObj, 
                                                    LocationInformationServiceObject source = null,
                                                    LocationInformationServiceObject destination = null)
        {
            if (locationObj == null || locationObj.Count == 0)
            {
                throw new ArgumentNullException("locationObj");
            }

            this.locationServiceObjList = locationObj;

            LocationDataObject sourceLocationDataObj = null,destinationLocationObj = null;
            if(source != null)
            {
                sourceLocationDataObj = DataAccessUtils.ToLocationRepositoryObject(source);
            }

            if(destination != null)
            {
                destinationLocationObj = DataAccessUtils.ToLocationRepositoryObject(destination);
            }

            this.reverseGeocodeResponse = await this.gRepositoryAccessor.ReverseGeocodeAsync(GenerateCoordinateList(), sourceLocationDataObj, destinationLocationObj);

            if (this.reverseGeocodeResponse == null || this.reverseGeocodeResponse.Count < 2)
            {
                this.logger.Error("Null response received from reverse geocode response");
                this.ErrorCode = ErrorCode.GeocoderError;
                return false;
            } 
            return true;
        }

        /// <summary>
        /// This function takes in the service objects and retuns the service objects back in sorted order of their arrival time
        /// </summary>
        /// <param name="locationObj">service location objects</param>
        /// <param name="source">Source object</param>
        /// <param name="destination">destination objects</param>
        /// <returns></returns>
        public async Task<List<LocationInformationServiceObject>> GetWeatherAsync(List<LocationInformationServiceObject> locationObj,
                                                                                  LocationInformationServiceObject source,
                                                                                  LocationInformationServiceObject destination)
        {
            var success = await ReverseGeocodeAsync(locationObj, source, destination);

            if (!success)
            {
                return null;
            }

            FilterOutNullDupesAndGenerateMap();

            //sanity check
            if(this.locationServiceObjList.Count != this.reverseGeocodeResponse.Count)
            {
                this.logger.Error("location service obj list and reversegeocode objects do not match");
                throw new OperationCanceledException("Mismatch occurred between locationservice object list and reverse geocoded list");
            }

            var requiredTimeList = GetRequiredTimeList();

            var weatherResult = await this.wRepositoryAccessor.FetchWeather(this.reverseGeocodeResponse, requiredTimeList);

            if (requiredTimeList.Count != weatherResult.Count)
            {
                this.logger.Error("required time count {0} and weather result returned do not match {1}", requiredTimeList.Count, weatherResult.Count);
            }
             
            return CreateResponse(weatherResult);
        }

        public async Task<List<LocationInformationServiceObject>> SyncWeather(List<LocationInformationServiceObject> locationInfoObjList)
        {
            if (locationInfoObjList == null || locationInfoObjList.Count == 0)
            {
                throw new ArgumentNullException("locationInfoObjList");
            }
            
            var locationObjList = new List<LocationDataObject>();
            foreach(var locationInfoObj in locationInfoObjList)
            {
                locationObjList.Add(DataAccessUtils.ToLocationRepositoryObject(locationInfoObj));
            }

            List<LocationDataObject> notfetched = null;
            try
            {
                notfetched = await this.wRepositoryAccessor.SyncWeather(locationObjList);
            }
            catch(WeatherParserException)
            {
                //error exists in this url
                StringBuilder locationString = new StringBuilder();
                foreach(var location in locationObjList)
                {
                    locationString.Append(location);
                }

                throw new WeatherRepositoryException("Problems with locations: " + locationString);
            }
            

            var result = new List<LocationInformationServiceObject>();
            foreach(var item in notfetched)
            {
                result.Add(DataAccessUtils.ToLocationServiceObject(item));
            }

            return result;
        }

        /// <summary>
        /// Get the required time from the weather service object and convert it to list
        /// </summary>
        /// <returns></returns>
        private List<ulong> GetRequiredTimeList()
        {
            var requiredTimeList = new List<ulong>();
            foreach(var location in this.locationServiceObjList)
            {
                if(location.Weather == null)
                {
                    this.logger.Error("No arrival time given for location {0}", location.ToString());
                    continue;
                }

                requiredTimeList.Add(location.Weather.RequiredTime);
            }
            return requiredTimeList;
        }

        /// <summary>
        /// Generate a coordinate data object list from teh location service object list
        /// </summary>
        /// <returns>List of geo coordinate data object</returns>
        private List<GeoCoordinateDataObject> GenerateCoordinateList()
        {
            var coordinateList = new List<GeoCoordinateDataObject>();
            foreach(var location in this.locationServiceObjList)
            {
                var obj = DataAccessUtils.ToGeoCoordinateDataObject(location.Coordinates);
                coordinateList.Add(obj);
            }
            return coordinateList;
        }
        /// <summary>
        /// There are chances that reverse geocoding may fail. In case if it fails, the accessor will return a null value.
        /// We need to remove it and also remove corresponding time
        /// </summary>
        private void FilterOutNullDupesAndGenerateMap()
        {
            this.dataObjtoSvcObjMap = new Dictionary<LocationDataObject, LocationInformationServiceObject>();
            var nullLocationObjs = 0;

            ISet<string> uniqueZipCodes = new HashSet<string>();

            for(int i = 0; i < this.reverseGeocodeResponse.Count;)
            {
                if (this.reverseGeocodeResponse[i] == null)
                {
                    //remove the items in required time and reversegeocode response.
                    //here the assumption is made that the mapping will be maintained
                    this.reverseGeocodeResponse.Remove(this.reverseGeocodeResponse[i]);
                    this.locationServiceObjList.Remove(this.locationServiceObjList[i]);
                    ++nullLocationObjs;
                }
                else if (uniqueZipCodes.Contains(this.reverseGeocodeResponse[i].Zipcode))
                {
                    //remove the items in required time and reversegeocode response.
                    //here the assumption is made that the mapping will be maintained
                    this.reverseGeocodeResponse.Remove(this.reverseGeocodeResponse[i]);
                    this.locationServiceObjList.Remove(this.locationServiceObjList[i]);
                }
                else
                {
                    this.dataObjtoSvcObjMap.Add(this.reverseGeocodeResponse[i], this.locationServiceObjList[i]);
                    uniqueZipCodes.Add(this.reverseGeocodeResponse[i].Zipcode);
                    ++i;
                }
            }

            if(nullLocationObjs > 0)
            {
                this.logger.Warn("Total Null Location objs encountered {0}", nullLocationObjs);
            }
        }

        private List<LocationInformationServiceObject> CreateResponse(Dictionary<LocationDataObject, WeatherDataObject> weatherDetails)
        {
            var sortedDictionaryByTime = new SortedDictionary<ulong, LocationInformationServiceObject>();
            var nullWeatherCount = 0;

            foreach (var locationWeather in weatherDetails)
            {
                if (locationWeather.Value == null || locationWeather.Key == null)
                {
                    this.logger.Debug("Null weather received for location");
                    ++nullWeatherCount;
                    continue;
                }
                var currentWeather = locationWeather.Value;
                LocationInformationServiceObject actualLocationSvcResponse;

                //we need the service object since it has the time information inside it
                if (!this.dataObjtoSvcObjMap.TryGetValue(locationWeather.Key, out actualLocationSvcResponse))
                {
                    //we cannot find corresponding time
                    this.logger.Warn("Unable to find corresponding location service response {0}", locationWeather.Key.ToString());
                    continue;
                }

                DataAccessUtils.TryPopulateServiceObject(locationWeather.Key, ref actualLocationSvcResponse);
                var serviceWeatherResponse = actualLocationSvcResponse.Weather;
                
                currentWeather.PopulateServiceObject(serviceWeatherResponse);
                
                sortedDictionaryByTime.Add(serviceWeatherResponse.RequiredTime, actualLocationSvcResponse);
            }

            if(nullWeatherCount > 0)
            {
                this.logger.Warn("Total null weather received", nullWeatherCount);
            }
            
            //now loop through the sorteddict and get the list
            var result = new List<LocationInformationServiceObject>();
            foreach(var locationResponse in sortedDictionaryByTime)
            {
                result.Add(locationResponse.Value);
            }

            return result;
        }
    }
}