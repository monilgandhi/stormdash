﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    interface ICacheRepository
    {
        Task<CacheDataObject<T>> Get<T>(string key);

        Task<bool> Set<T>(string key, CacheDataObject<T> obj, TimeSpan? timeOut = null);

    }
}