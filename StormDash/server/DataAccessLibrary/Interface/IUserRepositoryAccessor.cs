﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Interface
{
    internal interface IUserRepositoryAccessor
    {
        void RegisterUser(string email);
    }
}
