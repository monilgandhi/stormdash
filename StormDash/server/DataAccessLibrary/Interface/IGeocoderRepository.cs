﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    internal interface IGeocoderRepository
    {
        Task<GeoCoordinateDataObject> GeocodeAsync(string address);
        Task<List<RepositoryResult<LocationDataObject>>> ReverseGeocodeAsync(List<GeoCoordinateDataObject> coordinates);
    }
}
