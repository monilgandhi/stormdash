﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    internal interface IWeatherRepository
    {
        Task<Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>>> Fetch(List<LocationDataObject> locationObjs);
    }
}
