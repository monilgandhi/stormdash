﻿using System.Threading.Tasks;
using WeatherApp.Communication;

namespace DataAccessLibrary.Interface
{
    public interface IFeedbackRepository
    {
        Task<bool> PostFeedbackAsync(FeedbackPostRequest feedback);
    }
}