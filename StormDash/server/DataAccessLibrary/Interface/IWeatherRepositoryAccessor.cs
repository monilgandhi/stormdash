﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    internal interface IWeatherRepositoryAccessor
    {
        Task<Dictionary<LocationDataObject, WeatherDataObject>> FetchWeather(List<LocationDataObject> locationDataObjs, List<ulong> arrivalTime);
        Task<List<LocationDataObject>> SyncWeather(List<LocationDataObject> locationDataObjs);
    }
}
