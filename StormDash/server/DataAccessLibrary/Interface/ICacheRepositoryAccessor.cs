﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    internal interface ICacheRepositoryAccessor
    {
        Task<CacheRepositoryResult<T>> GetAsync<T>(String key);
        Task<CacheRepositoryResult<bool>> SetAsync<T>(String key, T obj, ulong timeout = 0, ulong lastUpdateTime = 0);
    }
}
