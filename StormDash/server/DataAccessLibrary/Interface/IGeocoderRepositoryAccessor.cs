﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    internal interface IGeocoderRepositoryAccessor
    {
        Task<List<LocationDataObject>> ReverseGeocodeAsync(List<GeoCoordinateDataObject> locationObj,
                                                     LocationDataObject source = null, 
                                                     LocationDataObject destination = null);
    }
}
