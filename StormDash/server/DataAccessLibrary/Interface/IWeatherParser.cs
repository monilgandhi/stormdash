﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Interface
{
    interface IWeatherParser
    {
        Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>> ParseWeatherXml();
    }
}
