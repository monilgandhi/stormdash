﻿using System;
namespace DataAccessLibrary.DataAccessObject
{
    [Serializable]
    internal class GeoCoordinateDataObject
    {

        public float Longitude
        {
            get;
            private set;
        }

        public float Latitude
        {
            get;
            private set;
        }

        public GeoCoordinateDataObject(float latitude,float longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override bool Equals(Object obj)
        {
 	        if(obj == null || obj.GetType() != GetType())
            {
                return false;
            }
            var myLat = Math.Round(Latitude, 2);
            var myLon = Math.Round(Longitude, 2);

            var theirObj = obj as GeoCoordinateDataObject;

            if (theirObj == null)
            {
                return false;
            }

            var theirLat = Math.Round(theirObj.Latitude, 2);
            var theirLon = Math.Round(theirObj.Longitude, 2);

            // we want atleast two decimal place precision
            return !(Math.Abs(myLat - theirLat) > 0.009) || !(Math.Abs(myLon - theirLon) > 0.009);
        }
    }
}