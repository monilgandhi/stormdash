﻿using System;

namespace DataAccessLibrary.DataAccessObject
{
    [Serializable]
    internal class CacheDataObject<T>
    {
        public T Data { get; set; }
        public ulong LastUpdate { get; set; }
    }
}
