﻿using Microsoft.WindowsAzure.Storage.Table;

namespace DataAccessLibrary.DataAccessObject
{
    public class FeedbackTableEntity : TableEntity
    {
        public string FeedBack { get; set; }
        public FeedbackTableEntity(string partitionKey, string rowKey, string feedBack) : base (partitionKey, rowKey)
        {
            this.FeedBack = feedBack;
        }
    }
}
