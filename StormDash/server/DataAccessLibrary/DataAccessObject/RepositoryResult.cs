﻿namespace DataAccessLibrary.DataAccessObject
{
    internal enum RepositoryResultEnum
    {
        Success,
        Failure
    }

    internal class RepositoryResult<T>
    {
        public T ResultData;
        public RepositoryResultEnum Status;
    }

    internal class CacheRepositoryResult<T> : RepositoryResult<T>
    {
        public ulong LastUpdate;
    }
}
