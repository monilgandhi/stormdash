﻿using System;
using System.Collections.Generic;
using ServiceSchemas.Schemas;

namespace DataAccessLibrary.DataAccessObject
{
    [Serializable]
    internal class WeatherDataObject : DataObject
    {   
        public ushort? PrecipitationChances { get; internal set; }

        public float? SnowfallValue { get; internal set; }

        public Int16? MinTemperature { get; internal set; }

        public Int16? MaxTemperature { get; internal set; }

        public Int16? Temperature { get; internal set; }

        public UInt16? CloudCoverPercent { get; internal set; }

        public ulong WeatherTime { get; private set; }

        public ThreeHourlyWeatherSummary ThreeHourlyWeatherSummary { get; internal set; } 

        public WeatherDataObject(ulong weatherTime)
        {
            WeatherTime = weatherTime;
        }

        public override ServiceObject ToServiceObjecct()
        {
            var weatherSvcObj = new WeatherServiceObject(WeatherTime);
            weatherSvcObj.PrecipitationChances = PrecipitationChances;
            weatherSvcObj.MinTemperature = MinTemperature;
            weatherSvcObj.MaxTemperature = MaxTemperature;
            weatherSvcObj.CloudCoverPercent = CloudCoverPercent;
            weatherSvcObj.SnowfallValue = SnowfallValue;
            weatherSvcObj.Temperature = Temperature;
            return weatherSvcObj;
        }

        public override ServiceObject PopulateServiceObject(ServiceObject svcObj)
        {
            if(svcObj.GetType() != typeof(WeatherServiceObject))
            {
                return svcObj;
            }

            var weatherObj = svcObj as WeatherServiceObject;
            weatherObj.PrecipitationChances = PrecipitationChances;
            weatherObj.MinTemperature = MinTemperature;
            weatherObj.MaxTemperature = MaxTemperature;
            weatherObj.CloudCoverPercent = CloudCoverPercent;
            weatherObj.SnowfallValue = SnowfallValue;
            weatherObj.Temperature = Temperature;
            if (this.ThreeHourlyWeatherSummary != null && 
                this.ThreeHourlyWeatherSummary.WeatherConditions != null &&
                this.ThreeHourlyWeatherSummary.WeatherConditions.Count > 0)
            {
                var weatherConditionServiceObjects = new List<WeatherConditionServiceObject>();
                foreach (var weatherConditionDataObject in this.ThreeHourlyWeatherSummary.WeatherConditions)
                {
                    weatherConditionServiceObjects.Add(new WeatherConditionServiceObject()
                    {
                        weatherCoverage = weatherConditionDataObject.weatherCoverage,
                        weatherIntensity = weatherConditionDataObject.weatherIntensity,
                        weatherType = weatherConditionDataObject.weatherType
                    });
                }

                weatherObj.ThreeHourlyWeatherSummaryServiceObject = new ThreeHourlyWeatherSummaryServiceObject()
                {
                    WeatherAddtives = this.ThreeHourlyWeatherSummary.WeatherAddtives,
                    WeatherConditions = weatherConditionServiceObjects
                };

            }
            return weatherObj;
        }

        public override bool Equals(Object obj)
        {
            var typedObj = obj as WeatherDataObject;
            if (typedObj == null)
            {
                return false;
            }

            return (typedObj.CloudCoverPercent == this.CloudCoverPercent && 
                   typedObj.PrecipitationChances == this.PrecipitationChances &&
                   typedObj.SnowfallValue == this.SnowfallValue && 
                   typedObj.Temperature == this.Temperature && 
                   typedObj.MinTemperature == this.MinTemperature && 
                   typedObj.MaxTemperature == this.MaxTemperature && 
                   typedObj.WeatherTime == this.WeatherTime && 
                   this.ThreeHourlyWeatherSummary.Equals(typedObj.ThreeHourlyWeatherSummary));
        }

        public override string ToString()
        {
            return
                string.Format(
                    "Cloud: {0}, Precipitation: {1}, MinTemp: {2}, MaxTemp: {3}, Snowfall: {4}, WeatherTime:{5}, Temp: {6}, ThreeHourly: {7}",
                    this.CloudCoverPercent, this.PrecipitationChances, this.MinTemperature, this.MaxTemperature,
                    this.SnowfallValue, this.WeatherTime,
                    this.Temperature, this.ThreeHourlyWeatherSummary);
        }
    }
}
