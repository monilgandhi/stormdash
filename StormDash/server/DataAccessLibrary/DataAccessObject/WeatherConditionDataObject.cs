﻿using CommunicationSchemas.Schemas.Protobuf;

namespace DataAccessLibrary.DataAccessObject
{
    internal class WeatherConditionDataObject
    {
        public WeatherCoverage weatherCoverage;
        public WeatherIntensity weatherIntensity;
        public WeatherType weatherType;

        public override bool Equals(object obj)
        {
            var typedObj = obj as WeatherConditionDataObject;
            if (typedObj == null)
            {
                return false;
            }

            return this.weatherCoverage == typedObj.weatherCoverage && 
                   this.weatherIntensity == typedObj.weatherIntensity && 
                   this.weatherType == typedObj.weatherType;
        }

        public override string ToString()
        {
            return string.Format("Coverage: {0}, Intensity: {1}, type:{2}", weatherCoverage, weatherIntensity,
                weatherType);
        }
    }
}
