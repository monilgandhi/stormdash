﻿using System;
using System.Collections.Generic;

namespace DataAccessLibrary.DataAccessObject
{
    internal class RouteDataObject
    {
        public LocationDataObject Source;
        public LocationDataObject Destination;
        public TimeSpan JourneyTime;
        public string Summary;
        public double Distance;
        public string Unit;
        public IList<GeoCoordinateDataObject> Path;
    }
}
