﻿using System;
using ServiceSchemas.Schemas;

namespace DataAccessLibrary.DataAccessObject
{
    [Serializable]
    internal abstract class DataObject
    {
        public abstract ServiceObject ToServiceObjecct();
        public abstract ServiceObject PopulateServiceObject(ServiceObject svcObj);
    }
}
