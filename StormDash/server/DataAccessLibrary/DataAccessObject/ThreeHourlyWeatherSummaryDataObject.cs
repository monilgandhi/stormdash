﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommunicationSchemas.Schemas.Protobuf;

namespace DataAccessLibrary.DataAccessObject
{
    class ThreeHourlyWeatherSummary
    {
        public List<WeatherConditionDataObject> WeatherConditions;
        public List<WeatherAdditive> WeatherAddtives;

        public override bool Equals(object obj)
        {
            var typedObj = obj as ThreeHourlyWeatherSummary;

            if (typedObj == null)
            {
                return false;
            }

            if ((this.WeatherAddtives == null && typedObj.WeatherAddtives != null) ||
                (this.WeatherAddtives != null && typedObj.WeatherAddtives == null))
            {
                return false;
            }

            if (this.WeatherConditions == null && typedObj.WeatherConditions != null ||
                this.WeatherConditions != null && typedObj.WeatherConditions == null)
            {
                return false;
            }

            if (this.WeatherConditions == null || this.WeatherAddtives == null)
            {
                return true;
            }

            if (this.WeatherAddtives.Count != typedObj.WeatherAddtives.Count ||
                this.WeatherConditions.Count != typedObj.WeatherConditions.Count)
            {
                return false;
            }

            if (this.WeatherAddtives.Where((t, i) => t != typedObj.WeatherAddtives[i]).Any())
            {
                return false;
            }

            if (this.WeatherConditions.Where((t, i) => !t.Equals(typedObj.WeatherConditions[i])).Any())
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            if (WeatherConditions == null)
            {
                return string.Empty;
            }

            StringBuilder finalString = new StringBuilder();
            for(int i = 0; i < WeatherConditions.Count; i++)
            {
                finalString.Append(string.Format(" condition: {0}", this.WeatherConditions[i].ToString()));
            }

            return finalString.ToString();
        }
    }
}
