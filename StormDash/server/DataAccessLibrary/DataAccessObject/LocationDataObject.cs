﻿using System;
using ServiceSchemas.Schemas;

namespace DataAccessLibrary.DataAccessObject
{
    [Serializable]
    internal class LocationDataObject
    {
        public readonly String City;

        public readonly String State;

        public readonly String Zipcode;

        public readonly GeoCoordinateDataObject Coordinates;

        public LocationDataObject()
        { }
        public LocationDataObject(GeoCoordinateDataObject coordinates,
                                  string city,
                                  string state,
                                  string zip)
        {
            Coordinates = coordinates;
            City = city;
            State = state;
            Zipcode = zip;
        }

        public override string ToString()
        {
            return string.Format("lat,lon: {0},{1};city:{2};state{3};zip{4}", Coordinates.Latitude, Coordinates.Longitude, City, State, Zipcode);
        }
    }
}
