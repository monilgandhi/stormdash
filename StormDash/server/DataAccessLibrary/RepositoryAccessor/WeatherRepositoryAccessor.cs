﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;

namespace DataAccessLibrary.RepositoryAccessor
{
    internal class WeatherRepositoryAccessor : IWeatherRepositoryAccessor
    {
        private IWeatherRepository weatherRepository;
        private ICacheRepositoryAccessor cacheRepositoryaccessor;
        private ILogger logger;

        private const uint REFRESH_RATE = 3600;

        public WeatherRepositoryAccessor(Guid requestGuid, IWeatherRepository weatherRepository)
        {
            this.weatherRepository = weatherRepository;
            cacheRepositoryaccessor = new CacheRepositoryAccessor(requestGuid);
            logger = LogWrapper.GetLogger(this.GetType(), requestGuid);
        }

        public async Task<List<LocationDataObject>> SyncWeather(List<LocationDataObject> locationDataObjs)
        {
            if (locationDataObjs == null || locationDataObjs.Count == 0)
            {
                logger.Debug("Location information is empty for fetching weather");
                throw new ArgumentNullException("locationDataObjs");
            }
            var sourceResults = await weatherRepository.Fetch(locationDataObjs);
            //get the ones that we did not get
            var unfortunateOnes = new List<LocationDataObject>();

            foreach(var locationObj in locationDataObjs)
            {
                if(!sourceResults.ContainsKey(locationObj))
                {
                    unfortunateOnes.Add(locationObj);
                }
            }

            //StoreInCache(sourceResults);
            return unfortunateOnes;
        }

        public async Task<Dictionary<LocationDataObject,WeatherDataObject>> FetchWeather(List<LocationDataObject> locationDataObjs, 
                                                                                         List<ulong> arrivalTime)
        {
            if (locationDataObjs == null || locationDataObjs.Count == 0)
            {
                logger.Debug("Location information is empty for fetching weather");
                throw new ArgumentNullException("locationDataObjs");
            }

            if (arrivalTime == null || arrivalTime.Count == 0)
            {
                logger.Debug("arrivaltime is empty for fetching weather");
                throw new ArgumentNullException("arrivalTime");
            }

            if(locationDataObjs.Count != arrivalTime.Count)
            {
                logger.Debug("Location objects and arrival time count do not match");
                throw new ArgumentException("counts mismatch with locationdataobj and arrival time");
            }

            //this count is used so that we can know which of the location objects failed from cache.
            //we will build another list to get from source
            var locationWeatherCacheMisses = 0;

            //whatever is missed from cache we will retrieve it from source
            //this is the input to the weather repository
            var resultToGetFromSource = new List<LocationDataObject>();

            //this is for retrieving the value once we get from source
            var mappedArrivalTimeForResultFromSource = new Dictionary<LocationDataObject, ulong>();

            var result = new Dictionary<LocationDataObject, WeatherDataObject>();

            var locationCacheTasks = new List<Task<CacheRepositoryResult<SortedDictionary<ulong, WeatherDataObject>>>>();

            var cacheWeatherFetchSw = Stopwatch.StartNew();
            for (int i = 0; i < locationDataObjs.Count; i++)
            {
                var now = Utils.ConverttoUnixTimeStamp(DateTime.Now);
                if(locationDataObjs[i] == null)
                {
                    logger.Warn("location object at position {0} is null",i);
                    continue;
                }

                if(arrivalTime[i] < Utils.ConverttoUnixTimeStamp(DateTime.Now))
                {
                    logger.Warn("Arrival time requested is less than currenttime arrivaltime:{0}, currenttime{1}",arrivalTime[i],now);
                    continue;
                }

                //Previously we used to check for exact time key as well.
                //This did not seem useful since there will be hardly time when the arrival time will match the exact time of the weather

                //try to get the entire location weather since it is possible specific time weather is not present
                var locationSpecificKey = GetKey(locationDataObjs[i]);
                locationCacheTasks.Add(cacheRepositoryaccessor.GetAsync<SortedDictionary<ulong, WeatherDataObject>>(locationSpecificKey));
            }

            await Task.WhenAll(locationCacheTasks.ToArray());

            for (int i = 0; i < locationCacheTasks.Count; i++)
            {
                var locationCacheTask = locationCacheTasks[i];
                if (!locationCacheTask.IsCompleted || locationCacheTask.Result == null)
                {
                    continue;
                }

                var locationSpecificResultCache = locationCacheTask.Result;
                WeatherDataObject requiredWeatherData = null;
                if (locationSpecificResultCache.Status == RepositoryResultEnum.Success && locationSpecificResultCache.ResultData != null &&
                    !IsDataOutDated(locationSpecificResultCache.LastUpdate) &&
                    SelectAppropriateWeather(locationSpecificResultCache.ResultData, arrivalTime[i], out requiredWeatherData))
                {
                    result.Add(locationDataObjs[i], requiredWeatherData);
                }


                //we did not find the data. Fetch it from source
                if (requiredWeatherData == null)
                {
                    ++locationWeatherCacheMisses;
                    //we could not find this location in cache. Get it from source
                    resultToGetFromSource.Add(locationDataObjs[i]);
                    //we need to maintain the map so that we fetch only what is required
                    mappedArrivalTimeForResultFromSource.Add(locationDataObjs[i], arrivalTime[i]);
                }
            }

            this.logger.Performance("Cache weather total time {0} ms", cacheWeatherFetchSw.ElapsedMilliseconds);

            if(locationWeatherCacheMisses > 0)
            {
                logger.Warn("Cache: Location weather misses {0}", locationWeatherCacheMisses);
            }

            if (resultToGetFromSource.Count == 0)
            {
                return result;
            }

            var sourceResults = await weatherRepository.Fetch(resultToGetFromSource);

            var rightResultSw = Stopwatch.StartNew();

            foreach (var locationObj in resultToGetFromSource)
            {
                SortedDictionary<ulong, WeatherDataObject> locationsWeather = null;

                if (!sourceResults.TryGetValue(locationObj, out locationsWeather))
                {
                    //something went wrong
                    //check if this is even possible
                    logger.Debug("Location Weather not fetched {0}", locationObj.ToString());
                    continue;
                }


                if (locationsWeather == null)
                {
                    //something went wrong
                    //check if this is even possible
                    logger.Debug("Value returned null from the weather repository for {0}", locationObj.ToString());
                    continue;
                }

                ulong requiredTime;
                if (!mappedArrivalTimeForResultFromSource.TryGetValue(locationObj, out requiredTime))
                {
                    //this should not happen
                    logger.Error("Unable to find location arrival time entry while fetching from source");
                    continue;
                }

                WeatherDataObject requiredWeather;
                //Get the appropriate weather


                if (!SelectAppropriateWeather(locationsWeather, requiredTime, out requiredWeather))
                {
                    //we could not find this weather. Something is wrong
                    logger.Warn("Unable to find location arrival time entry while fetching from source for {0}",
                        locationObj.ToString());
                    continue;
                }

                result.Add(locationObj, requiredWeather);
            }

            this.logger.Performance("Time taken to retrieve right results: {0} ms", rightResultSw.ElapsedMilliseconds);
            
            //store in the cache
            //await StoreInCache(sourceResults);
            
            return result;
        }

        /// <summary>
        /// This function finds appropriate weather in case exact time match or gets the weather around the required time
        /// </summary>
        /// <param name="weatherObjectList"></param>
        /// <param name="requiredTime"></param>
        /// <returns></returns>
        private bool SelectAppropriateWeather(SortedDictionary<ulong, WeatherDataObject> weatherObjectList, ulong requiredTime, out WeatherDataObject requiredWeatherObject)
        {
            var objectFound = weatherObjectList.TryGetValue(requiredTime, out requiredWeatherObject);
            try
            {
                if (!objectFound)
                {
                    //same time is not present. Probably somewhere between.
                    ulong previousWeatherTimeStamp = 0;
                    WeatherDataObject previousWeather = null;

                    var approximateForLoopTime = Stopwatch.StartNew();
                    foreach (var weatherData in weatherObjectList)
                    {
                        if (weatherData.Key > requiredTime)
                        {
                            logger.Debug("Weather required time {0}, weather fetched for time {1}",
                            Utils.ConvertUnixTimeStampToUniversalDateTime(requiredTime),
                            Utils.ConvertUnixTimeStampToUniversalDateTime(previousWeatherTimeStamp));
                            //we found the weather we want. bail out
                            requiredWeatherObject = previousWeather ?? weatherData.Value;

                            if (requiredWeatherObject.ThreeHourlyWeatherSummary != null && requiredWeatherObject.ThreeHourlyWeatherSummary.WeatherConditions != null)
                            {
                                logger.Debug("Selected weather type is {0}", requiredWeatherObject.ThreeHourlyWeatherSummary.WeatherConditions.First().weatherType);    
                            }
                            else
                            {
                                logger.Debug("Selected weather does not have any weather type");
                            }

                            objectFound = true;
                            break;
                        }
                        previousWeatherTimeStamp = weatherData.Key;
                        previousWeather = weatherData.Value;
                    }
                    // this.logger.Performance("Time taken to select approximate weather loop for {1} elements: {0} ms", approximateForLoopTime.ElapsedMilliseconds, weatherObjectList.Count);
                }

            }
            catch (Exception)
            {
                System.Diagnostics.Debugger.Break();
                throw;
            }
            
            return objectFound;
        }

        private async Task StoreInCache(Dictionary<LocationDataObject,SortedDictionary<ulong,WeatherDataObject>> locationsWeather)
        {
            foreach(var locationKeyValuePair in locationsWeather)
            {
                var currentLocation = locationKeyValuePair.Key;
                var currentLocationWeather = locationKeyValuePair.Value;
                if (currentLocation == null || currentLocationWeather == null)
                {
                    logger.Warn("Key/Value of locations weather is null. Skipping");
                    continue;
                }

                var currentTime = Utils.ConverttoUnixTimeStamp(DateTime.Now);
                //store location directly
                await cacheRepositoryaccessor.SetAsync(GetKey(currentLocation), currentLocationWeather, 0, currentTime + REFRESH_RATE);
            }
        }

        /// <summary>
        /// Get the weather for a specific location
        /// </summary>
        /// <param name="locationDataObj"></param>
        /// <returns></returns>
        private String GetKey(LocationDataObject locationDataObj)
        {
            var key = locationDataObj.City.Replace(" ", "") + "_" + locationDataObj.State + "_" + locationDataObj.Zipcode;
            return key;
        }


        /// <summary>
        /// Get the weather for a specific location at specific time
        /// </summary>
        /// <param name="locationDataObj"></param>
        /// <param name="arrivalTime"></param>
        /// <returns></returns>
        private String GetKey(LocationDataObject locationDataObj, ulong arrivalTime)
        {
            var key = locationDataObj.City.Replace(" ", "") + "_" + locationDataObj.State + "_" + locationDataObj.Zipcode + "_" + arrivalTime;
            return key;
        }

        private bool IsDataOutDated(ulong lastUpdatedTime)
        {
            var dataOutDated =  (Utils.ConverttoUnixTimeStamp(DateTime.Now) - lastUpdatedTime) > REFRESH_RATE;

            if (dataOutDated)
            {
                this.logger.Debug("Need refresh of weather data");
            }

            return dataOutDated;
        }
    }
}
