﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccess;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
using StackExchange.Redis;

namespace DataAccessLibrary.RepositoryAccessor
{
    internal class CacheRepositoryAccessor : ICacheRepositoryAccessor
    {
        private const bool IsCacheTurnedOn = false;
        private readonly ICacheRepository cacheRepository;
        private bool connectionFailure;
        private TimeSpan lastConnectionFailure;
        private ILogger logger;
        public CacheRepositoryAccessor(Guid requestGuid)
        {
            this.cacheRepository = new RedisCacheRepository(requestGuid);
            this.logger = LogWrapper.GetLogger(GetType(), requestGuid);
        }

        /// <summary>
        /// Set the value in cache. This function is mostly fire and forget. Do not wait for this function unless you want to verify
        /// </summary>
        /// <typeparam name="T">Type of object to store. Could be anything</typeparam>
        /// <param name="key">Key to store</param>
        /// <param name="obj">Actual object</param>
        /// <param name="timeout">Timeout for the object</param>
        /// <param name="lastUpdateTime">The last update time</param>
        public async Task<CacheRepositoryResult<bool>> SetAsync<T>(string key, T obj, ulong timeout = 0, ulong lastUpdateTime = 0)
        {
            if (String.IsNullOrEmpty(key) || obj == null)
            {
                throw new ArgumentNullException("key");
            }

            var result = new CacheRepositoryResult<bool>();
            result.Status = RepositoryResultEnum.Failure;

            if (!IsCacheTurnedOn)
            {
                return result;
            }

            if (this.connectionFailure)
            {
                var difference = (DateTime.Now.TimeOfDay - this.lastConnectionFailure);
                this.logger.Debug("Last connection failure occurred {0} milliseconds before. Not retrying", difference.TotalMilliseconds);
                return result;
            }

            TimeSpan? timeoutTs = null;
            var nowUtcTimeStamp = Utils.ConverttoUnixTimeStamp(DateTime.Now);

            if(timeout > 0)
            {
                if (nowUtcTimeStamp > timeout)
                {
                    //this data is already expired. log and exit
                    this.logger.Debug("Expired data. Skipping key {0} currenttime: {1}", key, nowUtcTimeStamp);
                    result.ResultData = true;
                    result.Status = RepositoryResultEnum.Success;
                    return result;
                }

                //convert the current time to UTC since timeout is already in utc
                var timeDifference = timeout - Utils.ConverttoUnixTimeStamp(DateTime.Now);
                timeoutTs = TimeSpan.FromSeconds(timeDifference);
            }

            try
            {
                if(lastUpdateTime > 0)
                {
                    lastUpdateTime = Utils.ConverttoUnixTimeStamp(DateTime.Now);
                }

                var cacheObject = new CacheDataObject<T>
                {
                    Data = obj,
                    LastUpdate = lastUpdateTime
                };

                result.ResultData = await this.cacheRepository.Set<T>(key, cacheObject, timeoutTs);
                result.Status = RepositoryResultEnum.Success;
            }
            catch (RedisConnectionException)
            {
                this.logger.Error("Unable to connect to redis server. Any further requests will not be entertained");
                this.connectionFailure = true;
                this.lastConnectionFailure = DateTime.Now.TimeOfDay;
            }
            catch (Exception ex)
            {
                this.logger.Error("Unknown exception occurred {0}", ex.Message);
                result.Status = RepositoryResultEnum.Failure;
            }

            return result;
        }

        public async Task<CacheRepositoryResult<T>> GetAsync<T>(string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            var result = new CacheRepositoryResult<T>();
            result.Status = RepositoryResultEnum.Failure;

            if (!IsCacheTurnedOn)
            {
                return result;
            }

            if (this.connectionFailure)
            {
                var difference = (DateTime.Now.TimeOfDay - this.lastConnectionFailure);
                this.logger.Debug("Last connection failure occurred {0} milliseconds before. Not retrying", difference.TotalMilliseconds);
                return result;
            }

            var requestSw = Stopwatch.StartNew();
            try
            {
                var cacheResult = await this.cacheRepository.Get<T>(key);
                result.ResultData = cacheResult.Data;
                result.LastUpdate = cacheResult.LastUpdate;
                result.Status = RepositoryResultEnum.Success;
            }
            catch (InvalidCastException)
            {
                this.logger.Error("Unable to cast object with key {0} to type {1}", key, typeof(T).ToString());
                result.Status = RepositoryResultEnum.Failure;
            }
            catch(RedisConnectionException)
            {
                this.logger.Error("Unable to connect to redis server. Any further requests will not be entertained");
                this.connectionFailure = true;
                this.lastConnectionFailure = DateTime.Now.TimeOfDay;
            }
            catch (Exception ex)
            {
                this.logger.Error("Unknown exception occurred {0}", ex.Message);
                result.Status = RepositoryResultEnum.Failure;
            }
            finally
            {
                if(requestSw.ElapsedMilliseconds > 0)
                {
                    this.logger.Performance("Time taken for cache request {0} ms", requestSw.ElapsedMilliseconds);
                }
            }
            return result;
        }
    }
}
