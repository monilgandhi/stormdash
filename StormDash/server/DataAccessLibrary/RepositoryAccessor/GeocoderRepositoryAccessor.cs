﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CommonUtils;
using DataAccessLibrary.DataAccess;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
namespace DataAccessLibrary.RepositoryAccessor
{
    internal class GeocoderRepositoryAccessor : IGeocoderRepositoryAccessor
    {
        private IGeocoderRepository geocoderRepository;
        private ICacheRepositoryAccessor cacheRepositoryAccessor;
        private ILogger logger;
        public GeocoderRepositoryAccessor(Guid requestGuid) 
        {
            geocoderRepository = new PostgisGeocodeRespository(requestGuid);
            cacheRepositoryAccessor = new CacheRepositoryAccessor(requestGuid);
            logger = LogWrapper.GetLogger(this.GetType(), requestGuid);
        }

        /// <summary>
        /// This function takes in list of lat and lon and reverse geocodes them
        /// In case if there was an error, like zip code not found or empty city, we add null object.
        /// This will ensure we return same count of location at lat and lon.
        /// </summary>
        /// <param name="coordinateList">Coordinate list</param>
        /// <param name="source">Source object</param>
        /// <param name="destination">Destination object</param>
        /// <returns>List of location data object</returns>
        public async Task<List<LocationDataObject>> ReverseGeocodeAsync(List<GeoCoordinateDataObject> coordinateList, 
                                                                        LocationDataObject source = null, 
                                                                        LocationDataObject destination = null)
        {
            if (coordinateList == null || coordinateList.Count == 0)
            {
                logger.Error("coordinates object are empty or null");
                throw new ArgumentNullException("coordinateList");
            }

            string key = null;
            List<RepositoryResult<LocationDataObject>> result = null;
            if(source != null && destination != null)
            {
                key = GetCacheKey(source, destination);
            }

            List<LocationDataObject> locationList = null;
            if(!String.IsNullOrEmpty(key))
            {
                //look inside the cache
                var cacheTask = await cacheRepositoryAccessor.GetAsync<List<LocationDataObject>>(key);
                if (cacheTask != null && cacheTask.Status != RepositoryResultEnum.Failure && cacheTask.ResultData != null)
                {
                    locationList = cacheTask.ResultData;
                    return locationList;
                }
            }
            
            //not found in cache. get from the source and save it to cache
            var sw = Stopwatch.StartNew();
            result = await geocoderRepository.ReverseGeocodeAsync(coordinateList);
            logger.Performance("Time taken by reversegeocoding for {0} points is {1}ms", coordinateList.Count, sw.ElapsedMilliseconds);

            locationList = new List<LocationDataObject>();
            var errorCount = 0;
            var latlonIterator = 0;
            foreach (var location in result)
            {
                if (location.Status == RepositoryResultEnum.Failure)
                {
                    //something went wrong with this location. log it and skip it
                    logger.Debug("GeocoderRepositoryAccessor:Error occurred for location coordinates:{0},{1}",
                                        coordinateList[latlonIterator].Longitude, coordinateList[latlonIterator].Latitude);
                    ++errorCount;
                }

                locationList.Add(location.ResultData);
                ++latlonIterator;
            }

            if (errorCount > 0)
            {
                logger.Warn("GeocoderRepositoryAccessor:Errors occurred while reverse geocoding {0}", errorCount);
            }

            if (errorCount == locationList.Count)
            {
                //no point caching the error results
                return locationList;
            }

            if(source == null)
            {
                source = locationList.First();
            }
            
            if(destination == null)
            {
                destination = locationList.Last();
            }
            

            //store in the cache
            if(String.IsNullOrEmpty(key))
            {
                key = GetCacheKey(source, destination);
            }

            if(!String.IsNullOrEmpty(key))
            {
                await cacheRepositoryAccessor.SetAsync<List<LocationDataObject>>(key, locationList);
            }
            return locationList;
        }

        private String GetCacheKey(LocationDataObject source, LocationDataObject destination)
        {
            // TODO measure the impact. IF too slow add caching
            return string.Empty;
            if(String.IsNullOrEmpty(source.City) || String.IsNullOrEmpty(source.State) ||
               String.IsNullOrEmpty(destination.City) || String.IsNullOrEmpty(destination.State))
            {
                return null;
            }

            return source.City + "_" + source.State + "_" + destination.City + "_" + destination.State;
        }
    }
}
