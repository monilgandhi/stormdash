﻿using System;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeatherApp.Communication;

namespace DataAccessLibrary.Test
{
    [TestClass]
    public class TestFeedbackServiceManager
    {
        private FeedbackRepository feedbackRepository;

        [TestInitialize]
        public void Initialize()
        {
            this.feedbackRepository = new FeedbackRepository();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task TestNullFeedback()
        {
            await this.feedbackRepository.PostFeedbackAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task TestemptyFeedback()
        {
            var feedback = new FeedbackPostRequest();
            await this.feedbackRepository.PostFeedbackAsync(feedback);
        }
    }
}
