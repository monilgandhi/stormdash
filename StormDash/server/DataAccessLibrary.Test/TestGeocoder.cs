﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
using DataAccessLibrary.RepositoryAccessor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DataAccessLibrary.Exceptions;

namespace DataAccessLibrary.Test
{
    [TestClass]
    public class TestGeocoder
    {
        private IGeocoderRepositoryAccessor geocoder;
        private List<GeoCoordinateDataObject> coordinates;
        private List<LocationDataObject> output;

        private static string FolderName = "\\GeocoderRepository\\";

        private void SetUpInputs(bool nullCoordinates = false,
                                 string latLonFile = "")
        {
            this.geocoder = new GeocoderRepositoryAccessor(Guid.NewGuid());

            var latList = new List<float>();
            var lonList = new List<float>();
            this.coordinates = new List<GeoCoordinateDataObject>();

            if(!String.IsNullOrEmpty(latLonFile))
            {
                var filePath = TestUtils.GetTestFilesFolderLocation() + FolderName + latLonFile;
                TestUtils.ReadLatLon(ref latList, ref lonList, filePath);
                
                for(var i = 0; i < latList.Count; i++)
                {
                    var coordinate = new GeoCoordinateDataObject(latList[i], lonList[i]);
                    this.coordinates.Add(coordinate);
                }
            }

            if (nullCoordinates)
            {
                this.coordinates = null;
            }
        }

        private async Task Execute()
        {
            this.output = await this.geocoder.ReverseGeocodeAsync(this.coordinates);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task TestNullInputs()
        {
            SetUpInputs(true);
            await Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task TestEmptyLatList()
        {
            SetUpInputs();
            await Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ReverseGeocodingException))]
        public async Task TestReverseGeocodingOutOfbounds()
        {
            SetUpInputs(latLonFile: "reversergeocodeOutofbounds.xml");
            await Execute();
        }

        [TestMethod]
        public async Task TestReverseGeocoding()
        {
            SetUpInputs(latLonFile: "reversegeocodeLocation.xml");
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(3, this.output.Count);
            
            //Idaho falls
            var location = this.output[0];
            Assert.IsNotNull(location);
            Assert.AreEqual("Idaho Falls", location.City);
            Assert.AreEqual("ID", location.State);
            Assert.AreEqual("83402", location.Zipcode);

            //Idaho fall
            location = this.output[1];
            Assert.IsNotNull(location);
            Assert.AreEqual("Pierre", location.City);
            Assert.AreEqual("SD", location.State);
            Assert.AreEqual("57501", location.Zipcode);


            //Idaho falls
            location = this.output[2];
            Assert.IsNotNull(location);
            Assert.AreEqual("Fairbanks", location.City);
            Assert.AreEqual("AK", location.State);
            Assert.AreEqual("99703", location.Zipcode);
        }

    }
}
