﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Interface;
using DataAccessLibrary.Parser;

namespace DataAccessLibrary.Test.Mocks
{
    internal class MockWeatherRepository : IWeatherRepository
    {
        private string weatherFilePath;
        private string fileName;
        public MockWeatherRepository(string weatherFilePath, string fileName)
        {
            this.weatherFilePath = weatherFilePath;
            this.fileName = fileName;
        }

        public async Task<Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>>> Fetch(
            List<LocationDataObject> locationObjs)
        {
            var result = new Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>>();
            switch (this.fileName)
            {
                case "EmptyResponse.xml":
                    return result;

                case "EmptyWeather.xml":
                    foreach (var location in locationObjs)
                    {
                        result.Add(location, new SortedDictionary<ulong, WeatherDataObject>());
                    }
                    return result;
                    
                default:
                    var fileToXmlStream = TestUtils.ReadXmlStream(this.weatherFilePath);
                    IWeatherParser noaaParser = new NoaaWeatherParser(fileToXmlStream, locationObjs, "testUrl", Guid.NewGuid());
                    return noaaParser.ParseWeatherXml();
            }
        }
    }
}
