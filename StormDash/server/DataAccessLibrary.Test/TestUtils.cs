﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using DataAccessLibrary.DataAccessObject;

namespace DataAccessLibrary.Test
{
    internal static class TestUtils
    {
        /// <summary>
        /// This location is from the debug folder
        /// </summary>
        private static String TestFolderLocation = @"\TestFiles\";

        /// <summary>
        /// Date time to use for required time
        /// NOTE: This time is in UTC. So PST will be 13:00:00
        /// </summary>
        private static String DateTimeForTestWeatherXml = "2018-04-13 20:00:00";
        public static string GetTestFilesFolderLocation()
        {
            var currentPath = Directory.GetCurrentDirectory();

            return currentPath + TestFolderLocation;
        }

        public static void ReadLatLon(ref List<float> latList, ref List<float> lonList, string fileName)
        {
            if(String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException(fileName);
            }

            if(latList == null)
            {
                latList = new List<float>();
            }

            if (lonList == null)
            {
                lonList = new List<float>();
            }

            var xmlRoot = XDocument.Load(fileName);

            if(xmlRoot == null)
            {
                return;
            }

            var locationsElement = xmlRoot.Descendants("location");

            if(locationsElement == null || !locationsElement.Any())
            {
                throw new XmlException("Location tag not present. Check your xml");
            }

            foreach(var locationElement in locationsElement)
            {
                if(String.IsNullOrEmpty(locationElement.Value))
                {
                    throw new XmlException("lat,lon pair cannot be empty");
                }

                var latlon = locationElement.Value.Split(',');
                if(latlon.Length != 2)
                {
                    throw new XmlException("lat,lon pair format incorrect");
                }
                float lat,lon;
                if(!float.TryParse(latlon[0], out lat) || !float.TryParse(latlon[1], out lon))
                {
                    throw new XmlException("Lat or lon are invalid");
                }

                latList.Add(lat);
                lonList.Add(lon);
            }
        }

        public static List<LocationDataObject> LoadLocationDataObject(string fileName, string folderName)
        {
            var filePath = GetTestFilesFolderLocation() + folderName + "\\" + fileName;
            return LoadLocationDataObject(filePath);
        }

        public static  List<LocationDataObject> LoadLocationDataObject(string filePath)
        {
            var result = new List<LocationDataObject>();
            var xml = XDocument.Load(filePath);
            
            if(xml == null)
            {
                throw new FileLoadException("Unable to load the file for reading xml");
            }

            var locations = xml.Descendants("location");

            if(locations == null)
            {
                throw new XmlException("location no found");
            }

            foreach(var locationElement in locations)
            {
                var latitudeElementValue = locationElement.Element("latitude").Value;
                var longitudeElementValue = locationElement.Element("longitude").Value;
                var cityName = locationElement.Element("city").Value;
                var stateName = locationElement.Element("state").Value;
                var zipStr = locationElement.Element("zip").Value;

                float lat, lon;
                if(!float.TryParse(latitudeElementValue, out lat) || 
                   !float.TryParse(longitudeElementValue, out lon)||
                   String.IsNullOrEmpty(zipStr))
                {
                    throw new XmlException("Unable to convert latitude/longitude/zip");
                }

                var geocoordinate = new GeoCoordinateDataObject(lat, lon);
                var location = new LocationDataObject(geocoordinate, cityName, stateName, zipStr);
                result.Add(location);
            }
            return result;
        }

        public static List<ulong> GenerateRequiredTimeListWRTNow(int count)
        {
            var result = new List<ulong>();
            var dateTime = DateTime.Parse(DateTimeForTestWeatherXml);
            for (int i = 1; i <= count; i++ )
            {
                result.Add(CommonUtils.Utils.ConverttoUnixTimeStampInSameTimeZone(dateTime));
            }

            return result;
        }

        public static FileStream ReadXmlStream(string fileName, string folderName)
        {
            var filePath = GetTestFilesFolderLocation() + folderName + "\\" + fileName;
            return ReadXmlStream(filePath);
        }

        public static FileStream ReadXmlStream(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException(filePath);
            }

            var fs = File.OpenRead(filePath);
            return fs;
        }
    }
}
