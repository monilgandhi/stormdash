﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.RepositoryAccessor;
using DataAccessLibrary.Test.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataAccessLibrary.Test
{
    [TestClass]
    public class TestWeatherRepositoryAccessor
    {
        private WeatherRepositoryAccessor wRepository;
        private List<LocationDataObject> locationDataObj;
        private List<ulong> requiredTime;
        private static string LocationDataTestFilePath = @"location.xml";
        private static string TestFileFolder = "\\WeatherRepository\\";

        private Dictionary<LocationDataObject,WeatherDataObject> output;

        private void SetupInput(bool nullLocationObj = false,
                                bool emptyLocationObj = false,
                                bool nullRequiredTimeList = false,
                                bool emptyRequiredTimeList = false,
                                bool mismatchBetweenLocationAndRequiredTime = false,
                                string fileName = "MultipleCitiesWeather.xml")
        {
            var requestGuid = Guid.NewGuid();
            var filePath = TestUtils.GetTestFilesFolderLocation() + TestFileFolder + fileName;
            this.wRepository = new WeatherRepositoryAccessor(requestGuid, new MockWeatherRepository(filePath, fileName));

            if(!nullLocationObj)
            {
                this.locationDataObj = new List<LocationDataObject>();

                if(!emptyLocationObj)
                {
                    this.locationDataObj = TestUtils.LoadLocationDataObject(TestUtils.GetTestFilesFolderLocation() + TestFileFolder + "location.xml");
                }
            }

            if(!nullRequiredTimeList)
            {
                this.requiredTime = new List<ulong>();

                if(!emptyRequiredTimeList && this.locationDataObj != null)
                {
                    int count = this.locationDataObj.Count;
                    if(mismatchBetweenLocationAndRequiredTime)
                    {
                        count = this.locationDataObj.Count - 1;
                    }

                    this.requiredTime = TestUtils.GenerateRequiredTimeListWRTNow(count);
                }
            }
        }

        private async Task Execute()
        {
            this.output = await this.wRepository.FetchWeather(this.locationDataObj, this.requiredTime);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task NullLocationObjectList()
        {
            SetupInput(nullLocationObj:true);
            await Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task EmptyLocationObjectList()
        {
            SetupInput(emptyLocationObj: true);
            await Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task NullRequiredTimeList()
        {
            SetupInput(nullRequiredTimeList:true);
            await Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task EmptyRequiredTimeList()
        {
            SetupInput(emptyRequiredTimeList: true);
            await Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task MismatchWithLocationAndRequiredTime()
        {
            SetupInput(mismatchBetweenLocationAndRequiredTime: true);
            await Execute();
        }

        [TestMethod]
        public async Task ParserFailureEmptyResponse()
        {
            this.SetupInput(fileName: @"EmptyResponse.xml");
            await this.Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(0, this.output.Count);
        }

        [TestMethod]
        public async Task ParserFailureEmptyWeather()
        {
            this.SetupInput(fileName: @"EmptyWeather.xml");
            await this.Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(0, this.output.Count);

        }

        /// <summary>
        /// This test will test all location at 1300 hours
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ValidateRepositoryOutput()
        {
            SetupInput();
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(this.locationDataObj.Count, this.output.Count);
            foreach(var locationItem in this.locationDataObj)
            {
                Assert.IsTrue(this.output.ContainsKey(locationItem));

                WeatherDataObject weatherObject = null;

                this.output.TryGetValue(locationItem, out weatherObject);

                switch(locationItem.City)
                {
                    //arrival of time here is 1PM
                    case "Seattle":
                        //max temperature
                        Assert.AreEqual((short)56, weatherObject.MaxTemperature);

                        //min temperature
                        Assert.AreEqual((short)46, weatherObject.MinTemperature);

                        //cloud cover
                        Assert.AreEqual((ushort)100, weatherObject.CloudCoverPercent);

                        //percipitation
                        Assert.AreEqual((ushort)92, weatherObject.PrecipitationChances);

                        //temperature
                        Assert.AreEqual((short)52, weatherObject.Temperature);

                        //snow
                        Assert.AreEqual(0.0f, weatherObject.SnowfallValue);
                        break;

                    case "San Francisco":
                        //max temperature
                        Assert.AreEqual((short)61, weatherObject.MaxTemperature);

                        //min temperature
                        Assert.AreEqual((short)51, weatherObject.MinTemperature);

                        //cloud cover
                        Assert.AreEqual((ushort)31, weatherObject.CloudCoverPercent);

                        //percipitation
                        Assert.AreEqual((ushort)1, weatherObject.PrecipitationChances);

                        //temperature
                        Assert.AreEqual((short)58, weatherObject.Temperature);

                        //snow
                        Assert.AreEqual(0.0f, weatherObject.SnowfallValue);
                        break;

                    case "New york":
                        //max temperature
                        Assert.AreEqual((short)67, weatherObject.MaxTemperature);

                        //min temperature
                        Assert.AreEqual((short)47, weatherObject.MinTemperature);

                        //cloud cover
                        Assert.AreEqual((ushort)27, weatherObject.CloudCoverPercent);

                        //percipitation
                        Assert.AreEqual((ushort)3, weatherObject.PrecipitationChances);

                        //temperature
                        Assert.AreEqual((short)66, weatherObject.Temperature);

                        //snow
                        Assert.AreEqual(0.0f, weatherObject.SnowfallValue);
                        break;

                    case "Chicago":
                        //max temperature
                        Assert.AreEqual((short)59, weatherObject.MaxTemperature);

                        //min temperature
                        Assert.AreEqual((short)51, weatherObject.MinTemperature);

                        //cloud cover
                        Assert.AreEqual((ushort)35, weatherObject.CloudCoverPercent);

                        //percipitation
                        Assert.AreEqual((ushort)53, weatherObject.PrecipitationChances);

                        //temperature
                        Assert.AreEqual((short)58, weatherObject.Temperature);

                        //snow
                        Assert.AreEqual(0.0f, weatherObject.SnowfallValue);
                        break;

                    case "Minneapolis":
                        //max temperature
                        Assert.AreEqual((short)62, weatherObject.MaxTemperature);

                        //min temperature
                        Assert.AreEqual((short)44, weatherObject.MinTemperature);

                        //cloud cover
                        Assert.AreEqual((ushort)15, weatherObject.CloudCoverPercent);

                        //percipitation
                        Assert.AreEqual((ushort)0, weatherObject.PrecipitationChances);

                        //temperature
                        Assert.AreEqual((short)58, weatherObject.Temperature);

                        //snow
                        Assert.AreEqual(0.0f, weatherObject.SnowfallValue);
                        break;
                }
            }
            
        }
    }
}
