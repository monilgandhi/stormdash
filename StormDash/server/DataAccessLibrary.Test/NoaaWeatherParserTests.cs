﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using CommonUtils;
using DataAccessLibrary.DataAccessObject;
using DataAccessLibrary.Exceptions;
using DataAccessLibrary.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommunicationSchemas.Schemas.Protobuf;

namespace DataAccessLibrary.Test
{
    [TestClass]
    public class NoaaWeatherParserTests
    {
        private static string FolderName = "NoaaWeatherParser";
        private FileStream xmlStream;
        private List<LocationDataObject> locationDataObject;
        private Dictionary<LocationDataObject, SortedDictionary<ulong, WeatherDataObject>> output; 

        private async Task Execute()
        {
            var noaaParser = new NoaaWeatherParser(this.xmlStream, this.locationDataObject, "testUrl", Guid.NewGuid());
            this.output = noaaParser.ParseWeatherXml();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            this.xmlStream.Dispose();
        }
        
        [ExpectedException(typeof(WeatherParserException))]
        [TestMethod]
        public async Task TestErrorXml()
        {
            this.xmlStream = TestUtils.ReadXmlStream("ErrorXml.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("location.xml", FolderName);
            await Execute();
        }

        [TestMethod]
        public async Task ValidateMinimumTemperature()
        {
            this.xmlStream = TestUtils.ReadXmlStream("MinimumTemp.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("location.xml", FolderName);
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(1, this.output.Count);

            var cityWeather = this.output.FirstOrDefault().Value;
            Assert.IsNotNull(cityWeather);
            Assert.AreEqual(6, cityWeather.Count);

            var currentUlongTime = Utils.ConverttoUnixTimeStampInSameTimeZone(DateTime.Parse("2016-03-13T20:00:00").AddHours(4));
            const int incrementHours = 24;
            var expectedMinimumTemperature = new List<short>() {46, 46, 51, 51, 45, 41};
            int next = 0;
            foreach (var cityWeatherMinimumTemp in cityWeather)
            {
                Assert.IsNotNull(cityWeatherMinimumTemp.Value);
                Assert.AreEqual(currentUlongTime, cityWeatherMinimumTemp.Key);
                Assert.IsNotNull(cityWeatherMinimumTemp.Value.MinTemperature);
                Assert.AreEqual(expectedMinimumTemperature[next++], cityWeatherMinimumTemp.Value.MinTemperature);
                currentUlongTime += (incrementHours * 3600);
            }
        }

        [TestMethod]
        public async Task ValidateMaximumTemperature()
        {
            this.xmlStream = TestUtils.ReadXmlStream("MaximumTemperature.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("location.xml", FolderName);
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(1, this.output.Count);

            var cityWeather = this.output.FirstOrDefault().Value;
            Assert.IsNotNull(cityWeather);

            var expectedMaximumTemperature = new List<short>() { 50, 49, 50, 52, 54, 57, 53 };
            Assert.AreEqual(expectedMaximumTemperature.Count, cityWeather.Count);

            var currentUlongTime = Utils.ConverttoUnixTimeStampInSameTimeZone(DateTime.Parse("2016-03-13T08:00:00").AddHours(7));
            const int incrementHours = 24;
            
            int next = 0;
            foreach (var cityWeatherMaximumTemp in cityWeather)
            {
                Assert.IsNotNull(cityWeatherMaximumTemp.Value);
                Assert.AreEqual(currentUlongTime, cityWeatherMaximumTemp.Key);
                Assert.IsNotNull(cityWeatherMaximumTemp.Value.MaxTemperature);
                Assert.AreEqual(expectedMaximumTemperature[next++], cityWeatherMaximumTemp.Value.MaxTemperature);
                currentUlongTime += (incrementHours * 3600);
            }
        }

        [TestMethod]
        public async Task ValidateCloudCoverage()
        {
            this.xmlStream = TestUtils.ReadXmlStream("CloudCoverage.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("location.xml", FolderName);
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(1, this.output.Count);

            var cityWeather = this.output.FirstOrDefault().Value;
            Assert.IsNotNull(cityWeather);

            var expectedCloudCoverage = new List<ushort>() { 98, 93, 93, 93, 93, 90, 90, 90, 90, 91, 91, 91, 91, 82, 82, 82, 82, 75, 75, 72, 72, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 52, 52, 84 };
            Assert.AreEqual(expectedCloudCoverage.Count, cityWeather.Count);
             
            this.xmlStream.Position = 0;

            var xmlRoot = XDocument.Load(this.xmlStream);
            var xmlDateTime = xmlRoot.Descendants("start-valid-time").ToList();

            int next = 0;
            foreach (var cityWeatherCloudCoverage in cityWeather)
            {
                var currentUlongTime = ExtractStartTime(xmlDateTime[next].Value);
                Assert.IsNotNull(cityWeatherCloudCoverage.Value);
                Assert.AreEqual(currentUlongTime, cityWeatherCloudCoverage.Key);
                Assert.IsNotNull(cityWeatherCloudCoverage.Value.CloudCoverPercent);
                Assert.AreEqual(expectedCloudCoverage[next++], cityWeatherCloudCoverage.Value.CloudCoverPercent);
            }
        }

        private ulong ExtractStartTime(string dateTimeString)
        {
            if (string.IsNullOrWhiteSpace(dateTimeString))
            {
                throw new NullReferenceException("dateTimeString");
            }

            var dtsPosEnd = dateTimeString.LastIndexOf('-');
            DateTime localTime, utcTimeDifference;
            if (!DateTime.TryParse(dateTimeString.Substring(0, dtsPosEnd), out localTime))
            {
                throw new FormatException("dateTimeString");
            }

            if (!DateTime.TryParse(dateTimeString.Substring(dtsPosEnd + 1), out utcTimeDifference))
            {
                throw new FormatException("utcTimeDifference");
            }

            //this time is in UTC
            return Utils.ConverttoUnixTimeStampInSameTimeZone(localTime.AddHours(utcTimeDifference.Hour));
        }

        [TestMethod]
        public async Task Validate12HourlyPercipitation()
        {
            this.xmlStream = TestUtils.ReadXmlStream("Percipitation.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("location.xml", FolderName);
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(1, this.output.Count);

            var cityWeather = this.output.FirstOrDefault().Value;
            Assert.IsNotNull(cityWeather);

            var expectedPercipitationProbability = new List<ushort>() { 89, 84, 61, 76, 41, 57, 25, 13, 13, 13, 13, 13, 23 };
            Assert.AreEqual(expectedPercipitationProbability.Count, cityWeather.Count);

            var currentUlongTime = Utils.ConverttoUnixTimeStampInSameTimeZone(DateTime.Parse("2016-03-13T05:00:00").AddHours(7));
            const int incrementHours = 12;
            
            int next = 0;
            foreach (var cityWeatherPercipitation in cityWeather)
            {
                Assert.IsNotNull(cityWeatherPercipitation.Value);
                Assert.AreEqual(currentUlongTime, cityWeatherPercipitation.Key);
                Assert.IsNotNull(cityWeatherPercipitation.Value.PrecipitationChances);
                Assert.AreEqual(expectedPercipitationProbability[next++], cityWeatherPercipitation.Value.PrecipitationChances);
                currentUlongTime += (incrementHours * 3600);
            }
        }

        [TestMethod]
        public async Task ValidateWeatherConditionDetail()
        {
            this.xmlStream = TestUtils.ReadXmlStream("WeatherCondition.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("location.xml", FolderName);
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(1, this.output.Count);

            this.xmlStream.Position = 0;
            var xmlRoot = XDocument.Load(this.xmlStream);
            var xmlDateTime = xmlRoot.Descendants("start-valid-time").ToList();
            var weatherconditionDescendants = xmlRoot.Descendants("weather-conditions").ToList();

            var cityWeather = this.output.FirstOrDefault().Value;
            Assert.IsNotNull(cityWeather);
            Assert.AreEqual(xmlDateTime.Count, cityWeather.Count);

            var next = 0;

            foreach (var cityWeatherSummary in cityWeather)
            {
                var currentUlongTime = ExtractStartTime(xmlDateTime[next].Value);
                Assert.IsNotNull(cityWeatherSummary.Value);
                Assert.AreEqual(currentUlongTime, cityWeatherSummary.Key);
                var currentValueNodes = weatherconditionDescendants[next++].Descendants("value").ToList();

                if (currentValueNodes.Count == 0)
                {
                    Assert.IsNotNull(cityWeatherSummary.Value.ThreeHourlyWeatherSummary);
                    Assert.IsNull(cityWeatherSummary.Value.ThreeHourlyWeatherSummary.WeatherConditions);
                    continue;
                }

                Assert.AreEqual(currentValueNodes.Count, cityWeatherSummary.Value.ThreeHourlyWeatherSummary.WeatherConditions.Count);

                for (int i = 0; i < currentValueNodes.Count; i++)
                {
                    var valueNode = currentValueNodes[i];

                    var expectedWeatherType = valueNode.Attribute("weather-type").Value.ToLower().Replace(' ', '_');
                    Assert.AreEqual(expectedWeatherType, cityWeatherSummary.Value.ThreeHourlyWeatherSummary.WeatherConditions[i].weatherType.ToString().ToLower());

                    var expectedCoverage = valueNode.Attribute("coverage").Value.ToLower().Replace(' ', '_');
                    Assert.AreEqual(expectedCoverage, cityWeatherSummary.Value.ThreeHourlyWeatherSummary.WeatherConditions[i].weatherCoverage.ToString().ToLower());

                    var expectedIntensity = valueNode.Attribute("intensity").Value.ToLower().Replace(' ', '_');
                    Assert.AreEqual(expectedIntensity, cityWeatherSummary.Value.ThreeHourlyWeatherSummary.WeatherConditions[i].weatherIntensity.ToString().ToLower());
                }
            }
        }

        [TestMethod]
        public async Task TestMultipleLocationWeatherSingleTime()
        {
            this.xmlStream = TestUtils.ReadXmlStream("MultipleLocationSingleDay.xml", FolderName);
            this.locationDataObject = TestUtils.LoadLocationDataObject("Multiplelocations.xml", FolderName);
            await Execute();
            Assert.IsNotNull(this.output);
            Assert.AreEqual(this.locationDataObject.Count, this.output.Count);

            foreach (var city in this.output)
            {
                Assert.IsNotNull(city.Value);
                Assert.IsTrue(city.Value.Count > 0);

                ulong requiredTimeStamp;
                WeatherDataObject expectedWeather = null;
                WeatherDataObject actualWeather = null;
                List<WeatherConditionDataObject> expectedWeatherList = null;

                switch (city.Key.City)
                {
                    case "Seattle":
                        requiredTimeStamp =
                            Utils.ConverttoUnixTimeStampInSameTimeZone(DateTime.Parse("2016-03-14 18:00:00"));
                        expectedWeather =
                            new WeatherDataObject(requiredTimeStamp)
                            {
                                CloudCoverPercent = (ushort) 90,
                                MinTemperature = (short) 46,
                                MaxTemperature = (short) 50,
                                PrecipitationChances = (ushort) 61,
                            };

                        expectedWeatherList = new List<WeatherConditionDataObject>();

                        expectedWeatherList.Add(new WeatherConditionDataObject()
                        {
                            weatherCoverage = WeatherCoverage.Definitely,
                            weatherIntensity = WeatherIntensity.Light,
                            weatherType = WeatherType.Rain_Showers
                        });

                        expectedWeatherList.Add(new WeatherConditionDataObject()
                        {
                            weatherCoverage = WeatherCoverage.Slight_Chance,
                            weatherIntensity = WeatherIntensity.Very_Light,
                            weatherType = WeatherType.Snow_Showers
                        });

                        expectedWeather.ThreeHourlyWeatherSummary = new ThreeHourlyWeatherSummary()
                        {
                            WeatherAddtives = new List<WeatherAdditive>() {WeatherAdditive.And},
                            WeatherConditions = expectedWeatherList
                        };

                        Assert.IsTrue(city.Value.TryGetValue(requiredTimeStamp, out actualWeather));

                        Assert.IsNotNull(actualWeather);
                        Assert.AreEqual(expectedWeather, actualWeather);
                        break;

                    case "Pierre":
                        requiredTimeStamp =
                            Utils.ConverttoUnixTimeStampInSameTimeZone(DateTime.Parse("2016-03-15 03:00:00"));
                        expectedWeather =
                            new WeatherDataObject(requiredTimeStamp)
                            {
                                CloudCoverPercent = (ushort)24,
                                MinTemperature = (short)49,
                                MaxTemperature = (short)67,
                                PrecipitationChances = (ushort)32,
                                ThreeHourlyWeatherSummary = new ThreeHourlyWeatherSummary()
                            };

                        Assert.IsTrue(city.Value.TryGetValue(requiredTimeStamp, out actualWeather));

                        Assert.IsNotNull(actualWeather);
                        Assert.AreEqual(expectedWeather, actualWeather);
                        Assert.IsNotNull(actualWeather.ThreeHourlyWeatherSummary);
                        break;
                }
            }
        }
    }
}
