﻿
using CommunicationSchemas.Schemas.Protobuf;

namespace ServiceSchemas.Schemas
{
    public class WeatherConditionServiceObject
    {
        public WeatherCoverage weatherCoverage;
        public WeatherIntensity weatherIntensity;
        public WeatherType weatherType;
    }
}
