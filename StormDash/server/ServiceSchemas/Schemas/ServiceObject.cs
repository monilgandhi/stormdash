﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Communication.Interface;

namespace ServiceSchemas.Schemas
{
    public abstract class ServiceObject
    {
        public abstract ICommunicationObject ToCommunicationObject();
    }
}
