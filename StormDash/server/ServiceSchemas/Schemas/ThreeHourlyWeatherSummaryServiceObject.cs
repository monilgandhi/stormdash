﻿using System.Collections.Generic;
using CommunicationSchemas.Schemas.Protobuf;
using WeatherApp.Communication;
using WeatherApp.Communication.Interface;

namespace ServiceSchemas.Schemas
{
    public class ThreeHourlyWeatherSummaryServiceObject : ServiceObject
    {
        public List<WeatherConditionServiceObject> WeatherConditions;
        public List<WeatherAdditive> WeatherAddtives;

        public override ICommunicationObject ToCommunicationObject()
        {
            var threehourlyCommObject = new ThreeHourlyWeatherSummary()
            {
                WeatherAddtives = this.WeatherAddtives,
                WeatherConditions = new List<WeatherCondition>()
            };

            foreach (var weathercondition in this.WeatherConditions)
            {
                threehourlyCommObject.WeatherConditions.Add( new WeatherCondition()
                {
                    weatherCoverage = weathercondition.weatherCoverage,
                    weatherIntensity = weathercondition.weatherIntensity,
                    weatherType = weathercondition.weatherType
                });
            }

            return threehourlyCommObject;
        }
    }
}
