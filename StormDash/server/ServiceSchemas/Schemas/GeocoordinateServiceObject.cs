﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Communication;

namespace ServiceSchemas.Schemas
{
    public class GeocoordinateServiceObject : ServiceObject
    {
        public float Latitude;
        public float Longitude;

        public GeocoordinateServiceObject(Geocoordinate coordinate)
        {
            if(coordinate == null)
            {
                throw new ArgumentNullException("coordinates cannot be null");
            }

            if(coordinate.Latitude == 0 || coordinate.Longitude == 0)
            {
                throw new ArgumentException("Latitude and longitude cannot be 0");
            }

            Latitude = coordinate.Latitude;
            Longitude = coordinate.Longitude;
        }

        public GeocoordinateServiceObject(float latitude, float longitude)
        {
            if(latitude == 0 || longitude == 0)
            {
                throw new ArgumentException("Latitude and longitude cannot be 0");
            }
            Latitude = latitude;
            Longitude = longitude;
        }

        public override WeatherApp.Communication.Interface.ICommunicationObject ToCommunicationObject()
        {
            var obj = new Geocoordinate()
            {
                Latitude = this.Latitude,
                Longitude = this.Longitude
            };
            return obj;
        }
    }
}
