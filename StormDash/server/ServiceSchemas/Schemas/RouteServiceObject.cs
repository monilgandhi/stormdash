﻿using System;
using System.Collections.Generic;

namespace ServiceSchemas.Schemas
{
    public class RouteServiceObject
    {
        public LocationInformationServiceObject Source;
        public LocationInformationServiceObject Destination;
        public TimeSpan JourneyTime;
        public string Summary;
        public double Distance;
        public string Unit;
        public IList<GeocoordinateServiceObject> Path;

    }
}
