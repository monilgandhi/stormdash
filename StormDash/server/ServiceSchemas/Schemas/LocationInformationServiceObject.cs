﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Communication;

namespace ServiceSchemas.Schemas
{
    public class LocationInformationServiceObject : ServiceObject
    {
        public String City { get; set; }

        public String State { get; set; }

        public String Zipcode { get; set; }
        public GeocoordinateServiceObject Coordinates { get; set; }
        public WeatherServiceObject Weather { get; set; }
        public double DistanceFromSource { get; set; }

        public LocationInformationServiceObject() {}

        /// <summary>
        /// Initializes LocationInformationServiceObject from location object in communication
        /// schemas
        /// </summary>
        /// <param name="location">Communication location object</param>
        /// <param name="distanceFromSource">Distance from source</param>
        public LocationInformationServiceObject(Location location, double distanceFromSource = 0)
        {
            if(location == null)
            {
                throw new ArgumentNullException("location is null");
            }

            City = location.City;
            State = location.State;
            Zipcode = location.ZipCode;

            Coordinates = new GeocoordinateServiceObject(location.Latitude, location.Longitude);
            DistanceFromSource = distanceFromSource;
        }

        /// <summary>
        /// Initialized LocationInformationServiceObject without weather object
        /// </summary>
        /// <param name="coordinates"> Coordinates required</param>
        /// <param name="distanceFromSource"> Distance from source if any</param>
        public LocationInformationServiceObject(Geocoordinate coordinates, double distanceFromSource = 0)
        {
            if (coordinates == null)
            {
                throw new ArgumentNullException("coordinates");
            }

            Coordinates = new GeocoordinateServiceObject(coordinates);
            DistanceFromSource = distanceFromSource;
        }

        /// <summary> 
        /// Initializes required LocationInformationServiceObject with weather object 
        /// </summary>
        /// <param name="coordinates">Coordinates of location</param>
        /// <param name="requiredTime">Required time of the weather</param>
        /// <param name="distanceFromSource">Distance from the source</param>
        public LocationInformationServiceObject(Geocoordinate coordinates, ulong requiredTime, double distanceFromSource = 0)
        {
            if(coordinates == null)
            {
                throw new ArgumentNullException("coordinates");
            }

            Coordinates = new GeocoordinateServiceObject(coordinates);
            DistanceFromSource = distanceFromSource;

            if(requiredTime > 0)
            {
                Weather = new WeatherServiceObject(requiredTime);
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(City);
            sb.Append(",");
            sb.Append(State);
            sb.Append(",");
            sb.Append(Zipcode);
            sb.Append(",");
            sb.Append(Coordinates.Longitude);
            sb.Append(",");
            sb.Append(Coordinates.Latitude);
            return sb.ToString();
        }

        public override WeatherApp.Communication.Interface.ICommunicationObject ToCommunicationObject()
        {
            var locObj = new LocationInformation()
            {
                City = this.City,
                State = this.State,
                ZipCode = this.Zipcode,
                DistanceFromStartPoint = this.DistanceFromSource,
                Coordinates = this.Coordinates.ToCommunicationObject() as Geocoordinate,
                Weather = this.Weather.ToCommunicationObject() as WeatherInformation,
                ApproximateArrivalTime = this.Weather.RequiredTime
            };

            return locObj;
        }
 
    }
}
