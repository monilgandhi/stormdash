﻿using System;
using WeatherApp.Communication;
using WeatherApp.Communication.Interface;

namespace ServiceSchemas.Schemas
{
    public class WeatherServiceObject : ServiceObject
    {
        public ulong RequiredTime;
        public ushort? PrecipitationChances;

        public float? SnowfallValue;

        public Int16? MinTemperature;

        public Int16? MaxTemperature;

        public Int16? Temperature;

        public UInt16? CloudCoverPercent;
        public ThreeHourlyWeatherSummaryServiceObject ThreeHourlyWeatherSummaryServiceObject;

        public override ICommunicationObject ToCommunicationObject()
        {
            var weatherObj = new WeatherInformation()
            {
                PrecipitationChances = this.PrecipitationChances,
                SnowfallValue = this.SnowfallValue,
                MinTemperature = this.MinTemperature,
                MaxTemperature = this.MaxTemperature,
                Temperature = this.Temperature,
                CloudCoverPercentage = this.CloudCoverPercent,
                ValidTime = this.RequiredTime,
                ThreeHourlyWeatherSummary = this.ThreeHourlyWeatherSummaryServiceObject == null ? null : 
                this.ThreeHourlyWeatherSummaryServiceObject.ToCommunicationObject() as ThreeHourlyWeatherSummary
            };

            
            
            return weatherObj;
        }

        public WeatherServiceObject(ulong requiredTime)
        {
            if(requiredTime == 0)
            {
                throw new ArgumentException("Required time cannot be zero");
            }

            RequiredTime = requiredTime;
        }

    }
}
