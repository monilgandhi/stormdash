﻿using System;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CommonUtils
{
    internal static class LogTableManager
    {
        private static CloudTable table = Initialize("stormdashlogs");

        public static CloudTable Initialize(string tableName)
        {
            var connectionsString = ConfigurationManager.ConnectionStrings["TableStorage"].ConnectionString;

            CloudStorageAccount account = null;

            if (CloudStorageAccount.TryParse(connectionsString, out account))
            {
                var tableClient = account.CreateCloudTableClient();
                return tableClient.GetTableReference(tableName);   
            }

            return null;
        }

        private static void InsertLog(LogEntity entity)
        {
            var operation = TableOperation.Insert(entity);
            if (table != null)
            {
                table.Execute(operation);
            }
        }

        public static void LogError(string message, Guid requestId, Type owner)
        {
            var logEntity = new LogEntity(LogType.Error, owner, requestId, message);
            InsertLog(logEntity);
        }

        public static void LogWarning(string message, Guid requestId, Type owner)
        {
            var logEntity = new LogEntity(LogType.Warning, owner, requestId, message);
            InsertLog(logEntity);
        }
        public static void LogPerformance(string message, Guid requestId, Type owner)
        {
            var logEntity = new LogEntity(LogType.Performance, owner, requestId, message);
            InsertLog(logEntity);
        }
        public static void LogDebug(string message, Guid requestId, Type owner)
        {
            var logEntity = new LogEntity(LogType.Debug, owner, requestId, message);
            InsertLog(logEntity);
        }
    }
}