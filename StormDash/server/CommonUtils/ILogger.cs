﻿using System;

namespace CommonUtils
{
    public interface ILogger
    {
        Guid RequestGuid { get; }
        void Debug(string message, params object[] args);
        void Performance(string message, params object[] args);
        void Warn(string message, params object[] args);
        void Error(string message, params object[] args);
    }
}
