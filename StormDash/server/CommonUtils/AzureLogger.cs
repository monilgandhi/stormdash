﻿using System;
namespace CommonUtils
{
    internal class AzureLogger : ILogger
    {
        private Type owner;
        private Guid requestGuid;

        public Guid RequestGuid
        {
            get { return this.requestGuid; }
        }
        public AzureLogger(Type owner, Guid requestGuid)
        {
            this.owner = owner;
            this.requestGuid = requestGuid;
        }

        private bool IsInputInvalid(String message)
        {
            return String.IsNullOrWhiteSpace(message);
        }

        public void Error(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            LogTableManager.LogError(string.Format(message, args), this.requestGuid, owner);
        }

        public void Warn(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            LogTableManager.LogWarning(string.Format(message, args), this.requestGuid, owner);

        }

        public void Debug(string message, params object[] args)
        {
            return;
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            LogTableManager.LogDebug(string.Format(message, args), this.requestGuid, owner);
        }

        public void Performance(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            LogTableManager.LogPerformance(string.Format(message, args), this.requestGuid, owner);
        }

    }
}
