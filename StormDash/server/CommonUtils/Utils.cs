﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Communication;

namespace CommonUtils
{
    public static class Utils
    {
        public static ulong ConverttoUnixTimeStamp(DateTime time)
        {
            return (ulong)time.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static DateTime ConvertUnixTimeStampToUniversalDateTime(ulong timestamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(timestamp);
            return dtDateTime;
        }

        /// <summary>
        /// This function does the conversion of the time stamp in the same time zone as provided
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static ulong ConverttoUnixTimeStampInSameTimeZone(DateTime time)
        {
            return (ulong)time.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static bool AreCoordinateEqual(float fCoordinate, float sCoordinate)
        {
            return Math.Abs(fCoordinate - sCoordinate) > 0.009;
        }

        public static List<Geocoordinate> DecodeGoogleMapsPoints(string encodedPoints)
        {
            char[] polylineChars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;

            var result = new List<Geocoordinate>();
            while (index < polylineChars.Length)
            {
                int next5bits;
                int sum = 0;
                int shifter = 0;
                // calculate next latitude
                do
                {
                    next5bits = (int)polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                //calculate next longitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5bits = (int)polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length && next5bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                var latitude = Convert.ToDouble(currentLat) / 1E5;
                var longitude = Convert.ToDouble(currentLng) / 1E5;
                var geocoordinate = new Geocoordinate()
                {
                    Latitude = (float)latitude,
                    Longitude = (float)longitude
                };

                result.Add(geocoordinate);
            }

            return result;
        }
    }
}
