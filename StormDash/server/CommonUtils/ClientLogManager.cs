﻿using CommunicationSchemas.Schemas.Protobuf;
using Microsoft.WindowsAzure.Storage.Table;

namespace CommonUtils
{
    public static class ClientLogManager
    {
        private static CloudTable table = LogTableManager.Initialize("stormdashclientlogs");

        public static void Log(ClientLog log)
        {
            if (log == null) return;
            ClientLogEntity entity = null;
            
            switch (log.ErrorType)
            {
                case ClientErrorType.Error:
                    entity = new ClientLogEntity(log.ErrorType, log.Platform, log.StackTrace, log.Message, log.ClassName, log.ExceptionType);
                    break;
            }

            if (entity != null)
            {
                InsertLog(entity);
            }
        }

        private static void InsertLog(ClientLogEntity entity)
        {
            var operation = TableOperation.Insert(entity);
            if (table != null)
            {
                table.Execute(operation);
            }
        }
    }
}
