﻿namespace CommonUtils
{
    internal enum LogType
    {
        Error = 1,
        Warning,
        Debug,
        Performance
    }
}
