﻿using System;
using System.Configuration;

namespace CommonUtils
{
    public static class LogWrapper
    {
        public static ILogger GetLogger(Type type, Guid requestGuid)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["useLog4net"]) || 
                !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Test"]))
            {
                return new Log4NetLogger(type, requestGuid);    
            }

            return new AzureLogger(type, requestGuid);    
        }

        public static ILogger GetClientLogger(Type type, Guid requestGuid)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["useLog4net"]) ||
                !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Test"]))
            {
                return new Log4NetLogger(type, requestGuid);
            }

            return new AzureLogger(type, requestGuid);
        }

        public static Guid GetNewguid()
        {
            return Guid.NewGuid();
        }
    }
}
