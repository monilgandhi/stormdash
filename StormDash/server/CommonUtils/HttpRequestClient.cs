﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace CommonUtils
{
    public class HttpRequestClient
    {
        public async Task<Stream> GetResponse(string url, string method = "GET")
        {
            if (String.IsNullOrEmpty(url))
            {
                //TODO something went wrong, retry
                throw new ArgumentNullException("url");
            }

            var request = WebRequest.Create(url);
            /*WebProxy proxy = new WebProxy(url);
            request.Proxy = proxy;*/
            request.Method = method;

            try
            {
                var response = await request.GetResponseAsync();
                if (response.ContentLength == 0)
                {
                    throw new WebException("No response received for url " + url);
                }
              
                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    throw new EndOfStreamException("Response stream is empty for url " + url);
                }
                return responseStream;

            }
            catch(Exception ex)
            {
            }

            return null;

        }
    }

}
