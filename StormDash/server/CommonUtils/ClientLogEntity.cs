﻿using System;
using CommunicationSchemas.Schemas.Protobuf;
using Microsoft.WindowsAzure.Storage.Table;

namespace CommonUtils
{
    public class ClientLogEntity : TableEntity
    {
        public ClientLogEntity(ClientErrorType logType, String clientPlatform, string stackTrace, string log, string className, string exceptionType)
        {
            this.PartitionKey = logType.ToString();
            this.RowKey = Guid.NewGuid().ToString();
            this.Log = log;
            this.StackTrace = stackTrace;
            this.ClientPlatform = clientPlatform;
            this.ClassName = className;
            this.ExceptionType = exceptionType;
        }

        public string ExceptionType { get; set; }
        public string ClassName { get; set; }
        public string StackTrace { get; set; }
        public string ClientPlatform { get; set; }
        public string Log { get; set; }
    }
}
