﻿using System;
using log4net;

namespace CommonUtils
{
    internal class Log4NetLogger : ILogger
    {
        private ILog log;
        private Guid requestGuid;

        public Guid RequestGuid
        {
            get { return this.requestGuid; }
        }
        public Log4NetLogger(Type type, Guid requestGuid)
        {
            log4net.Config.XmlConfigurator.Configure();
            this.requestGuid = requestGuid;
            ThreadContext.Properties["EventID"] = this.requestGuid.ToString();
            this.log = LogManager.GetLogger(type);
        }
        private bool IsInputInvalid(String message)
        {
            return String.IsNullOrWhiteSpace(message);
        }

        public void Debug(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            if(this.log.IsDebugEnabled)
            {
                var formatMsg = String.Format(message, args);
                this.log.Debug(formatMsg);
            }
        }

        public void Performance(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            if(this.log.IsInfoEnabled)
            {
                var formatMsg = String.Format(message, args);
                this.log.Info(formatMsg);
            }
        }

        public void Warn(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            if(this.log.IsWarnEnabled)
            {
                var formatMsg = String.Format(message, args);
                this.log.Warn(formatMsg);
            }
            
        }

        public void Error(string message, params object[] args)
        {
            if (IsInputInvalid(message))
            {
                throw new ArgumentNullException("message");
            }

            if (this.log.IsErrorEnabled)
            {
                var formatMsg = String.Format(message, args);
                this.log.Error(formatMsg);
            }
        }
    }
}
