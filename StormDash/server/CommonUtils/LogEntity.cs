﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace CommonUtils
{
    internal class LogEntity : TableEntity
    {
        public LogEntity(LogType logType, Type owner, Guid requestId, string log)
        {
            this.PartitionKey = logType.ToString();
            this.RowKey = Guid.NewGuid().ToString();
            this.Log = log;
            this.RequestId = requestId;
            this.ClassName = owner.FullName;
        }

        public Guid RequestId { get; set; }
        public string ClassName { get; set; }
        public string Log { get; set; }
    }
}
