﻿using WeatherApp.Communication.Interface;

namespace WeatherAppService.Models
{
    public class ServiceResponse<T> where T : ICommunicationObject
    {
        public readonly T Result;
        public readonly ResponseCode ResponseCode;

        public ServiceResponse(T obj, ResponseCode responseCode)
        {
            this.Result = obj;
            this.ResponseCode = responseCode;
        }
    }
}
