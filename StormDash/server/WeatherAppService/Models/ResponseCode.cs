﻿namespace WeatherAppService.Models
{
    public enum ResponseCode
    {
        Success,
        BadRequest,
        InternalServerError
    }
}