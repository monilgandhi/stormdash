﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CommonUtils;
using WeatherApp.Communication;
using WeatherAppService.CommunicationSerializers;
using WeatherAppService.ServiceManager;

[assembly:InternalsVisibleTo("WeatherAppService.Tests")]
namespace WeatherAppService.Managers
{
    internal class WeatherFetcher : BaseManager
    {
        public WeatherFetcher(Stream requestStream, Guid requestGuid)
            : base(requestStream, requestGuid)
        {}

        public async Task<List<LocationInformation>> FetchWeatherForRoute()
        {
            List<LocationInformation> loctionInfo = new List<LocationInformation>();

            var routeInfo = CommunicationSerializer.Deserialize<RouteInformation>(request);
            logger.Debug("Requested route from {0} to {1} starting at {2} utc", routeInfo.Source.City, routeInfo.Destination.City, Utils.ConvertUnixTimeStampToUniversalDateTime(routeInfo.DateUTC));

            var routeGeocoder = new WeatherDataServiceManager(routeInfo, this.logger.RequestGuid);

            //call the data services and get the mapLocation info
            loctionInfo = await routeGeocoder.GetCitiesWeather();
            if (loctionInfo.Count == 0)
            {
                logger.Error("location info returned is empty");
            }

            return loctionInfo;
        }
    }
}