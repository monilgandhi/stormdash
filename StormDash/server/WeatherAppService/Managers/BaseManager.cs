﻿using System;
using System.IO;
using CommonUtils;
namespace WeatherAppService.Managers
{
    public abstract class BaseManager
    {
        protected ILogger logger;
        protected readonly Stream request;
        protected BaseManager(Stream requestStream, Guid requestGuid)
        {
            logger = LogWrapper.GetLogger(GetType(), requestGuid);
            logger.Debug("Received request");
            if (requestStream == null || requestStream.Length == 0)
            {
                logger.Error("Request stream is empty or null");
                throw new ArgumentNullException("requestStream");
            }
            request = requestStream;
        }
    }
}