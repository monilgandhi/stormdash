﻿using System;
using System.IO;
using System.Threading.Tasks;
using WeatherApp.Communication;
using WeatherAppService.CommunicationSerializers;
using WeatherAppService.ServiceManager;

namespace WeatherAppService.Managers
{
    public class ReverseGeocodingManager : BaseManager
    {
        private readonly GeocodeServiceManager routeServiceManager;
        private RouteInformation routeInfo;

        public ReverseGeocodingManager(Stream requestStream, Guid requestGuid) : base(requestStream, requestGuid)
        {
            routeInfo = CommunicationSerializer.Deserialize<RouteInformation>(request);

            if (routeInfo == null || string.IsNullOrWhiteSpace(routeInfo.RoutePoints))
            {
                logger.Error("Routeinfo is null or route points are absent");
                throw new ArgumentException("Routeinfo is null on deserializing request stream");
            }

            routeServiceManager = new GeocodeServiceManager(routeInfo, requestGuid);
        }

        public async Task<bool> ReverseGeocodeRouteAsync()
        {
            
            var success = await routeServiceManager.ReverseGeocodeAsync();
            return success;
        }
    }
}