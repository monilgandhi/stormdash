﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using CommonUtils;
using DataAccessLibrary;
using ServiceSchemas.Schemas;
using WeatherApp.Communication;

namespace WeatherAppService.ServiceManager
{
    internal abstract class BaseServiceManager
    {
        protected ILogger logger;

        protected List<Geocoordinate> validCoordinates;
        protected RouteInformation routeInfo;
        protected Repository repository;
        protected List<double> distanceBetweenPointFromSource;
        protected List<LocationInformationServiceObject> serviceObjects;
        protected LocationInformationServiceObject source;
        protected LocationInformationServiceObject destination;

        protected List<ulong> timeOfArrival;

        public BaseServiceManager(RouteInformation route, Guid requestGuid)
        {
            logger = LogWrapper.GetLogger(this.GetType(), requestGuid);
            if (route == null)
            {
                throw new ArgumentNullException("route");
            }

            if (string.IsNullOrWhiteSpace(route.RoutePoints))
            {
                throw new ArgumentException("RouteInfo has some invalid values");
            }

            routeInfo = route;
            distanceBetweenPointFromSource = new List<double>();
            repository = new Repository(requestGuid);
        }

        private float DetermineDistanceBetweenCities()
        {
            //10 miles
            if (routeInfo.TotalDistance <= 32186.9f)
            {
                //1 mile
                return 1609.34f;
            }

            //50 miles or less
            if (routeInfo.TotalDistance <= 80467.2)
            {
                //2 miles
                return 3218.69f;
            }

            //75 miles or less
            if (routeInfo.TotalDistance <= 120701)
            {
                //3 miles
                return 4828.03f;
            }

            //5 miles
            return 8046.72f;
        }
        protected abstract void InitializeServiceObjects();
        protected abstract void CreateServiceObjects();
        protected void GetRequiredPoints()
        {
            validCoordinates = new List<Geocoordinate>();
            var routePoints = Utils.DecodeGoogleMapsPoints(routeInfo.RoutePoints);    
            //We need to tranform this so that we can find distance between two cities
            var startPoint = new GeoCoordinate(routePoints[0].Latitude, routePoints[0].Longitude);

            // add the source
            validCoordinates.Add(routePoints[0]);
            distanceBetweenPointFromSource.Add(0);

            double distanceBetweenCities = DetermineDistanceBetweenCities();
            logger.Debug("Distance between cities selected is {0}", distanceBetweenCities);

            var actualDistanceFromStartPoint = 0.0;
            var previousPoint = startPoint;

            logger.Debug("Total number of route points {0}", routePoints.Count);

            foreach (var endCoordinate in routePoints)
            {
                var endPoint = new GeoCoordinate(endCoordinate.Latitude, endCoordinate.Longitude);
                var distanceFromStartPoint = startPoint.GetDistanceTo(endPoint);

                //we do this because the haversine formula calculates crow flying distance and not road distance
                actualDistanceFromStartPoint += previousPoint.GetDistanceTo(endPoint);
                if (distanceFromStartPoint > distanceBetweenCities)
                {
                    validCoordinates.Add(endCoordinate);
                    distanceBetweenPointFromSource.Add(actualDistanceFromStartPoint);
                    startPoint = endPoint;
                }
                previousPoint = endPoint;
            }

    
            // add the destination
            validCoordinates.Add(routePoints.Last());
            distanceBetweenPointFromSource.Add(routeInfo.TotalDistance);
            logger.Debug("Total number of points to be reverse geocoded {0}", validCoordinates.Count);
        }
    }
}