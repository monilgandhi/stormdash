﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ServiceSchemas.Schemas;
using WeatherApp.Communication;

namespace WeatherAppService.ServiceManager
{
    internal class GeocodeServiceManager :BaseServiceManager
    {
        private List<LocationInformationServiceObject> routePoints;
      
        public GeocodeServiceManager(RouteInformation routeInfo, Guid requestGuid): base(routeInfo, requestGuid)
        {
        }

        public async Task<bool> ReverseGeocodeAsync()
        {
            //convert to service objects
            InitializeServiceObjects();

            var result = await repository.ReverseGeocodeAsync(serviceObjects, source, destination);
            return result;
        }

        protected override void InitializeServiceObjects()
        {
            GetRequiredPoints();
            CreateServiceObjects();
        }
        protected override void CreateServiceObjects()
        {
            if(routeInfo.Source != null)
            {
                source = new LocationInformationServiceObject(routeInfo.Source, 0);
            }

            if(routeInfo.Destination != null)
            {
                destination = new LocationInformationServiceObject(routeInfo.Destination, routeInfo.TotalDistance);
            }

            serviceObjects = new List<LocationInformationServiceObject>();
            for (int i = 0; i < validCoordinates.Count; i++)
            {
                var currentValidCoordinate = validCoordinates[i];
                var distanceFromSource = distanceBetweenPointFromSource[i];

                var coordinateServiceObject = new GeocoordinateServiceObject(currentValidCoordinate);

                var locationServiceObject = new LocationInformationServiceObject(currentValidCoordinate, 0, distanceFromSource);
                serviceObjects.Add(locationServiceObject);
            }
        }
    }
}