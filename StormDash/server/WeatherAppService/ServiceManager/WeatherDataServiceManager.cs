﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Communication;
using ServiceSchemas.Schemas;

namespace WeatherAppService.ServiceManager
{
    internal class WeatherDataServiceManager : BaseServiceManager
    {
        public WeatherDataServiceManager(RouteInformation route, Guid requestGuid) : base(route, requestGuid)
        {
        }

        protected override void InitializeServiceObjects()
        {
            //get the source and destination
            if (routeInfo.Source != null)
            {
                source = new LocationInformationServiceObject(routeInfo.Source, 0);
            }

            if (routeInfo.Destination != null)
            {
                destination = new LocationInformationServiceObject(routeInfo.Destination, routeInfo.TotalDistance);
            }

            //we need not consider all the points. Only points at certain distance are considered
            GetRequiredPoints();
            //Get the arrival time for each of the points
            DetermineArrivalTime();

            var requiredLat = new List<float>();
            var requiredLon = new List<float>();

            foreach (var point in validCoordinates)
            {
                requiredLat.Add(point.Latitude);
                requiredLon.Add(point.Longitude);
            }

            //just a basic check
            if (requiredLat.Count != requiredLon.Count && requiredLat.Count != timeOfArrival.Count())
            {
                logger.Error("The count of lat,lon ({0}) and time of arrival ({1}) do not match", requiredLat.Count, timeOfArrival.Count);
                throw new ApplicationException("The count of lat,lon and time of arrival do not match");
            }
            CreateServiceObjects();

        }

        public async Task<List<LocationInformation>> GetCitiesWeather()
        {
            InitializeServiceObjects();
            var result = new List<LocationInformation>();
            var serviceResponse = await repository.GetWeatherAsync(serviceObjects,source,destination);

            foreach(var serviceObject in serviceResponse)
            {
                var commObj = serviceObject.ToCommunicationObject() as LocationInformation;
                result.Add(commObj);
            }
            return result;
        }

        protected override void CreateServiceObjects()
        {
            serviceObjects = new List<LocationInformationServiceObject>();
            for (int i = 0; i < validCoordinates.Count; i++ )
            {
                var currentValidCoordinate = validCoordinates[i];
                var currentRequiredTime = timeOfArrival[i];
                var distanceFromSource = distanceBetweenPointFromSource[i];

                var locationServiceObject = new LocationInformationServiceObject(currentValidCoordinate, currentRequiredTime, distanceFromSource);
                serviceObjects.Add(locationServiceObject);
            }
        }

        private void DetermineArrivalTime()
        {
            timeOfArrival = new List<ulong>();

            //convert to miles
            var totalDistance = routeInfo.TotalDistance * 0.00062;
            var totalMinutes = (routeInfo.EstimatedTime.NumberofHours * 60) + routeInfo.EstimatedTime.NumberofMinutes;
            //figure out time for each of the points
            var averageSpeedinMinutes = totalDistance / totalMinutes;
            var averageSpeed = averageSpeedinMinutes * 60;

            ulong previousCityTime = routeInfo.DateUTC;

            //add the first one by default
            timeOfArrival.Add(previousCityTime);

            for (int i = 1; i < validCoordinates.Count; i++)
            {
                //get distance between cities in miles
                var distanceBetweenCities = (distanceBetweenPointFromSource[i] - distanceBetweenPointFromSource[i-1]);

                var timeOfArrivalForCurrentCity = CalculateArrivalUnixTime(previousCityTime, averageSpeed, distanceBetweenCities);
                //calculate the arrival time
                timeOfArrival.Add(timeOfArrivalForCurrentCity);   
                previousCityTime = timeOfArrivalForCurrentCity;
            }

        }

        private ulong CalculateArrivalUnixTime(ulong previousCityArrivalTime, double averageSpeed, double distanceFromPreviousCity)
        {
            var timeTaken = (ulong)Math.Round((((distanceFromPreviousCity * 0.00062) / averageSpeed) * 3600), 0);
            return previousCityArrivalTime + timeTaken;
        }
    }
}