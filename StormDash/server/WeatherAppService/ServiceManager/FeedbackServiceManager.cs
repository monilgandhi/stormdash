﻿using System;
using System.Threading.Tasks;
using DataAccessLibrary;
using WeatherApp.Communication;

namespace WeatherAppService.ServiceManager
{
    public class FeedbackServiceManager
    {
        public async Task<bool> SubmitFeedbackAsync(FeedbackPostRequest FeedbackPostRequest, Guid requestGuid)
        {
            if(string.IsNullOrWhiteSpace(FeedbackPostRequest?.Feedback))
            {
                throw new ArgumentNullException(nameof(FeedbackPostRequest));
            }

            var repository = new Repository(requestGuid);
            return await repository.PostFeedbackAsync(FeedbackPostRequest);
        }
    }
}