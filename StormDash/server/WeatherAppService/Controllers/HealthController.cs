﻿using System.Web.Http;

namespace WeatherAppService.Controllers
{
    public class HealthController : ApiController
    {
        // GET api/<controller>
        public string Get()
        {
            return "Health OK";
        }
    }
}