﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CommonUtils;
using WeatherAppService.Managers;

namespace WeatherAppService.Controllers
{ 
    public class GeocodeController : ApiController
    {
        private ILogger logger;
        private async Task<Stream> GetRequestStream()
        {
            return await Request.Content.ReadAsStreamAsync();
        }

        /// <summary>
        /// This api is used to reverse geocode the route and get no response back.
        /// Hence this function will not wait.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PreReverseGeocodeRoute()
        {
            this.logger = LogWrapper.GetLogger(this.GetType(), LogWrapper.GetNewguid());
            var requestStream = GetRequestStream().Result;
            var reverGeocodingManager = new ReverseGeocodingManager(requestStream, this.logger.RequestGuid);

            //this is best effort. Do not wait. Just return
            reverGeocodingManager.ReverseGeocodeRouteAsync();
            

            HttpResponseMessage result = new HttpResponseMessage();
            result.StatusCode = HttpStatusCode.OK;
            return result;

        }
        // POST api/geocodebatch
        /*[HttpPost]
        public async Task<HttpResponseMessage> GeocodeBatch()
        {
            var requestStream = Request.Content.ReadAsStreamAsync().Result;
            var geocodeAddresses = CommunicationSerializer.Deserialize<GeocodeAddress>(requestStream);

            var geocodedResult = await RouteGeocoder.GeocodeBatchAsync(geocodeAddresses.RequestedAddress);
            HttpResponseMessage result = new HttpResponseMessage();
            if (geocodedResult.Count == 0)
            {
                //TODO error log it
                result = Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            else
            {
                MemoryStream ms = new MemoryStream();
                foreach (var coordinate in geocodedResult)
                {
                    CommunicationSerializer.Serialize(coordinate, ms);
                }
                ms.Position = 0;
                if (ms.Length > 0)
                {
                    result = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content = new StreamContent(ms)
                    };
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentLength = ms.Length;
                }
            }
            return result;
        }*/
    }
}
