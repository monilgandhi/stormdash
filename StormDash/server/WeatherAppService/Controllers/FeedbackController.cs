﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CommonUtils;
using WeatherApp.Communication;
using WeatherAppService.CommunicationSerializers;
using WeatherAppService.ServiceManager;

namespace WeatherAppService.Controllers
{
    public class FeedbackController : ApiController
    {
        private ILogger logger;
        private async Task<Stream> GetRequestStream()
        {
            return await Request.Content.ReadAsStreamAsync();
        }

        /// <summary>
        /// This api is used to reverse geocode the route and get no response back.
        /// Hence this function will not wait.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Submit ()
        {
            this.logger = LogWrapper.GetLogger(this.GetType(), LogWrapper.GetNewguid());
            var requestStream = GetRequestStream().Result;
            var feedbackPostRequest = CommunicationSerializer.Deserialize<FeedbackPostRequest>(requestStream);
            var feedbackServiceManager = new FeedbackServiceManager();
            FeedbackPostResponse responseContent = new FeedbackPostResponse();
            
            HttpResponseMessage result = new HttpResponseMessage();
            try
            {
                await feedbackServiceManager.SubmitFeedbackAsync(feedbackPostRequest, this.logger.RequestGuid);
                result.StatusCode = HttpStatusCode.OK;
                responseContent.Success = true;
            }
            catch (ArgumentNullException)
            {
                result.StatusCode = HttpStatusCode.BadRequest;
            }
            catch (ArgumentException)
            {
                result.StatusCode = HttpStatusCode.BadRequest;
            }
            catch (Exception)
            {
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            var resultStream = new MemoryStream();
            CommunicationSerializer.Serialize(responseContent, resultStream);
            resultStream.Position = 0;
            result.Content = new StreamContent(resultStream);
            return result;
        }
    }
}
