﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CommonUtils;
using WeatherApp.Communication;
using WeatherAppService.CommunicationSerializers;
using WeatherAppService.Managers;

namespace WeatherAppService.Controllers
{
    public class WeatherController : ApiController
    {
        private ILogger logger;
        [HttpPost]
        public async Task<HttpResponseMessage> RouteWeatherAsync()
        {
            logger = LogWrapper.GetLogger(GetType(), LogWrapper.GetNewguid());
            var requestStream = GetRequestStream().Result;
            var weatherFetcher = new WeatherFetcher(requestStream, this.logger.RequestGuid);

            var requestTotalTime = Stopwatch.StartNew();
            var weatherFetchTime = Stopwatch.StartNew();
            var weatherFetchedCities = await weatherFetcher.FetchWeatherForRoute();
            logger.Performance("Weather fetched for {0} cities in {1} ms for time starting at {2} utc", weatherFetchedCities.Count, weatherFetchTime.ElapsedMilliseconds, Utils.ConvertUnixTimeStampToUniversalDateTime(weatherFetchedCities.First().ApproximateArrivalTime));

            HttpResponseMessage result = new HttpResponseMessage();
            var locationInformationResponse = new LocationInformationResponse();

            locationInformationResponse.WeatherLocationInformation = weatherFetchedCities;

            var resultStream = new MemoryStream();
            CommunicationSerializer.Serialize(locationInformationResponse, resultStream);
            //reset the position
            resultStream.Position = 0;
            result.StatusCode = HttpStatusCode.OK;
            result.Content = new StreamContent(resultStream);

            logger.Performance("Time taken for request {0}", requestTotalTime.ElapsedMilliseconds);
            return result;
        }

        private async Task<Stream> GetRequestStream()
        {
            return await Request.Content.ReadAsStreamAsync();
        }
    }
}
