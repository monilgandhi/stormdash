﻿using System.Web.Http;
using CommonUtils;
using CommunicationSchemas.Schemas.Protobuf;
using WeatherAppService.CommunicationSerializers;

namespace WeatherAppService.Controllers
{
    public class LogController : ApiController
    {
        [System.Web.Mvc.HttpPost]
        public void LogIt()
        {
            var requestStream = Request.Content.ReadAsStreamAsync().Result;
            var clientLog = CommunicationSerializer.Deserialize<ClientLog>(requestStream);
            if (clientLog == null) return;

            ClientLogManager.Log(clientLog);
        }
    }
}