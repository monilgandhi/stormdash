﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteWeather.CommonLib
{
    public class IconManager
    {
        private string weatherIconBasePath;
        private string otherIconBasePath;

        public static string RouteIcon = "route";
        public static string SpeedIcon = "speed";
        public static string WeatherAlertIcon = "alert";
        public static string SourceIcon = "location";
        public static string DestinationIcon = "destination";
        public static string UncheckedGlyphIcon = "uncheck";
        public static string CheckedGlyphIcon = "check";
        private static string PNGExt = ".png";


        public static string GetWeatherConditionIconPath(string condition, ulong arrivalTime, Theme colorTheme)
        {
            var iconsFullPath = new StringBuilder(WeatherIconBasePath);
            iconsFullPath.Append(colorTheme.ToString());

            var localDateTime =
                Utils.ConvertUnixToDateTime(arrivalTime);

            if (Utils.DetermineIfDay(localDateTime))
            {
                iconsFullPath.Append(PathSeparator + "Day");
            }
            else
            {
                iconsFullPath.Append(PathSeparator + "Night");
            }

            //TODO p0 Temp
            iconsFullPath.Append(PathSeparator + "Cloudy" + PathSeparator + condition + ".png");
            return iconsFullPath.ToString();
        }

        public static string GetOtherIcon(string iconName, Theme colorTheme)
        {
            var iconsFullPath = new StringBuilder(OtherIconBasePath);
            iconsFullPath.Append(colorTheme.ToString());
            iconsFullPath.Append(PathSeparator + iconName + PNGExt);

            return iconsFullPath.ToString();
        }
    }
}
