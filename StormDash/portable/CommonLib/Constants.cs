﻿using System;

namespace RouteWeather.CommonLib
{
    public static class Constants
    {
        #region Cache Constants
        public const string SourceCacheParam = "sourceLocationObject";
        public const string DestinationCacheParam = "destinationLocationObject";
        public const string RouteCacheParam = "route";
        public const string SelectedDateCacheParam = "selectedDate";
        public const string SelectedTimeStampCacheParam = "selectedTimeStamp";
        public const string MapRestrictionCacheParam = "maprestriction";
        public const string IsDebugCacheParam = "isdebug";
        public const string AutocompleteSelectedItemCacheParam = "autocompleteSelectedItem";
        public const string ServiceRouteCacheParam = "serviceRoute";
        public const string SelectedRouteMapDisplayCacheParam = "selectedroutemapdisplay";
        #endregion

        public const string LocationPlaceHolder = "Enter city or zip";

        public const byte SourceInputActive = 0;
        public const byte DestinationInputActive = 1;

        public const char CityStateSeperator = ',';
        public const string StormdashServiceBaseUrl = "http://stormdash.cloudapp.net/";
        public const string StormDashServiceLocalBaseUrl = "http://192.168.1.7/stormbeat";
        public const string RouteWeatherUrl = "/api/weather/routeweatherasync";
        public const string FeedbackUrl = "/api/feedback/submit";
        public const string LogUrl = "/api/log/logit";

        public const string AndroidStoreUrl = "https://play.google.com/store/apps/details?id=com.stormdash";
        public const string AppStoreUrl = "";
        public const string WinStoreUrl = "";
    }
}
