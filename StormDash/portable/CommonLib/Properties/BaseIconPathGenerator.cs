﻿using System;

namespace RouteWeather.CommonLib
{
    public abstract class BaseIconPathGenerator
    {
        protected string otherIconPath;
        protected string commonIconPath;
        protected Theme currentTheme;
        public BaseIconPathGenerator (string otherIconPath, string commonIconPath, Theme currentTheme)
        {
            this.otherIconPath = otherIconPath;
            this.commonIconPath = commonIconPath;
            this.currentTheme = currentTheme;
        }
    }
}

