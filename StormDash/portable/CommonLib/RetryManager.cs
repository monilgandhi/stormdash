﻿namespace RouteWeather.CommonLib
{
    public class RetryManager
    {
        private int _maxRetryCount;
        private int _retryCount;

        public RetryManager(int maxRetryCount = 5)
        {
            _maxRetryCount = maxRetryCount;
            _retryCount = 0;
        }

        public bool CanRetry
        {
            get
            {
                return _retryCount < _maxRetryCount;
            }
        }

        public bool Retry()
        {
            if(CanRetry)
            {
                ++_retryCount;
                return true;
            }

            return false;
        }

    }
}
