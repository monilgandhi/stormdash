﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherApp.Communication;

namespace RouteWeather.CommonLib
{
    public static class Utils
    {
        private static int[] twelveHour = new[]
        {12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        public static char DetermineAndAppendPluralForm(int number)
        {
            return number > 0 ? 's' : '\0';
        }

        public static DateTime ConvertUnixToDateTime(ulong unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static uint ConvertDateTimetoUnixUTC(DateTime dateTime)
        {
            return (uint)dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        }

        public static string ConvertTimeTo12Hour(int value)
        {
            string suffix = ":00 pm";
            if (value < 12)
            {
                suffix = ":00 am";
            }

            return twelveHour[value] + suffix;
        }


        public static string ConvertTimeTo12Hour(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return null;
            }

            UInt16 intValue = 0;
            if (!UInt16.TryParse(value, out intValue))
            {
                return null;
            }

            if(intValue == 24)
            {
                //reset to 0
                intValue = 0;
            }
            return ConvertTimeTo12Hour(intValue);
        }

        public static int GetDateSliderMinimumValue(DateTime selectedDate)
        {
            if (DateTime.Now.Day == selectedDate.Day)
            {
                return DateTime.Now.Hour + 1;
            }

            return 0;
        }

        //TODO p1 make this more smarter
        public static bool DetermineIfDay(DateTime localTime)
        {
            if (localTime.TimeOfDay.Hours >= 19)
            {
                return false;
            }

            if (localTime.TimeOfDay.Hours >= 0 && localTime.TimeOfDay.Hours <= 5)
            {
                return false;
            }
            return true;
        }

        public static string TemperatureImperialUnit
        {
            get
            {
                return (char) 176 + "F";
            }
        }
        public static double GetDistanceInLocalCulture(double lengthInmeters)
        {
            //TODO for now just convert to miles
            return Math.Round(lengthInmeters * 0.000621371,1);
        }

        public static string GetDistanceUnits()
        {
            return "miles";
        }

        public static string FormateEstimatedTime(RouteInformation routeInformation)
        {
            var sb = new StringBuilder();
            if (routeInformation.EstimatedTime.NumberofDays > 0)
            {
                sb.Append(routeInformation.EstimatedTime.NumberofDays 
                    + " day" 
                    + Utils.DetermineAndAppendPluralForm((int)routeInformation.EstimatedTime.NumberofDays) + " ");
            }
            if (routeInformation.EstimatedTime.NumberofHours > 0)
            {
                sb.Append(routeInformation.EstimatedTime.NumberofHours 
                    + " hour" 
                    + Utils.DetermineAndAppendPluralForm((int)routeInformation.EstimatedTime.NumberofHours) + " ");
            }
            if (routeInformation.EstimatedTime.NumberofMinutes > 0)
            {
                sb.Append(routeInformation.EstimatedTime.NumberofMinutes 
                    + " min" 
                    + Utils.DetermineAndAppendPluralForm((int)routeInformation.EstimatedTime.NumberofMinutes) + " ");
            }
            return sb.ToString();
        }

        public static List<Geocoordinate> DecodeGooglePolyLines(string encodedPoints)
        {
            char[] polylineChars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;

            var result = new List<Geocoordinate>();
            while (index < polylineChars.Length)
            {
                int next5bits;
                int sum = 0;
                int shifter = 0;
                // calculate next latitude
                do
                {
                    next5bits = (int)polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                //calculate next longitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5bits = (int)polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length && next5bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                var coordinate = new Geocoordinate () 
                {
                    Latitude = (float)(Convert.ToDouble (currentLat) / 1E5),
                    Longitude = (float)(Convert.ToDouble (currentLng) / 1E5)
                };

                result.Add(coordinate);
            }

            return result;
        }

        public static UInt16? GetAvgCloudCoverPercent(List<LocationInformation> locationInfo)
        {
            if (locationInfo == null || locationInfo.Count == 0) 
            {
                throw new ArgumentNullException ("locationInfo");
            }

            uint? cloudOvercastSum = null;
            int validCityCount = 0;
            foreach (var locationInfoObj in locationInfo)
            {
                if (locationInfoObj == null || locationInfoObj.Weather == null)
                {
                    //TODO log
                    continue;
                }

                if (locationInfoObj.Weather.CloudCoverPercentage == null)
                {
                    //TODO log
                    continue;
                }

                if (cloudOvercastSum == null)
                {
                    cloudOvercastSum = 0;
                }
                ++validCityCount;
                cloudOvercastSum += (uint)locationInfoObj.Weather.CloudCoverPercentage;
            }

            if (validCityCount == 0)
            {
                return null;
            }

            if (cloudOvercastSum != null)
            {
                return (ushort)(cloudOvercastSum / validCityCount);
            }

            return null;
        }

        public static byte [] GetBytes (string str)
        {
            return Encoding.UTF8.GetBytes (str.ToCharArray ());
        }

        public static string GetString (byte [] bytes)
        {
            return Encoding.UTF8.GetString (bytes, 0, bytes.Length);
        }
    }
}
