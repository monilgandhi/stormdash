﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteWeather.Exceptions
{
    public class EmptyHttpResponseException : Exception
    {
        public EmptyHttpResponseException()
        {

        }

        public EmptyHttpResponseException(string message) : 
            base(message)
        {

        }

        public EmptyHttpResponseException(string message, Exception inner)
            :base(message, inner)
        {

        }

    }
}
