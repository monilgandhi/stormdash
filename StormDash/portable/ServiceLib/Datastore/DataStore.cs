﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RouteWeather.Exceptions;
using SQLite;

namespace RouteWeather.DataStore
{
    public class DataStore :IDataStoreService
    {
        private string _database;
        private SQLiteAsyncConnection _connection;
        protected string _tableName;
        private SQLiteAsyncConnection Connection
        {
            get
            {
                if(_connection == null)
                {
                    _connection = new SQLiteAsyncConnection(_database,true);
                }
                return _connection;
            }
        }
        
        public DataStore(string database = "weatherroute.db")
        {
            if (String.IsNullOrWhiteSpace(database))
            {
                throw new NullReferenceException("Database name cannot be null or empty");
            }

            _database = database;
        }

        public async Task<List<T>> GetAsync<T>(string query, params object[] args) where T : new()
        {
            if(String.IsNullOrWhiteSpace(query))
            {
                return null;
            }

            var result = default(List<T>);

            try
            {
                result = await Connection.QueryAsync<T>(query,args);
            }
            catch
            {
                //TODO log this
                throw;
            }

            return result;
        }

        public async Task<int> InsertAsync<T>(T obj) where T : new()
        {
            if(obj == null)
            {
                throw new ArgumentNullException("Value of obj to be inserted is null");
            }
            int result = -1;
            try
            {
                result = await Connection.InsertAsync(obj);
            }
            catch
            {
                //TODO add another exception here
                throw;
            }
            return result;

        }

        public async Task<int> UpdateAsync<T>(T obj) where T : new()
        {
            if (obj == null)
            {
                throw new ArgumentNullException("Value of obj to be inserted is null");
            }
            int result = -1;
            try
            {
                result = await Connection.UpdateAsync(obj);
            }
            catch
            {
                //TODO add another exception here
                throw;
            }
            return result;
        }

        public async Task<int> InsertAllAsync<T>(IEnumerable<T> objList) where T : new()
        {
            if (objList == null || !objList.Any())
            {
                throw new ArgumentNullException("Value of obj to be inserted is null");
            }

            int result = -1;
            try
            {
                result = await Connection.InsertAllAsync(objList);
            }
            catch
            {
                //TODO add another exception here
                throw;
            }
            return result;

        }
        
        public AsyncTableQuery<T> Table<T>() where T: new()
        {
            return Connection.Table<T>(); 
        }

        public async Task<bool> CreateTableAsync<T>() where T: new()
        {
            var result = default(CreateTablesResult);

            try
            {
                result = await Connection.CreateTableAsync<T>();
                var type = typeof(T);
                int success;
                if(result.Results != null && result.Results.TryGetValue(type, out success) && success > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                //TODO log this
                throw;
            }

        }

        public async Task<bool> CheckTableExists()
        {
            try
            {
                var query = @"SELECT name FROM sqlite_master WHERE type='?' ORDER BY name";
                var result = await Connection.ExecuteScalarAsync<object>(query, _tableName);
                if(result == null)
                {
                    return false;
                }

                return true;
            }
            catch (SQLiteException ex)
            {
                throw new DataStoreException(String.Format("Exception occurred while checking if table {0} exists", _tableName));
            }

        }

        public async Task<int> DropTableAsync<T>() where T : new()
        {
            int result = -1;

            try
            {
                result = await Connection.DropTableAsync<T>();
                var type = typeof(T);
                return result;
            }
            catch
            {
                //TODO log this
                throw;
            }
        }
    }
}
