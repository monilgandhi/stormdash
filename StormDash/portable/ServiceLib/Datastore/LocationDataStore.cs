﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RouteWeather.Model;
using RouteWeather.Services;
using SQLite;

namespace RouteWeather.DataStore
{
    public class LocationDataStore : DataStore,ILocationDataStoreService
    {
        const String _CityColumnName = "city";
        const String _ZipColumnName = "zip";
        public LocationDataStore() : base() {}

        /// <summary>
        /// Get all matching cities names.
        /// </summary>
        /// <param name="cityName"></param>
        /// <param name="maxResults">Maximum number of results desired. Here -1 means all</param>
        /// <returns></returns>
        private async Task<List<Location>> GetCities(string matchingPattern, 
                                                     int maxResults = -1,
                                                     string columnName = _CityColumnName)
        {
            if (String.IsNullOrWhiteSpace(matchingPattern))
            {
                throw new ArgumentNullException("matching pattern cannot be null");
            }

            if (String.IsNullOrWhiteSpace(matchingPattern))
            {
                throw new ArgumentNullException("columnName cannot be null");
            }

            if(columnName != _CityColumnName && columnName != _ZipColumnName)
            {
                throw new ArgumentException("columname should be either zip or city");
            }

            if(maxResults == 0)
            {
                return new List<Location>();
            }
            
            
            var sb = new StringBuilder("Select * from location where ");
            sb.Append(columnName);
            sb.Append(" LIKE ? GROUP BY ");
            sb.Append(columnName);

            if(maxResults > 0)
            {
                sb.Append(" LIMIT ? ");
            }

            return await GetAsync<Location>(sb.ToString(), matchingPattern, maxResults);
        }

        public async Task<List<Location>> GetZipCodeAutoComplete(string zipSearchTerm, int maxResults)
        {
            if (String.IsNullOrWhiteSpace(zipSearchTerm))
            {
                throw new ArgumentNullException("Search term is null");
            }

            int zipSearchInt;
            if(!int.TryParse(zipSearchTerm, out zipSearchInt))
            {
                throw new ArgumentException("Search term is not a valid integer");
            }

            //first find all the cities starting with the letters
            StringBuilder searchTerm = new StringBuilder(zipSearchTerm + "%");
            var result = await GetCities(searchTerm.ToString(), maxResults,_ZipColumnName);

            var uniqueCityNames = new HashSet<UInt32>();

            foreach (var city in result)
            {
                uniqueCityNames.Add(city.Zipcode);
            }
            //since we got less than expected, lets search for anything that matches with these terms
            if (result.Count < maxResults)
            {
                searchTerm.Clear();
                searchTerm.Append("%" + zipSearchTerm + "%");

                //get the new result
                var newResult = await GetCities(searchTerm.ToString(), maxResults - result.Count);

                //it is okay to do this here since we do not expect maxresults to be > than 10
                //we do this so that we do not have multiple entries of same city
                foreach (var city in newResult)
                {
                    if (!uniqueCityNames.Contains(city.Zipcode))
                    {
                        result.Add(city);
                    }
                }
            }

            return result;
        }


        public async Task<List<Location>> GetCitiesAutoComplete(string citySearchPattern, int maxResults)
        {
            if (String.IsNullOrWhiteSpace(citySearchPattern))
            {
                throw new ArgumentNullException("Search pattern is null");
            }
            
            //first find all the cities starting with the letters
            StringBuilder searchTerm = new StringBuilder(citySearchPattern + "%");
            var result = await GetCities(searchTerm.ToString(), maxResults);

            var uniqueCityNames = new HashSet<String>();

            foreach(var city in result)
            {
                uniqueCityNames.Add(city.City);
            }
            //since we got less than expected, lets search for anything that matches with these terms
            if(result.Count < maxResults)
            {
                searchTerm.Clear();
                searchTerm.Append("%" + citySearchPattern + "%");

                //get the new result
                var newResult = await GetCities(searchTerm.ToString(), maxResults - result.Count);

                //it is okay to do this here since we do not expect maxresults to be > than 10
                //we do this so that we do not have multiple entries of same city
                foreach(var city in newResult)
                {
                    if (!uniqueCityNames.Contains(city.City))
                    {
                        result.Add(city);
                    }
                }
            }

            return result;
        }

        public async Task<int> Populate(Locations allLocations)
        {
            if (allLocations == null || allLocations.LocationList == null || allLocations.LocationList.Count == 0)
            {
                throw new ArgumentNullException("allLocations");
            }
            //TODO LAME!!
            //we should modify the query of sql instead of doing something like this
            //we get this because of different latitude and longitude for same city and same zipcode
            var uniqueCity = new HashSet<string>();
            var uniqueLocations = new List<Location>();
            foreach (var location in allLocations.LocationList)
            {
                var key = location.City + "_" + location.State + "_" + location.Zipcode;
                if (!uniqueCity.Contains(key))
                {
                    uniqueCity.Add(key);
                    uniqueLocations.Add(location);
                }
            }
            //after this we continue to insert everything inside the database
            await DropTableAsync<Location>();
            await CreateTableAsync<Location>();
            return await InsertAllAsync<Location>(uniqueLocations);
        }

        public async Task<int> Populate(string jsonString)
        {
            if (String.IsNullOrWhiteSpace(jsonString))
            {
                throw new NullReferenceException("Could not get contents from the file");
            }

            var allLocations = JsonConvert.DeserializeObject<Locations>(jsonString);

            return await Populate(allLocations);
        }
    }
}