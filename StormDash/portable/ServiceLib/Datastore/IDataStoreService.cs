﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace RouteWeather.DataStore
{
    public interface IDataStoreService
    {
        Task<List<T>> GetAsync<T>(string query, params object[] args) where T : new();
        Task<int> InsertAsync<T>(T obj) where T : new();
        Task<int> InsertAllAsync<T>(IEnumerable<T> objList) where T : new();
        AsyncTableQuery<T> Table<T>() where T : new();
        Task<bool> CreateTableAsync<T>() where T : new();
        Task<int> DropTableAsync<T>() where T : new();
    }
}
