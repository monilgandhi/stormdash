﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteWeather.Exceptions
{
    public class DataStoreException : Exception
    {
        public DataStoreException():base()
        { }

        public DataStoreException(string message) : base(message)
        {}

    }
}
