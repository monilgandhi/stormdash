﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RouteWeather.Model;

namespace RouteWeather.Services
{
    public interface ILocationDataStoreService
    {
        Task<List<Location>> GetCitiesAutoComplete(string citySearchPattern, int maxResults = -1);
        Task<List<Location>> GetZipCodeAutoComplete(string zipSearchTerm, int maxResults = -1);
        Task<int> Populate(Locations allLocations);
        Task<int> Populate(string contentjsonString);

        Task<bool> CheckTableExists();
    }
}
