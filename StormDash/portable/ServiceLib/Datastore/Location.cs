﻿using System;
using System.Collections.Generic;
using SQLite;

namespace RouteWeather.Model
{
    [Table("location")]
    public class Location
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        [Indexed,NotNull]
        [Column("zip")]
        public UInt32 Zipcode {get;set;}

        [Indexed,NotNull]
        [Column("city")]
        public String City { get; set; }

        [NotNull]
        [Column("state")]
        public String State { get; set; }

        [NotNull]
        [Column("latitude")]
        public float Latitude { get; set; }

        [NotNull]
        [Column("longitude")]
        public float Longitude { get; set; }
    }

    /// <summary>
    /// This is a dummy class for json deserialization
    /// </summary>
    public class Locations
    {
        public List<Location> LocationList { get; set; }
    }
}
