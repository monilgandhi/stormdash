﻿using System;
using System.IO;

namespace RouteWeather.Services
{
    public class ServiceUtils
    {
        public static void SerializeWithProtobuf<T> (T obj, Stream stream) where T : class
        {
            if (obj == null) {
                return;
            }

            ProtoBuf.Serializer.SerializeWithLengthPrefix (stream, obj, ProtoBuf.PrefixStyle.Base128, 0);
        }

        public static T DeserializeWithProtobuf<T> (Stream requestedStream) where T : class
        {
            T deserializedObject = default (T);
            if (requestedStream.Length == 0) {
                //TODO something wrong. log and return
                return deserializedObject;
            }

            deserializedObject = ProtoBuf.Serializer.DeserializeWithLengthPrefix<T> (requestedStream, ProtoBuf.PrefixStyle.Base128);
            return deserializedObject;
        }
    }
}

