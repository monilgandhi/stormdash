﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RouteWeather.Model;

namespace RouteWeather.Services.DataStore
{
    public interface ILocationDataStoreService
    {
        Task<List<LocationDO>> GetCitiesAutoComplete(string citySearchPattern, int maxResults = -1);
        Task<LocationDO> GetExactCities(string cityName, string stateName);
        Task<List<LocationDO>> GetZipCodeAutoComplete(string zipSearchTerm, int maxResults = -1);
        Task<int> Populate(Locations allLocations);
        Task<int> Populate(string contentjsonString);
        Task<int> GetLocationCount();
        Task<bool> CheckTableExists();
        Task<LocationDO> SaveAndGetSelectedLocation (LocationDO selectedLocation);
    }
}
