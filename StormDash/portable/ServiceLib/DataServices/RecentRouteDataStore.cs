﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using RouteWeather.Model;

namespace RouteWeather.Services.DataStore
{
    public class RecentRouteDataStore : DataStore, IRecentLocationDataStoreService
    {
        internal const string RecentRouteTableName = "recentroutes";
        public RecentRouteDataStore(ISqliteConnection sqliteConnectionFactory) : base(sqliteConnectionFactory)
        {
            this.tableName = RecentRouteTableName;
        }

        public async Task AddAsync(LocationDO source, LocationDO destination)
        {
            if(source == null)
            {
                throw new ArgumentNullException("source");
            }

            if(destination == null)
            {
                throw new ArgumentNullException("destination");
            }

            if (!await this.CheckTableExists ()) 
            {
                //Create the table by default. SQLLite will create table if and only if it does not exist.
                await CreateTableAsync<RecentRoutes> ();
            }

            string primaryKey = string.Format ("{0}_{1}", source.Id, destination.Id);
            string queryToCheckIfExisting = String.Format("select * from {0} where primarykey = ?", this.tableName);

            var result = await GetAsync<RecentRoutes>(queryToCheckIfExisting, primaryKey);
            RecentRoutes recentLocation = null;

            bool isUpdate = false;
            if(result == null || result.Count == 0)
            {
                //this key is not present. Create one
                recentLocation = new RecentRoutes()
                {
                    PrimaryKey = primaryKey,
                    SourceId = source.Id,
                    DestinationId = destination.Id,
                    Count = 0,
                    Favourite = false
                };
            }

            if(result != null && result.Count > 0)
            {
                //take the first one since we should have only one
                recentLocation = result[0];
                isUpdate = true;
            }

            ++recentLocation.Count;
            recentLocation.LastSelected = DateTime.UtcNow.AddDays(-1);

            try
            {
                if(!isUpdate)
                {
                    await InsertAsync(recentLocation);
                }
                else
                {
                    await UpdateAsync(recentLocation);
                }
            }
            catch(Exception ex)
            {
                Debugger.Break();
            }
            
        }

        public async Task<List<RecentRoutes>> GetRecentlyUsedLocationsAsync()
        {
            if (!await this.CheckTableExists ()) 
            {
                return null;
            }
            string query = string.Format("SELECT * FROM {0} ORDER BY lastSelected DESC LIMIT 5 ", this.tableName);

            List<RecentRoutes> result = null;
            try
            {
                result = await GetAsync<RecentRoutes>(query, this.tableName);
                if(result != null && result.Count > 0)
                {
                    string queryToFetchLocation = string.Format("SELECT * FROM {0} where id = ? OR id = ?", LocationDataStore.LocationTableName);
                    foreach(var item in result)
                    {
                        var locations = await GetAsync<LocationDO>(queryToFetchLocation, item.SourceId, item.DestinationId);
                        if(locations == null || locations.Count < 2 || locations[0] == null || locations[1] == null)
                        {
                            continue;
                        }

                        if(locations[0].Id == item.SourceId)
                        {
                            item.Source = locations[0];
                            item.Destination = locations[1];
                        }
                        else
                        {
                            item.Destination = locations[0];
                            item.Source = locations[1];
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                Debugger.Break();
            }

            return result;

        }

        private string GetPrimaryKey(LocationDO source, LocationDO destination)
        {
            if(String.IsNullOrEmpty(source.City) || String.IsNullOrEmpty(source.State) ||
               String.IsNullOrEmpty(destination.City) || String.IsNullOrEmpty(destination.State))
            {
                throw new ArgumentException("Source/Destination data incomplete. Cannot form the key");
            }

            return source.City + "," + source.State + "this." + destination.City + "," + destination.State;
        }
    }
}
