﻿using System;
using SQLite;

namespace RouteWeather.Services.DataStore
{
    public interface ISqliteConnection
    {
        SQLiteAsyncConnection GetConnection(string databaseName);
    }
}

