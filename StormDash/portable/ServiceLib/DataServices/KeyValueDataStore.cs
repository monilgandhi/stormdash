﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RouteWeather.CommonLib;
using RouteWeather.Model;
using SQLite;

namespace RouteWeather.Services.DataStore
{
    public class KeyValueDataStore:DataStore, IKeyValueDataStore
    {
        public KeyValueDataStore(ISqliteConnection sqliteConnectionFactory) : base(sqliteConnectionFactory)
        {
            this.tableName = "keyvalue";
        }

        public async Task MarkAppRated ()
        {
            AppUsage appUsage = await GetAppUsageData ();
            appUsage.IsAppRated = true;
            await SetAppUsageData (appUsage);
        }

        public async Task<AppUsage> GetAppUsageData ()
        {
            AppUsage appUsage = await this.Get<AppUsage> ("AppUsageData");
            return appUsage == null ? new AppUsage () : appUsage;
        }

        public async Task<bool> SetAppUsageData (AppUsage appUsageData)
        {
            return await this.Set<AppUsage>("AppUsageData", appUsageData);
        }

        public async Task<bool> Set<T>(string key, T value) where T:class
        {
            if (string.IsNullOrWhiteSpace (key)) 
            {
                throw new ArgumentNullException ("key");
            }

            if (value == null) 
            {
                throw new ArgumentNullException ("value");
            }

            try
            {
                if (!await CheckTableExists())
                {
                    throw new OperationCanceledException("Unable to create a table");
                }

                string serializedObject;
                if(value is String)
                {
                    serializedObject = value as String;
                }
                else
                {
                    serializedObject = JsonConvert.SerializeObject (value);
                }

                var keyValueObj = new KeyValue () 
                {
                    Key = key,
                    Value = serializedObject
                };

                // check if the value is already present or not
                var query = "select * from keyvalue where key=?";
                var existingResult = await this.GetAsync<KeyValue>(query, args:key);
                if (existingResult == null || existingResult.Count == 0) 
                {
                    // use insert
                    await this.InsertAsync (keyValueObj);
                } 
                else 
                {
                    await this.UpdateAsync (keyValueObj);
                }
            }
            catch(JsonSerializationException jex)
            {
                return false;
            }
            catch(SQLiteException sqex)
            {
                return false;
            }

            return true;
        }

        public async Task<T> Get<T>(string key) where T : class
        {
            if (string.IsNullOrWhiteSpace (key)) 
            {
                throw new ArgumentNullException ("key");
            }

            var query = "select value from keyvalue where key=?";

            try
            {
                await CreateTableAsync<KeyValue>();

                var result = await this.GetAsync<KeyValue>(query, key);

                if(result == null || result.Count == 0 || result[0] == null)
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<T>(result[0].Value);
            }
            catch(SQLiteException sqex) 
            {
                // TODO log
                throw;
            }
            catch(JsonSerializationException jex) 
            {
                // TODO log
                throw;
            }
        }
    }
}

