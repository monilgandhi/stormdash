﻿using System;

namespace RouteWeather.Services.Exceptions
{
    public class DataStoreException : Exception
    {
        public DataStoreException():base()
        { }

        public DataStoreException(string message) : base(message)
        {}

    }
}
