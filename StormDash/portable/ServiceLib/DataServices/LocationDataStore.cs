﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RouteWeather.Model;
using System.IO;

namespace RouteWeather.Services.DataStore
{
    public class LocationDataStore : DataStore, ILocationDataStoreService
    {
        const String CityColumnName = "city";
        const String ZipColumnName = "zip";
        const String StateColumnName = "state";
        const String LatitudeColumnName = "latitude";
        const String LongitudeColumnName = "longitude";
        internal const string LocationTableName = "location";

        public LocationDataStore(ISqliteConnection sqliteConnectionFactory) : base(sqliteConnectionFactory)
        {
            this.tableName = LocationDataStore.LocationTableName;
        }

        public async Task<LocationDO> SaveAndGetSelectedLocation (LocationDO selectedLocation)
        {
            if (selectedLocation == null || string.IsNullOrWhiteSpace (selectedLocation.City) ||
               string.IsNullOrWhiteSpace (selectedLocation.State)) 
            {
                return null;
            }

            //Create table
            if (!await this.CheckTableExists ()) 
            {
                await CreateTableAsync<LocationDO> ();
            }

            //check if this location is already present
            LocationDO savedLocation = await GetExactCities (selectedLocation.City, selectedLocation.State);

            if (savedLocation == null) 
            {
                await this.InsertAsync<LocationDO> (selectedLocation);
                // we get the location again to get the primary key associated with it
                savedLocation = await GetExactCities (selectedLocation.City, selectedLocation.State);
            }

            return savedLocation;
        }

        public async Task<LocationDO> GetExactCities(string cityName, string state)
        {
            if (string.IsNullOrWhiteSpace (cityName)) 
            {
                throw new ArgumentNullException ("cityname");
            }

            if (string.IsNullOrWhiteSpace (state)) 
            {
                throw new ArgumentNullException ("state");
            }

            var sb = new StringBuilder(string.Format("Select * from {0} where ", this.tableName));
            sb.Append(CityColumnName);
            sb.Append(" LIKE ? AND ");
            sb.Append(StateColumnName);
            sb.Append(" LIKE ? LIMIT 1");

            var locationList =  await GetAsync<RouteWeather.Model.LocationDO>(sb.ToString(), cityName, state);
            return locationList.FirstOrDefault ();
        }

        /// <summary>
        /// Get all matching cities names.
        /// </summary>
        /// <param name="cityName"></param>
        /// <param name="maxResults">Maximum number of results desired. Here -1 means all</param>
        /// <returns></returns>
        private async Task<List<RouteWeather.Model.LocationDO>> GetCities(string matchingPattern, 
                                                     int maxResults = -1,
                                                     string columnName = CityColumnName)
        {
            if (String.IsNullOrWhiteSpace(matchingPattern))
            {
                throw new ArgumentNullException("matchingPattern");
            }

            if(columnName != CityColumnName && columnName != ZipColumnName)
            {
                throw new ArgumentException("columname should be either zip or city");
            }

            if(maxResults == 0)
            {
                return new List<RouteWeather.Model.LocationDO>();
            }
            
            
            var sb = new StringBuilder("Select * from location where ");
            sb.Append(columnName);
            sb.Append(" LIKE ? GROUP BY ");
            sb.Append(columnName);

            /*if(maxResults > 0)
            {
                sb.Append(" LIMIT ? ");
            }*/

            return await GetAsync<RouteWeather.Model.LocationDO>(sb.ToString(), matchingPattern, maxResults);
        }

        public async Task<List<RouteWeather.Model.LocationDO>> GetZipCodeAutoComplete(string zipSearchTerm, int maxResults)
        {
            if (String.IsNullOrWhiteSpace(zipSearchTerm))
            {
                throw new ArgumentNullException("Search term is null");
            }

            int zipSearchInt;
            if(!int.TryParse(zipSearchTerm, out zipSearchInt))
            {
                throw new ArgumentException("Search term is not a valid integer");
            }

            //first find all the cities starting with the letters
            StringBuilder searchTerm = new StringBuilder(zipSearchTerm + "%");
            var result = await GetCities(searchTerm.ToString(), maxResults,ZipColumnName);

            var uniqueCityNames = new HashSet<UInt32>();

            foreach (var city in result)
            {
                uniqueCityNames.Add(city.Zipcode);
            }
            //since we got less than expected, lets search for anything that matches with these terms
            if (result.Count < maxResults)
            {
                searchTerm.Clear();
                searchTerm.Append("%" + zipSearchTerm + "%");

                //get the new result
                var newResult = await GetCities(searchTerm.ToString(), maxResults - result.Count);

                //it is okay to do this here since we do not expect maxresults to be > than 10
                //we do this so that we do not have multiple entries of same city
                foreach (var city in newResult)
                {
                    if (!uniqueCityNames.Contains(city.Zipcode))
                    {
                        result.Add(city);
                    }
                }
            }

            return result;
        }


        public async Task<List<RouteWeather.Model.LocationDO>> GetCitiesAutoComplete(string citySearchPattern, int maxResults)
        {
            if (String.IsNullOrWhiteSpace(citySearchPattern))
            {
                throw new ArgumentNullException("Search pattern is null");
            }
            
            //first find all the cities starting with the letters
            StringBuilder searchTerm = new StringBuilder(citySearchPattern + "%");
            var result = await GetCities(searchTerm.ToString(), maxResults);
            var uniqueCityNames = new HashSet<String>();

            foreach(var city in result)
            {
                uniqueCityNames.Add(city.City);
            }
            //since we got less than expected, lets search for anything that matches with these terms
            if(result.Count < maxResults)
            {
                searchTerm.Clear();
                searchTerm.Append("%" + citySearchPattern + "%");

                //get the new result
                var newResult = await GetCities(searchTerm.ToString(), maxResults - result.Count);

                //it is okay to do this here since we do not expect maxresults to be > than 10
                //we do this so that we do not have multiple entries of same city
                foreach(var city in newResult)
                {
                    if (!uniqueCityNames.Contains(city.City))
                    {
                        result.Add(city);
                    }
                }
            }

            return result;
        }

        public async Task<int> GetLocationCount()
        {
            var query = "select count(*) from " + this.tableName;
            var result = await this.GetCount (query);
            if (result <= 0) 
            {
                return 0;
            }
            return result;
        }

        public async Task<int> Populate(RouteWeather.Model.Locations allLocations)
        {
            if (allLocations == null || allLocations.LocationList == null || allLocations.LocationList.Count == 0)
            {
                throw new ArgumentNullException("allLocations");
            }
            //TODO LAME!!
            //we should modify the query of sql instead of doing something like this
            //we get this because of different latitude and longitude for same city and same zipcode
            var uniqueCity = new HashSet<string>();
            var uniqueLocations = new List<RouteWeather.Model.LocationDO>();
            foreach (var location in allLocations.LocationList)
            {
                var key = location.City + "this." + location.State + "this." + location.Zipcode;
                if (!uniqueCity.Contains(key))
                {
                    uniqueCity.Add(key);
                    uniqueLocations.Add(location);
                }
            }
            //after this we continue to insert everything inside the database
            //await DropTableAsync<RouteWeather.Model.Location>();
            await CreateTableAsync<RouteWeather.Model.LocationDO>();
            return await InsertAllAsync<RouteWeather.Model.LocationDO>(uniqueLocations);
        }

        public async Task<int> Populate(string jsonString)
        {
            if (String.IsNullOrWhiteSpace(jsonString))
            {
                throw new NullReferenceException("Could not get contents from the file");
            }

            // convert string to stream
            byte[] byteArray = Convert.FromBase64String(jsonString);
            MemoryStream stream = new MemoryStream(byteArray);
            stream.Position = 0;
            var allLocations = ProtoBuf.Serializer.DeserializeWithLengthPrefix<Locations>(stream, ProtoBuf.PrefixStyle.Base128);
            return await Populate(allLocations);
        }
    }
}
