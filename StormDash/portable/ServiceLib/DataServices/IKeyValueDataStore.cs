﻿using System;
using System.Threading.Tasks;

namespace RouteWeather.Services.DataStore
{
    public interface IKeyValueDataStore
    {
        Task<bool> Set<T>(string key, T value) where T:class;
        Task<T> Get<T>(string key) where T:class;
        Task<AppUsage> GetAppUsageData ();
        Task<bool> SetAppUsageData (AppUsage appUsageData);
        Task MarkAppRated ();
    }
}

