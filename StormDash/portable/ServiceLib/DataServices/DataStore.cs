﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RouteWeather.Model;
using RouteWeather.Services.Exceptions;
using SQLite;

namespace RouteWeather.Services.DataStore
{
    public abstract class DataStore :IDataStoreService
    {
        private string database;
        private SQLiteAsyncConnection connection;
        protected string tableName;
        protected ISqliteConnection sqlConnectionFactory;
        private SQLiteAsyncConnection Connection
        {
            get
            {
                if(this.connection == null)
                {
                    this.connection = sqlConnectionFactory.GetConnection (database);
                }
                return this.connection;
            }
        }
        
        public DataStore(ISqliteConnection sqlConnectionFactory, string database = "stormdash")
        {
            this.sqlConnectionFactory = sqlConnectionFactory;
            if (String.IsNullOrWhiteSpace(database))
            {
                throw new NullReferenceException("Database name cannot be null or empty");
            }

            this.database = database;
        }

        public async Task DropAllTables ()
        {
            await this.DropTableAsync<LocationDO> ();
            await this.DropTableAsync<RecentRoutes> ();

        }
        public async Task<List<T>> GetAsync<T>(string query, params object[] args) where T : new()
        {
            if(String.IsNullOrWhiteSpace(query))
            {
                 throw new ArgumentNullException (query);
            }

            List<T> result;

            try
            {
                result = await Connection.QueryAsync<T>(query,args);
            }
            catch
            {
                //TODO log this
                throw;
            }

            return result;
        }
            
        public async Task<int> InsertAsync<T>(T obj) where T : new()
        {
            if(obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            int result = -1;
            try
            {
                result = await Connection.InsertAsync(obj);
            }
            catch
            {
                //TODO add another exception here
                throw;
            }
            return result;

        }

        public async Task<int> Delete<T> (T obj)
        {
            int result = -1;

            try 
            {
                result = await Connection.DeleteAsync (obj);
            }
            catch 
            {
                //TODO log this
                throw;
            }

            return result;
        }

        public async Task<int> UpdateAsync<T>(T obj) where T : new()
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            int result = -1;
            try
            {
                result = await Connection.UpdateAsync(obj);
            }
            catch
            {
                //TODO add another exception here
                throw;
            }
            return result;
        }

        public async Task<int> InsertAllAsync<T>(IEnumerable<T> objList) where T : new()
        {
            if (objList == null || !objList.Any())
            {
                throw new ArgumentNullException("objList");
            }

            int result = -1;
            try
            {
                result = await Connection.InsertAllAsync(objList);
            }
            catch
            {
                //TODO add another exception here
                throw;
            }
            return result;

        }
        
        public AsyncTableQuery<T> Table<T>() where T: new()
        {
            return Connection.Table<T>(); 
        }

        public async Task<bool> CreateTableAsync<T>() where T: new()
        {
            var result = default(CreateTablesResult);

            try
            {
                result = await Connection.CreateTableAsync<T>();
                var type = typeof(T);
                int success;

                result.Results.TryGetValue(type, out success);
                if(result.Results != null && result.Results.TryGetValue(type, out success) && success > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                System.Diagnostics.Debugger.Break ();
                //TODO log this
                throw;
            }

        }
            
        public async Task<bool> CheckTableExists()
        {
            try
            {
                var query = @"SELECT tbl_name FROM sqlite_master WHERE tbl_name=?";
                var result = await Connection.ExecuteScalarAsync<string>(query, this.tableName);
                if(result == null)
                {
                    return false;
                }

                return true;
            }
            catch (SQLiteException ex)
            {
                throw new DataStoreException(String.Format("Exception occurred while checking if table {0} exists", this.tableName));
            }

        }

        public async Task<int> DropTableAsync<T>() where T : new()
        {
            try
            {
                return await Connection.DropTableAsync<T>();
            }
            catch
            {
                //TODO log this
                throw;
            }
        }

        protected async Task<int> GetCount(string query, params object[] args)
        {
            if(String.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentNullException (query);
            }

            int result;

            try
            {
                result = await Connection.ExecuteScalarAsync<int>(query,args);
            }
            catch
            {
                //TODO log this
                throw;
            }

            return result;
        }

    }
}
