﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RouteWeather.Model;

namespace RouteWeather.Services.DataStore
{
    public interface IRecentLocationDataStoreService
    {
        Task<List<RecentRoutes>> GetRecentlyUsedLocationsAsync();
        Task AddAsync(LocationDO source, LocationDO destination);
    }
}
