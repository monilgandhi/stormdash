﻿using System;
using System.Collections.Generic;

namespace RouteWeather.Services
{
    public class GooglePlaceApiResponse
    {
        public List<GooglePlaceApiPrediction> predictions;
    }

    public class GooglePlaceApiPrediction
    {
        public string description;
        public List<GooglePlaceApiTerms> terms;
    }

    public class GooglePlaceApiTerms
    {
        public string value;
    }
}

