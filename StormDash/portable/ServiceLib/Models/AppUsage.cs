﻿using System;
using RouteWeather.CommonLib;

namespace RouteWeather.Services
{
    public class AppUsage
    {
        public static uint InitialiNumberOfUseToTriggerFeedback = 5;

        public ulong LastUseDateTimeStampUtc { get; set;}

        public uint UseSinceLastTimeFeedbackPopup { get; set;}

        public bool IsAppRated { get; set;}

        public AppUsage ()
        {
            this.LastUseDateTimeStampUtc = Utils.ConvertDateTimetoUnixUTC(DateTime.Now);
        }
    }
}