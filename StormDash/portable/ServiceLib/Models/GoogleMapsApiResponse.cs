﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RouteWeather.Services.Models
{
    internal class GoogleMapsApiResponse
    {
        public List<GoogleMapRoute> Routes = null;
    }

    internal class GoogleMapRoute
    {
        public string Summary = null;
        public List<GoogleMapLegs> Legs = null;
        public GoogleMapPolyLine Overview_Polyline = null;
    }

    internal class GoogleMapLegs
    {
        public List<GoogleMapLegSteps> Steps = null;
        public GoogleMapLocation Start_Location = null;
        public GoogleMapLocation End_Location = null;
        public GoogleMapDuration Duration = null;
        public GoogleMapDistance Distance = null;
    }

    internal class GoogleMapLegSteps
    {
        public GoogleMapPolyLine PolyLine = null;
    }

    internal class GoogleMapPolyLine
    {
        public string Points = null;
    }

    internal class GoogleMapDuration
    {
        public string Text = null;
    }

    internal class GoogleMapDistance
    {
        public string Text = null;
    }

    internal class GoogleMapLocation
    {
        public float lat = 0f;
        public float lng = 0f;
    }
}
