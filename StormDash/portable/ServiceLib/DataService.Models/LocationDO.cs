﻿using System;
using System.Collections.Generic;
using SQLite;
using ProtoBuf;
using RouteWeather.Services.DataStore;

namespace RouteWeather.Model
{
    [Table(LocationDataStore.LocationTableName)]
    public class LocationDO
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        [Indexed,NotNull]
        [Column("zip")]
        public UInt32 Zipcode {get;set;}

        [Indexed,NotNull]
        [Column("city")]
        public String City { get; set; }

        [NotNull]
        [Column("state")]
        public String State { get; set; }

        [NotNull]
        [Column("latitude")]
        public float Latitude { get; set; }

        [NotNull]
        [Column("longitude")]
        public float Longitude { get; set; }

        public override bool Equals (object obj)
        {
            var otherlocationObject = obj as LocationDO;
            if (otherlocationObject == null) 
            {
                return false;
            }
                
            bool success = otherlocationObject.City == this.City && otherlocationObject.State == this.State;

            if (otherlocationObject.Zipcode > 0 && this.Zipcode > 0)
            {
                success &= this.Zipcode == otherlocationObject.Zipcode;
            }

            return success;
        }

        public override string ToString()
        {
            return this.City + "," + this.State;
        }
    }

    /// <summary>
    /// This is a dummy class for json deserialization
    /// </summary>
    [ProtoContract]
    public class Locations
    {
        [ProtoMember(1)]
        public List<LocationDO> LocationList { get; set; }
    }}
