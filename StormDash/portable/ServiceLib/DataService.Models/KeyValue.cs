﻿using System;
using SQLite;
 
namespace RouteWeather.Model
{
    [Table("keyvalue")]
    public class KeyValue
    {
        [PrimaryKey]
        [Column("key")]
        public string Key { get; set;}

        [NotNull]
        [Column("value")]
        public string Value { get; set;}

        public KeyValue ()
        {
        }
    }
}

