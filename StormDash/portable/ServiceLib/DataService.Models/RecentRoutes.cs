﻿using System;
using RouteWeather.Services.DataStore;
using SQLite;

namespace RouteWeather.Model
{
    [Table(RecentRouteDataStore.RecentRouteTableName)]
    public class RecentRoutes
    {
        [PrimaryKey]
        public string PrimaryKey { get; set;}

        [Column ("sourceId"), Indexed]
        public int SourceId { get; set; }

        [Column ("destinationId"), Indexed]
        public int DestinationId { get; set; }

        [Column("count"), Indexed]
        public int Count { get; set; }

        [Column("favourite"), Indexed]
        public bool Favourite { get; set; }

        [Column("lastSelected"), Indexed]
        public DateTime LastSelected { get; set; }

        [Ignore]
        public LocationDO Source { get; set; }

        [Ignore]
        public LocationDO Destination { get; set; }

        public String RecentLocationRouteName 
        {
            get
            {
                if(Source == null || Destination == null)
                    return "";
                return Source.City + " To " + Destination.City;
            }
        }
    }
}
