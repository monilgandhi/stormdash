﻿using System;
using WeatherApp.Communication;
using System.Threading.Tasks;
using System.Net.Http;
using RouteWeather.Exceptions;
using System.IO;
using Newtonsoft.Json;
using RouteWeather.Services.Models;
using System.Collections.Generic;
using RouteWeather.Model;

namespace RouteWeather.Services
{
    public class GoogleMapsDirectionApiClient : HttpWebClientService
    {
        private const string ApiBaseUrl = @"https://maps.googleapis.com/maps/api/directions/json?";
        private const string SourceQuery = "&origin=";
        private const string DestinationQuery = "&destination=";
        private const string AlternativeQuery = "&alternatives=";

        public async Task<List<RouteInformation>> GetDirectionsAsync(LocationDO source, LocationDO destination) 
        {
            if(source == null)
            {
                throw new ArgumentNullException("source");
            }

            if(destination == null)
            {
                throw new ArgumentNullException("destination");
            }

            // prepare the url
            var googleUrl = ConstructUrl(source, destination);


            var responseStream = await this.RunQueryAsync(googleUrl, null, HttpMethod.Get);
            if (responseStream.Length == 0)
            {
                //TODO error
                System.Diagnostics.Debugger.Break ();
            }

            string responseContent = string.Empty;

            using (var sr = new StreamReader (responseStream)) 
            {
                responseContent = await sr.ReadToEndAsync ();
            }

            if (string.IsNullOrWhiteSpace (responseContent)) 
            {
                //TODO error
                System.Diagnostics.Debugger.Break ();
            }

            var result = new List<RouteInformation> ();

            var responseJsonObject = JsonConvert.DeserializeObject<GoogleMapsApiResponse>(responseContent);
            result = GetRouteInformation(responseJsonObject, source, destination);
            return result;
        }

        private static string ConstructUrl(RouteWeather.Model.LocationDO source, RouteWeather.Model.LocationDO destination)
        {
            var sourceCityName = string.Format("{0},{1}", source.City, source.State);
            var destinationCityName = string.Format("{0},{1}", destination.City, destination.State);
            var sourceCityQueryString = string.Format("{0}{1}", SourceQuery, sourceCityName);
            var destinationCityQueryString = string.Format("{0}{1}", DestinationQuery, destinationCityName);
            var alternativeQueryString = string.Format("{0}{1}", AlternativeQuery, true.ToString().ToLower());
            return string.Format("{0}{1}{2}{3}", ApiBaseUrl, sourceCityQueryString, destinationCityQueryString, alternativeQueryString);
        }

        private static List<RouteInformation> GetRouteInformation(
            GoogleMapsApiResponse apiResponse, 
            RouteWeather.Model.LocationDO source, 
            RouteWeather.Model.LocationDO destination)
        {
            var result = new List<RouteInformation>();

            foreach (var googleRoute in apiResponse.Routes)
            {
                if (string.IsNullOrWhiteSpace(googleRoute.Overview_Polyline.Points))
                {
                    throw new NullReferenceException("Points data empty");                        
                }

                var route = new RouteInformation()
                {
                    Summary = googleRoute.Summary,
                    RoutePoints = googleRoute.Overview_Polyline.Points
                };

                foreach (var googleLeg in googleRoute.Legs)
                {
                    route.Source = new Location () 
                    {
                        Latitude = googleLeg.Start_Location.lat,
                        Longitude = googleLeg.Start_Location.lng,
                        City = source.City,
                        State = source.State,
                        ZipCode = source.Zipcode.ToString()
                    };

                    route.Destination = new Location()
                    {
                        Latitude = googleLeg.End_Location.lat,
                        Longitude = googleLeg.End_Location.lng,
                        City = destination.City,
                        State = destination.State,
                        ZipCode = destination.Zipcode.ToString()
                    };

                    TimeSpan journeyTime;
                    if (!TryParseTimeSpan(googleLeg.Duration.Text, out journeyTime))
                    {
                        throw new FormatException("Time format unrecognized");
                    }

                    route.EstimatedTime = new RouteTime () 
                    {
                        NumberofDays = journeyTime.Days,
                        NumberofHours = journeyTime.Hours,
                        NumberofMinutes = journeyTime.Minutes,
                        NumberofSeconds = journeyTime.Seconds
                    };

                    double totalDistance;
                    if (!TryParseDistance(googleLeg.Distance.Text, out totalDistance))
                    {
                        throw new FormatException("Distance format unrecognized");
                    }

                    route.TotalDistanceInMiles = totalDistance;
                    route.TotalDistance = totalDistance * 1609.34;
                }

                result.Add(route);
            }

            return result;
        }

        private static bool TryParseDistance(string distanceString, out double distance)
        {
            if (string.IsNullOrWhiteSpace(distanceString))
            {
                throw new ArgumentNullException("distanceString");
            }

            // format of distance is 100 mi
            var distanceStringSplit = distanceString.Split(' ');

            return double.TryParse(distanceStringSplit[0], out distance);
        }

        private static bool TryParseTimeSpan(string timeSpan, out TimeSpan resultTimeSpan)
        {
            if (string.IsNullOrWhiteSpace(timeSpan))
            {
                throw new ArgumentNullException("timeSpan");
            }

            /**
             * The string can be of any of the following forms
             * 12 hours 10 mins, 1 hour 1 min
             * 1 day 10 hours
             * 2 days 3 hours, 1 min
             * */
            var timespanStringSplit = timeSpan.Split(' ');
            int dayNum = 0, hourNum = 0, minNum = 0;
            resultTimeSpan = new TimeSpan();
            for (var i = 0; i < timespanStringSplit.Length - 1; i += 2)
            {
                if (timespanStringSplit[i + 1].Contains("day"))
                {
                    if (!int.TryParse(timespanStringSplit[i], out dayNum))
                    {
                        return false;
                    }

                }

                if (timespanStringSplit[i + 1].Contains("hour"))
                {
                    if (!int.TryParse(timespanStringSplit[i], out hourNum))
                    {
                        return false;
                    }

                }

                if (timespanStringSplit[i + 1].Contains("min"))
                {
                    if (!int.TryParse(timespanStringSplit[i], out minNum))
                    {
                        return false;
                    }

                }
            }

            resultTimeSpan = new TimeSpan(dayNum, hourNum, minNum, 0);
            return true;
        }

    }
}

