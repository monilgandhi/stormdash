﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Communication.Interface;

namespace RouteWeather.Services
{
    public interface IWebClientService
    {
        Task<I> RunPostQueryAsync<T, I>(T requestObj, string url)
            where T : ICommunicationObject
            where I : ICommunicationObject;

        Task<IEnumerable<I>> RunPostQueryListResponseAsync<T, I>(T requestObj, string url)
            where T : ICommunicationObject
            where I : ICommunicationObject;

        Task<bool> RunPostQueryNoResponseAsync<T>(T requestObj, string url)
            where T : ICommunicationObject;
    }
}
