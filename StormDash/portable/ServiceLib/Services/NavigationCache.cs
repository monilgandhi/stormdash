﻿using System;
using System.Collections.Generic;

namespace RouteWeather.Services
{
    public class NavigationCache
    {
        private Dictionary<string, object> objectCache = new Dictionary<string, object>();
        private static NavigationCache instance = new NavigationCache();

        public static NavigationCache Instance
        {
            get
            {
                if (NavigationCache.instance == null)
                {
                    throw new NullReferenceException("Instance of Navigation Cache is null");
                }
                return NavigationCache.instance;
            }
        }

        public object GetObject(string key)
        {
            if(String.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException("key");
            }
            object value;
            if (this.objectCache.TryGetValue(key, out value))
            {
                return value;
            }
            return null;
        }

        public bool AddObject(string key, object value)
        {
            if(String.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException("key");
            }

            if(value == null)
            {
                throw new ArgumentNullException("value");
            }

            object existingValue;
            if (this.objectCache.TryGetValue(key, out existingValue))
            {
                //overwrite the value
                this.objectCache[key] = value;
            }
            else
            {
                this.objectCache.Add(key,value);
            }
            return true;
        }

        public static bool TryGetObject<T>(string key, out T obj) where T : class
        {
            if (string.IsNullOrWhiteSpace (key)) 
            {
                throw new ArgumentNullException ("key");
            }    

            obj = NavigationCache.Instance.GetObject(key) as T;
            return obj == null ? false : true;
        }

        public static void Reset()
        {
            NavigationCache.instance = new NavigationCache();
        }
    }
}
