﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using WeatherApp.Communication;
using RouteWeather.CommonLib;

namespace RouteWeather.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IWebClientService webClient;
        public FeedbackService()
        {
            this.webClient = new HttpWebClientService();
        }
        public async Task<bool> PostFeedbackAsync(FeedbackPostRequest feedback)
        {
            if (string.IsNullOrWhiteSpace(feedback?.Feedback))
            {
                throw new ArgumentNullException("feedback");
            }

            FeedbackPostResponse deserializedResponse;

            try
            {
                deserializedResponse = await this.webClient.RunPostQueryAsync<FeedbackPostRequest, FeedbackPostResponse>(feedback, GetServiceUrl());
            }
            catch (HttpRequestException ex)
            {
                return false;
            }

            return deserializedResponse.Success;
        }

        private string GetLocalUrl()
        {
            return Constants.StormDashServiceLocalBaseUrl + Constants.FeedbackUrl;
        }

        private string GetServiceUrl()
        {
            return Constants.StormdashServiceBaseUrl + Constants.FeedbackUrl;
        }
    }
}