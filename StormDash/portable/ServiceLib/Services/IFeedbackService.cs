﻿using System.Threading.Tasks;
using WeatherApp.Communication;

namespace RouteWeather.Services
{
    public interface IFeedbackService
    {
        Task<bool> PostFeedbackAsync(FeedbackPostRequest feedback);
    }
}