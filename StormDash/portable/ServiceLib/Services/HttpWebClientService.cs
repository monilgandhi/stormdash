﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using RouteWeather.Exceptions;
using WeatherApp.Communication.Interface;
using WeatherAppService.CommunicationSerializers;

namespace RouteWeather.Services
{
    public class HttpWebClientService : IWebClientService
    {
        private static HttpClient client;

        public HttpWebClientService()
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 1, 00);
        }

        public async Task<I> RunPostQueryAsync<T,I>(T requestObj, string url) 
            where T : ICommunicationObject
            where I : ICommunicationObject
        {
            if(requestObj == null || String.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException("requestObj");
            }

            //prepare the input
            MemoryStream requestStream = SerializeToStream(requestObj);

            var responseStream = await RunQueryAsync(url, requestStream.ToArray(), HttpMethod.Post);
            if (responseStream.Length == 0)
            {
                throw new EmptyHttpResponseException("Response stream is empty");
            }

            //deserialize the stream
            return CommunicationSerializer.Deserialize<I>(responseStream);
        }

        public async Task<bool> RunPostQueryNoResponseAsync<T>(T requestObj, string url)
            where T : ICommunicationObject
        {
            if (requestObj == null || String.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException("url");
            }

            //prepare the input
            MemoryStream requestStream = SerializeToStream(requestObj);

            await RunQueryAsync(url, requestStream.ToArray(), HttpMethod.Post);
            return true;
        }

        public async Task<IEnumerable<I>> RunPostQueryListResponseAsync<T, I>(T requestObj, string url)
            where T : ICommunicationObject
            where I : ICommunicationObject
        {
            if (requestObj == null || String.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException("url");
            }

            //prepare the input
            MemoryStream requestStream = SerializeToStream(requestObj);

            var responseStream = await RunQueryAsync(url, requestStream.ToArray(), HttpMethod.Post);
            if (responseStream.Length == 0)
            {
                throw new EmptyHttpResponseException("Response stream is empty");
            }

            //deserialize the stream
            return CommunicationSerializer.DeserializeList<I>(responseStream);
        }


        private MemoryStream SerializeToStream<T>(T obj) where T : ICommunicationObject
        {
            MemoryStream requestStream = new MemoryStream();

            CommunicationSerializer.Serialize(obj, requestStream);
            requestStream.Position = 0;
            return requestStream;
        }

        protected async Task<Stream> RunQueryAsync(String url, byte[] data, HttpMethod method)
        {
            if (String.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException("url");
            }
            
            var uri = new Uri(url);
            var request = new HttpRequestMessage (method, uri);

            if (data != null && data.Length > 0) 
            {
                request.Content = new ByteArrayContent(data);
            }
                
            try
            {
                var response = await client.SendAsync(request);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(response.StatusCode.ToString());
                }
                var contentStream = (StreamContent) response.Content;

                Stream responseStream = new MemoryStream();
                await contentStream.CopyToAsync(responseStream);
                responseStream.Position = 0;
                return responseStream;
            }
            catch(WebException) 
            {
                throw new HttpRequestException ("Unable to reach the server");
            }
        }  
    }
}
