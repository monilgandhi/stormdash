﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Communication;
using RouteWeather.Exceptions;
using RouteWeather.CommonLib;

namespace RouteWeather.Services
{
    public class CustomWeatherService : IWeatherService
    {
        private IWebClientService webClient;
        public CustomWeatherService() 
        {
            webClient = new HttpWebClientService();
        }

        public async Task<List<LocationInformation>> CalculateWeatherForRoute(RouteInformation route)
        {
            if (route == null)
            {
                throw new ArgumentNullException("route");
            }

            if (string.IsNullOrWhiteSpace(route.RoutePoints))
            {
                throw new NullReferenceException("Path for the given route is null");
            }

            var responseTask = await QueryServer(route);

            return responseTask;
        }
            
        private string GetLocalUrl()
        {
            return Constants.StormDashServiceLocalBaseUrl + Constants.RouteWeatherUrl;
        }

        private string GetServiceUrl()
        {
            return Constants.StormdashServiceBaseUrl + Constants.RouteWeatherUrl;
        }

        private async Task<List<LocationInformation>> QueryServer(RouteInformation commObject)
        {
            var locationInfoResponse = await webClient.RunPostQueryAsync<RouteInformation, LocationInformationResponse>(commObject, GetLocalUrl());
           
            if (locationInfoResponse == null || locationInfoResponse.WeatherLocationInformation == null)
            {
                throw new NullReferenceException("Response weather received from server is null");
            }

            if (locationInfoResponse.WeatherLocationInformation.Count == 0)
            {
                throw new EmptyHttpResponseException("Weather location information for this route is empty");
            }
            return locationInfoResponse.WeatherLocationInformation;
        }
    }
}