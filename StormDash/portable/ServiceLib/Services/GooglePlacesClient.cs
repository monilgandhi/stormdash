﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using RouteWeather.Model;
using Newtonsoft.Json;

namespace RouteWeather.Services
{
    public class GooglePlacesClient : HttpWebClientService
    {
        private const string BaseUrl = @"https://maps.googleapis.com/maps/api/place/autocomplete/json?";
        private const string QueryArgument = "input=";
        private const string ApiArgument = "&key=";
        private const string ApiKey = "AIzaSyBXKPJ3AnRvokvaXfQj2vXZuFGz91hYehI";
        private const string TypesParam = "&types=(cities)";
        private const string Constraints = "&components=country:us";

        public async Task<List<LocationDO>> AutoComplete(string query)
        {
            List<LocationDO> locations = new List<LocationDO>();
            if (string.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentNullException(query);
            }

            string googlePlaceUrl = ConstructUrl(query);

            try
            {
                Stream responseStream = await this.RunQueryAsync(googlePlaceUrl, null, HttpMethod.Get);
                if (responseStream.Length == 0)
                {
                    throw new HttpRequestException("Unable to get places from google places");
                }

                string responseContent = string.Empty;
                GooglePlaceApiResponse googlePlaceApiResponse = null;
                using (var sr = new StreamReader(responseStream))
                {
                    responseContent = await sr.ReadToEndAsync();
                    googlePlaceApiResponse = JsonConvert.DeserializeObject<GooglePlaceApiResponse>(responseContent);
                }
                    
                if (googlePlaceApiResponse == null || googlePlaceApiResponse.predictions == null || googlePlaceApiResponse.predictions.Count == 0)
                {
                    return locations;
                }

                foreach(var prediction in googlePlaceApiResponse.predictions)
                {
                    if(prediction.terms == null || prediction.terms.Count < 2)
                    {
                        continue;
                    }

                    locations.Add(new LocationDO()
                    {
                        City =  prediction.terms[0].value,
                        State = prediction.terms[1].value
                    });
                }

                return locations;
            }
            catch(JsonSerializationException)
            {
                return locations;
            }
        }

        private static string ConstructUrl(string query)
        {
            return BaseUrl + QueryArgument + query + ApiArgument + ApiKey + TypesParam + Constraints;
        }
    }
}