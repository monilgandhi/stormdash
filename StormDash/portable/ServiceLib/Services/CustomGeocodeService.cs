﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RouteWeather.Exceptions;
using WeatherApp.Communication;
using WeatherAppService.CommunicationSerializers;

namespace RouteWeather.Services
{
    public class CustomGeocodeService : IGeocoderService
    {
        private IWebClientService webClient;
        private string url = @"http://weatherapp.cloudapp.net/api/geocode/geocodebatch";
        //private const string url = @"http://169.254.80.80/weatheronroute/api/geocode/";
        public CustomGeocodeService()
        {
            webClient = new HttpWebClientService();
        }

        public async Task PrefetchReverseGeocoding(RouteInformation route)
        {
            if (route == null)
            {
                throw new ArgumentNullException("route is null");
            }

            if (string.IsNullOrWhiteSpace(route.RoutePoints))
            {
                throw new NullReferenceException("Path for the given route is null");
            }

            var prefetchReverseGeocodingUrl = url + "PreReverseGeocodeRoute";
            await webClient.RunPostQueryNoResponseAsync(route, prefetchReverseGeocodingUrl);
        }

        public async Task<List<Geocoordinate>> BatchGeocode(GeocodeAddress commObjects)
        {
            if (commObjects == null)
            {
                throw new ArgumentNullException("addresses");
            }

            //prepare the input
            MemoryStream requestStream = new MemoryStream();

            CommunicationSerializer.Serialize(commObjects, requestStream);
            requestStream.Position = 0;
            if (requestStream.Length == 0)
            {
                throw new EmptyHttpResponseException("Empty response received by geocoding service");
            }

            var geocodeBatchUrl = url + "geocodebatch";
            var deserializedList = await webClient.RunPostQueryListResponseAsync<GeocodeAddress, Geocoordinate>(commObjects, geocodeBatchUrl);
            return deserializedList.ToList();
        }
    }
}
