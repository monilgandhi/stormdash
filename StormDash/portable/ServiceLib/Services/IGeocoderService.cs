﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Communication;

namespace RouteWeather.Services
{
    public interface IGeocoderService
    {
        Task<List<Geocoordinate>> BatchGeocode(GeocodeAddress commObjects);
        Task PrefetchReverseGeocoding(RouteInformation route);
    }
}
