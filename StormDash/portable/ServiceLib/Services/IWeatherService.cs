﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Communication;

namespace RouteWeather.Services
{
    public interface IWeatherService
    {
        Task<List<LocationInformation>> CalculateWeatherForRoute(RouteInformation route);
    }
}
