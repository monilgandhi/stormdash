﻿using System;
using System.Threading.Tasks;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.CommonLib;

namespace RouteWeather.Services.Log
{
    public class ClientLogger
    {
        private static IWebClientService webClient = new HttpWebClientService();
        
        public static async Task Error(string className, Exception ex, string platform)
        {
            if (ex == null) return;
            var errorLog = new ClientLog()
            {
                ClassName = className,
                Message = ex.Message,
                ErrorType = ClientErrorType.Error,
                StackTrace = ex.StackTrace,
                Platform = platform,
                ExceptionType = ex.GetType().ToString()
            };

            webClient.RunPostQueryNoResponseAsync(errorLog, GetLocalUrl());
        }

        public static async Task Error(string className, string message, string platform)
        {
            if (string.IsNullOrEmpty(message)) return;
            var errorLog = new ClientLog()
            {
                ClassName = className,
                Message = message,
                ErrorType = ClientErrorType.Error,
                Platform = platform
            };

            webClient.RunPostQueryNoResponseAsync(errorLog, GetLocalUrl());
        }

        private static string GetServiceUrl()
        {
            return Constants.StormdashServiceBaseUrl + Constants.LogUrl;
        }

        private static string GetLocalUrl()
        {
            return Constants.StormDashServiceLocalBaseUrl + Constants.LogUrl;
        }
    }
}
