﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommunicationProtocol.Serializers;
using WeatherApp.Communication.Interface;

namespace WeatherAppService.CommunicationSerializers
{
    public static class CommunicationSerializer
    {
        private static ICommunicationSerializer _serializer;

        private static ICommunicationSerializer GetSerializer()
        {
            if (_serializer == null)
            {
                SetCurrentSerializer();
            }
            return _serializer;
        }

        private static void SetCurrentSerializer()
        {
            if (_serializer == null)
            {
                _serializer = new ProtobufSerializer();
            }
        }

        public static void Serialize<T>(T obj, Stream stream) where T : ICommunicationObject
        {
            if (EqualityComparer<T>.Default.Equals(obj, default(T)) || stream == null)
            {
                throw new ArgumentException("obj/stream is null");
            }
            GetSerializer().Serialize<T>(obj,stream);
        }

        public static void SerializeList<T>(IEnumerable<T> objEnumerable, Stream stream) where T : ICommunicationObject
        {
            if (objEnumerable == null || !objEnumerable.Any() || stream == null)
            {
                throw new ArgumentException("obj/stream is null");
            }
            var objList = objEnumerable.ToList();
            foreach (var obj in objList)
            {
                if (obj == null)
                {
                    throw new NullReferenceException("Cannot serailize a null object");
                }
                GetSerializer().Serialize(obj, stream);
            }
        }

        public static IEnumerable<T> DeserializeList<T>(Stream stream) where T : ICommunicationObject
        {
            if (stream == null || stream.Length == 0)
            {
                throw new ArgumentException("stream is null");
            }

            return GetSerializer().DeserializeList<T>(stream);
        }

        public static T Deserialize<T>(Stream stream) where T : ICommunicationObject
        {
            if (stream == null || stream.Length == 0)
            {
                throw new ArgumentException("stream is null");
            }

            return GetSerializer().Deserialize<T>(stream);
        }
    }
}