﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using WeatherApp.Communication.Interface;

namespace CommunicationProtocol.Serializers
{
    internal interface ICommunicationSerializer
    {
        void Serialize<T>(T obj, Stream stream) where T : ICommunicationObject;

        void SerializeList<T>(IEnumerable<T> obj, Stream stream) where T : ICommunicationObject;
        T Deserialize<T>(Stream requestedStream) where T : ICommunicationObject;
        IEnumerable<T> DeserializeList<T>(Stream requestedStream) where T : ICommunicationObject;
    }
}
