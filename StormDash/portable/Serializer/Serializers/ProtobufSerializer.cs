﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using WeatherApp.Communication.Interface;

namespace CommunicationProtocol.Serializers
{
    internal class ProtobufSerializer:ICommunicationSerializer
    {
        public void Serialize<T>(T obj, Stream stream) where T : ICommunicationObject
        {
            ProtoBuf.Serializer.SerializeWithLengthPrefix(stream, obj, ProtoBuf.PrefixStyle.Base128, 0);
        }

        public void SerializeList<T>(IEnumerable<T> obj, Stream stream) where T : ICommunicationObject
        {
            ProtoBuf.Serializer.SerializeWithLengthPrefix(stream, obj, ProtoBuf.PrefixStyle.Base128, 0);
        }


        public T Deserialize<T>(Stream requestedStream) where T : ICommunicationObject
        {
            T deserializedObject = default(T);
            if (requestedStream.Length == 0)
            {
                //TODO something wrong. log and return
                Debugger.Break();
                return deserializedObject;
            }

            try
            {
                deserializedObject = ProtoBuf.Serializer.DeserializeWithLengthPrefix<T>(requestedStream, ProtoBuf.PrefixStyle.Base128);
            }
            catch (Exception ex)
            {
                 
                Debugger.Break();
            }
            return deserializedObject;
        }

        public IEnumerable<T> DeserializeList<T>(Stream requestedStream) where T : ICommunicationObject
        {
            IEnumerable<T> deserializeList = default(IEnumerable<T>);
            if (requestedStream.Length == 0)
            {
                //TODO something wrong. log and return
                Debugger.Break();
                return null;
            }

            try
            {
                deserializeList = ProtoBuf.Serializer.DeserializeItems<T>(requestedStream, ProtoBuf.PrefixStyle.Base128, 0);
            }
            catch (Exception ex)
            {
                //TODO handle the exceptions
                Debugger.Break();
            }
            return deserializeList;
        }

    }
}