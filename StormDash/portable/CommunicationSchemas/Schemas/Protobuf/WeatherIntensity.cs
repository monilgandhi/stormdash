﻿namespace CommunicationSchemas.Schemas.Protobuf
{
    public enum WeatherIntensity
    {
        None,
        Very_Light,
        Light,
        Moderate,
        Heavy
    }
}
