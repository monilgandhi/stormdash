﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    /// <summary>
    /// This class is used to send the weather location from the server to the client. Sends all the cities enroute
    /// </summary>
    [ProtoContract]
    public class LocationInformationResponse : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public List<LocationInformation> WeatherLocationInformation;

        [ProtoMemberAttribute(2)] 
        public ushort DistanceBetweenCities;

        [ProtoMemberAttribute(4)]
        public ushort TotalCitiesWithWeather;

        [ProtoMemberAttribute(5)]
        public ErrorCode errorCode;

    }
}

