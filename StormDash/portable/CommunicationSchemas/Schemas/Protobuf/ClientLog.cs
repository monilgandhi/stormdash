﻿using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace CommunicationSchemas.Schemas.Protobuf
{
    [ProtoContract]
    public class ClientLog : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public string Message;
        [ProtoMemberAttribute(2)]
        public string StackTrace;
        [ProtoMemberAttribute(3)]
        public string ClassName;
        [ProtoMemberAttribute(4)]
        public string Platform;
        [ProtoMemberAttribute(5)]
        public ClientErrorType ErrorType;
        [ProtoMemberAttribute(6)]
        public string ExceptionType;
    }
}
