﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class RouteTime : ICommunicationObject
    {
        [ProtoMemberAttribute(1)] 
        public double NumberofDays;

        [ProtoMemberAttribute(2)]
        public double NumberofHours;

        [ProtoMemberAttribute(3)]
        public double NumberofMinutes;

        [ProtoMemberAttribute(4)]
        public double NumberofSeconds;
    }
}
