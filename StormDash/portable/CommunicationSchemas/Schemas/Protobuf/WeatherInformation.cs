﻿using System;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class WeatherInformation : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public ulong ValidTime { get; set; }

        [ProtoMemberAttribute(2)]
        public ushort? PrecipitationChances { get; set; }

        [ProtoMemberAttribute(3)]
        public float? SnowfallValue { get; set; }

        [ProtoMemberAttribute(4)]
        public Int16? Temperature { get; set; }
        [ProtoMemberAttribute(5)]
        public Int16? MaxTemperature { get; set; }
        [ProtoMemberAttribute(6)]
        public Int16? MinTemperature { get; set; }
        [ProtoMemberAttribute(7)]
        public UInt16? CloudCoverPercentage { get; set; }
        [ProtoMemberAttribute(8)]
        public String IconPath { get; set; }
        [ProtoMemberAttribute(9)]
        public ThreeHourlyWeatherSummary ThreeHourlyWeatherSummary { get; set; }
    }
}
