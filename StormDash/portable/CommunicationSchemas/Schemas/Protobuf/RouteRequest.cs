﻿using ProtoBuf;
using WeatherApp.Communication;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class RouteRequest : ICommunicationObject
    {
        [ProtoMember(1)]
        public Location Source;
        [ProtoMember(2)]
        public Location Destination;
    }
}
