﻿using System;
using System.Collections.Generic;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class GeocodeAddress : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public List<String> RequestedAddress;
    }
}
