﻿using System;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    /// <summary>
    /// This class is location information per city. List of all the cities is sent to client in LocationInformationResponse
    /// </summary>
    [ProtoContract]
    public class LocationInformation : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public String City { get; set; }

        [ProtoMemberAttribute(2)]
        public String State { get; set; }

        [ProtoMemberAttribute(3)]
        public String ZipCode { get; set; }

        [ProtoMemberAttribute(4)]
        public WeatherInformation Weather { get; set; }

        [ProtoMemberAttribute(5)]
        public ulong ApproximateArrivalTime { get; set; }

        [ProtoMemberAttribute(6)]
        public double DistanceFromStartPoint { get; set; }

        [ProtoMemberAttribute(7)]
        public Geocoordinate Coordinates { get; set; }
    }
}
