﻿using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class FeedbackPostRequest : ICommunicationObject
    {
        [ProtoMemberAttribute(1)] public string FeedbackEmail;
        [ProtoMemberAttribute(2)] public string Feedback;
    }
}
