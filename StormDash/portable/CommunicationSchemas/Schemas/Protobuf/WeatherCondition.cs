﻿using System.Runtime.Serialization;
using CommunicationSchemas.Schemas.Protobuf;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    [DataContract]
    public class WeatherCondition : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public WeatherCoverage weatherCoverage;
        [ProtoMemberAttribute(2)]
        public WeatherIntensity weatherIntensity;
        [ProtoMemberAttribute(3)]
        public WeatherType weatherType;
    }
}
