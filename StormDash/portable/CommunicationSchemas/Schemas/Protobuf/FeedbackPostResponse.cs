﻿using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class FeedbackPostResponse : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public bool Success;
    }
}