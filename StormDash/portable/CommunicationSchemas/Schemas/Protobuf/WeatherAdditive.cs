﻿namespace CommunicationSchemas.Schemas.Protobuf
{
    public enum WeatherAdditive
    {
        None,
        Or,
        And
    }
}
