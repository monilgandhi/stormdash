﻿namespace CommunicationSchemas.Schemas.Protobuf
{
    public enum WeatherCoverage
    {
        None,
        Slight_Chance,
        Chance,
        Likely,
        Occasional,
        Definitely,
        Isolated,
        Scattered,
        Numerous,
        Patchy,
        Areas,
        Widespread,
        PeriodsOf,
        Frequent,
        Intermittent
    }
}