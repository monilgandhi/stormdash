﻿using System;
using System.Runtime.Serialization;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    [DataContract]
    public class Geocoordinate : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public float Longitude { get; set; }
        [ProtoMemberAttribute(2)]
        public float Latitude { get; set; }
    }
}
