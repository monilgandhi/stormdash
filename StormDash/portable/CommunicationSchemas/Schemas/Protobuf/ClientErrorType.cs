﻿namespace CommunicationSchemas.Schemas.Protobuf
{
    public enum ClientErrorType
    {
        Error,
        Warning,
        Debug
    }
}
