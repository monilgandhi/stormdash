﻿namespace CommunicationSchemas.Schemas.Protobuf
{
    public enum WeatherType
    {
        None,
        Drizzle,
        Freezing_Drizzle,
        Rain_Showers,
        Freezing_Rain,
        Rain,
        Snow,
        Hail,
        Frost,
        Snow_Showers,
        Blowing_Snow,
        Blowing_Dust,
        Ice_Pellets,
        Thunderstorms,
        Fog,
        Haze,
        Smoke,
        Clear,
        Cloudy
    }
}