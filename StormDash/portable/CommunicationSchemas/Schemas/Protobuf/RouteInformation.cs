﻿using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    public class RouteInformation : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public Location Source;
        [ProtoMemberAttribute(2)]
        public Location Destination;
        [ProtoMemberAttribute(3)]
        public string RoutePoints;
        [ProtoMemberAttribute(4)]
        public RouteTime EstimatedTime;
        [ProtoMemberAttribute(5)]
        public double TotalDistance;
        [ProtoMemberAttribute(6)]
        public bool IsDebug;
        [ProtoMemberAttribute(7)]
        public ulong DateUTC;
        [ProtoMemberAttribute(8)]
        public string Summary;
        [ProtoMemberAttribute(9)]
        public double TotalDistanceInMiles;
    }
}
