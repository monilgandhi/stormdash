﻿namespace WeatherApp.Communication
{
    public enum ErrorCode
    {
        NoError = 0,
        RequestError = 1,
        GeocoderError = 2,
        WeatherFetchError = 3
    }
}
