﻿using System;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    /// <summary>
    /// This class is used to sync the location from the database to users device
    /// </summary>

    [ProtoContract]
    public class Location : ICommunicationObject
    {
        [ProtoMemberAttribute(1)]
        public String City { get; set; }

        [ProtoMemberAttribute(2)]
        public String State { get; set; }

        [ProtoMemberAttribute(3)]
        public String ZipCode { get; set; }
        [ProtoMemberAttribute(4)]
        public float Latitude { get; set; }
        [ProtoMemberAttribute(5)]
        public float Longitude { get; set; }
    }
}
