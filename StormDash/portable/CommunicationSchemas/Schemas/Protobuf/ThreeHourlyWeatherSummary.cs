﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using CommunicationSchemas.Schemas.Protobuf;
using ProtoBuf;
using WeatherApp.Communication.Interface;

namespace WeatherApp.Communication
{
    [ProtoContract]
    [DataContract]
    public class ThreeHourlyWeatherSummary : ICommunicationObject
    {
        [XmlArray("WeatherConditions")]
        [XmlArrayItem("WeatherCondition")]
        [ProtoMemberAttribute(1)]
        public List<WeatherCondition> WeatherConditions;

        [XmlArray("WeatherAddtives")]
        [XmlArrayItem("WeatherAddtive")]
        [ProtoMemberAttribute(2)]
        public List<WeatherAdditive> WeatherAddtives;
    }
}
