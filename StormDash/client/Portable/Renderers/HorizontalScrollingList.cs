﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Text;

namespace RouteWeather.Pcl.Renderer
{
    public class HorizontalScrollingList : ScrollView
    {
        public HorizontalScrollingList()
        {
            this.Orientation = ScrollOrientation.Horizontal;
        }

        public static readonly BindableProperty ItemSourceProperty = 
            BindableProperty.Create<HorizontalScrollingList, IList<string>>(prop => prop.ItemSource, null);

        public IList<string> ItemSource
        {
            get{ return (List<string>)base.GetValue (ItemSourceProperty); }
            set
            {
                base.SetValue (ItemSourceProperty, value); 
                SetContent ();
            }
        }

        public int SelectedItemIndex { get; set; }

        private void SetContent()
        {
            var stackLayout = new StackLayout ();
            stackLayout.Orientation = StackOrientation.Horizontal;
            stackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;

            var paddingSize = 20;
            foreach (var item in ItemSource) 
            {
                var currentGrid = new Grid () 
                {
                    Padding = new Thickness(paddingSize,0,paddingSize,0)
                };
                    
                var label = new Label () 
                {
                    Text = item,
                };
                currentGrid.Children.Add(label);
                stackLayout.Children.Add (currentGrid);
            }
            this.Content = stackLayout;
        }
    }
}

