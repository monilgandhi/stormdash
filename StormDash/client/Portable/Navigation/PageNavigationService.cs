﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace RouteWeather.Pcl.Navigation
{
    public class PageNavigationService : IPageNavigationService
    {
        private INavigation navInstance;
        public PageNavigationService ()
        {
        }

        public void Initialize(INavigation navInstance)
        {
            this.navInstance = navInstance;
        }

        public async Task PopAsync()
        {
            await this.navInstance.PopAsync();
        }

        public async Task PopModalAsync()
        {
            await this.navInstance.PopModalAsync ();
        }

        public async Task PushModalAsync(Page page)
        {
            await this.navInstance.PushModalAsync (page);
        }

        public async Task PushAsync(Page page)
        {
            await this.navInstance.PushAsync (page);
        }
    }
}