﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RouteWeather.Pcl.Navigation
{
    public interface IPageNavigationService
    {
        void Initialize (INavigation navInstance);
        Task PopAsync();
        Task PopModalAsync();
        Task PushAsync(Page page);
        Task PushModalAsync(Page page);
    }
}

