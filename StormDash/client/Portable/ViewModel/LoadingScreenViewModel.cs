﻿using System;
using RouteWeather.Services.DataStore;
using RouteWeather.Pcl.Navigation;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using RouteWeather.Pcl.Common;
using RouteWeather.Pcl.Views;
using System.Diagnostics;

namespace RouteWeather.Pcl.ViewModel
{
    public class LoadingScreenViewModel : CustomViewModelBase
    {
        private ILocationDataStoreService locationDataStoreService;
        private IKeyValueDataStore keyvalueDatastore;
        private IPageNavigationService pageNavigation;
        private List<string> loadingLabelValues;
        private int currentIterator = 0;
        // private Timer loadingValueTimer;

        public LoadingScreenViewModel(ILocationDataStoreService locationDataStoreService, IKeyValueDataStore keyvalueDatastore, IPageNavigationService pageNavigation)
        {
            this.locationDataStoreService = locationDataStoreService;
            this.keyvalueDatastore = keyvalueDatastore;
            this.pageNavigation = pageNavigation;
            this.loadingLabelValues = new List<string>
                {
                    "Loading..please wait"
                };

            // this.loadingValueTimer = new Timer(NextLoadingLabelValue, null, 5000, 25000);
        }

        public async override Task OnAppearingAsync(IDictionary<string, object> args)
        {
            await Task.Run(() => InitialSetup());
            // this.loadingValueTimer.Dispose();
            await this.pageNavigation.PushAsync(new RouteInfoPage());
        }

        public string NextLabel
        {
            get 
            {
                if (currentIterator < this.loadingLabelValues.Count - 1)
                {
                    Debug.WriteLine("Incrementing iterator");
                    ++currentIterator;
                }

                /*if (currentIterator == 3)
                {
                    this.loadingValueTimer.Cancel();
                }*/
                return this.loadingLabelValues[currentIterator]; 
            }
        }

        public void NextLoadingLabelValue(object state)
        {
            Debug.WriteLine("Loading new value triggered");
            RaisePropertyChanged("NextLabel");
        }

        private async Task InitialSetup()
        {
            var value = await this.keyvalueDatastore.Get<String>("InitialSetupComplete");
            bool isInitialSetupComplete;

            if (!bool.TryParse (value, out isInitialSetupComplete) || !isInitialSetupComplete) {
                //this is first time user.
                // set up the db
                string contents = RouteWeatherUtility.ReadResourcesFileContentsAsync ("RouteWeather.Pcl.Resources.locations.proto");
                await this.locationDataStoreService.Populate (contents);
            }

            // verify if we have all the locations or not
            var totalLocationCount = await Task.Run(() => this.locationDataStoreService.GetLocationCount());

            if (totalLocationCount > 0)
            {
                // add the key
                var success = await this.keyvalueDatastore.Set<String>("InitialSetupComplete", "true");

                if (!success)
                {
                    System.Diagnostics.Debugger.Break();
                }
            }
            else
            {
                // TODO log. This should not happen
            }
        }
    }
}

