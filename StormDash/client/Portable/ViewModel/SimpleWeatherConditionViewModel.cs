﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.BusinessLogic.Model;
using RouteWeather.CommonLib;
using WeatherApp.Communication;
using RouteWeather.Pcl.Common;
using System.Text;

namespace RouteWeather.Pcl.ViewModel
{
    public class SimpleWeatherConditionViewModel
    {
        private WeatherType weatherType;
        private WeatherIntensity? weatherIntensity;
        private double startDistanceCurrentWeather;
        private string sourceCity;
        private UInt16? cloudCoverPercent;
        private ulong startTimeUtc;
        private AssetManager assetManagerRef;
        private string lastCityName;
        private double totalMilesBetweenStartAndLastCity;

        public SimpleWeatherConditionViewModel(WeatherType type, WeatherIntensity? intensity, List<LocationInformation> locations, string sourceCity)
        {
            if (locations == null || locations.Count == 0) 
            {
                throw new ArgumentNullException ("location");
            }

            if (!AssetManager.TryGetResourceDictionaryInstance (out this.assetManagerRef)) 
            {
                // TODO log
            }

            this.weatherType = type;
            this.weatherIntensity = intensity;
            this.startDistanceCurrentWeather = Utils.GetDistanceInLocalCulture (locations.First().DistanceFromStartPoint);
            this.sourceCity = sourceCity;
            this.cloudCoverPercent = Utils.GetAvgCloudCoverPercent (locations);
            this.startTimeUtc = locations.First ().ApproximateArrivalTime;
            this.lastCityName = locations.Last ().City;
            this.totalMilesBetweenStartAndLastCity = Utils.GetDistanceInLocalCulture (locations.Last().DistanceFromStartPoint) - this.startDistanceCurrentWeather;
        }

        private string GetSummaryTextForCloudy()
        {
            var text = new StringBuilder ("Cloud overcast starting ");
            if (this.startDistanceCurrentWeather > 0) 
            {
                text.AppendFormat (" {0} miles ", this.startDistanceCurrentWeather);
            }

            text.AppendFormat ("from {0} to {1}", this.sourceCity, this.lastCityName);
            return text.ToString ();
        } 

        private string GetSummaryTextForClearWeather()
        {
            var sb = new StringBuilder("Clear Weather ");

            if (this.startDistanceCurrentWeather > 0)
            {
                sb.Append("starts at ");
                sb.Append(this.startDistanceCurrentWeather);
                sb.Append(" miles");
            }
            else
            {
                sb.Append("starts in ");
                sb.Append(this.sourceCity);
            }

            sb.Append(" and continues for ");
            sb.Append(this.totalMilesBetweenStartAndLastCity);
            sb.Append(" miles");
            return sb.ToString();
        }

        public string SummaryText
        {
            get
            {
                if (this.weatherType == CommunicationSchemas.Schemas.Protobuf.WeatherType.Clear) 
                {
                    return this.GetSummaryTextForClearWeather();
                }

                if (this.weatherType == CommunicationSchemas.Schemas.Protobuf.WeatherType.Cloudy) 
                {
                    return this.GetSummaryTextForCloudy();
                }

                var weatherTypeString = this.weatherType.ToString ().Replace('_', ' ');

                StringBuilder weatherIntensityString = new StringBuilder ();

                if (weatherIntensity != WeatherIntensity.None) 
                {
                    weatherIntensityString.Append(this.weatherIntensity.ToString ().Replace ('_', ' '));
                }

                var text = new StringBuilder(weatherIntensityString.ToString() + " " + weatherTypeString + " starts ");
                if (this.startDistanceCurrentWeather > 0) 
                {
                    text.Append(this.startDistanceCurrentWeather.ToString ());
                    text.Append(" miles ");
                    text.Append(" from ");
                }
                else 
                {
                    text.Append(" in ");
                } 

                text.Append(this.sourceCity);

                if (this.totalMilesBetweenStartAndLastCity > 0)
                {
                    text.Append(" and continues for  " + this.totalMilesBetweenStartAndLastCity + " miles.");
                }
                    
                text.AppendLine ();

                return text.ToString();
            }
        }

        public string SummaryImage
        {
            get 
            {
                return this.assetManagerRef.GetWeatherConditionIconPath (this.weatherType, this.startTimeUtc, this.cloudCoverPercent);
            }
        }
    }
}