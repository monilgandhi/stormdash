﻿using System.Threading.Tasks;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Command;
using RouteWeather.CommonLib;
using RouteWeather.Model;
using RouteWeather.Services;
using RouteWeather.Services.DataStore;
using RouteWeather.Pcl.Navigation;
using RouteWeather.Pcl.Views;

namespace RouteWeather.Pcl.ViewModel
{
    public class RouteInfoViewModel : CustomViewModelBase
    {
        #region Services
        ILocationDataStoreService locationDataStoreService;
        IKeyValueDataStore keyvalueDatastore;
        IRecentLocationDataStoreService recentLocationDataStoreService;
        #endregion

        #region Private Members
        private LocationDO sourceObj;
        private LocationDO destinationObj;
        private bool sourceInputActive;
        private bool destinationInputActive;
        private bool canFetchRoute;
        private List<RecentRoutes> recentSelectedLocations;
        #endregion

        public RelayCommand FindRoute { get; private set; }

        public string LocationPlaceHolderText
        {
            get { return Constants.LocationPlaceHolder;}
        }

        private string destination;
        public string Destination
        {
            get
            {
                return this.destination;
            }
            private set
            {
                this.destination = value; 
                RaisePropertyChanged();
            }
        }

        private string source;
        public string Source
        {
            get
            {
                return this.source;
            }
            private set
            {
                this.source = value;
                RaisePropertyChanged();
            }
        }

        public List<DualLineText> RecentSelectedLocation 
        {
            get 
            {
                var result = new List<DualLineText> ();
                if (this.recentSelectedLocations == null) 
                {
                    return result;
                }

                for (int i = 0; i < this.recentSelectedLocations.Count; i++) 
                {
                    string title = string.Format ("{0} to {1}", this.recentSelectedLocations [i].Source.City, this.recentSelectedLocations [i].Destination.City);
                    result.Add (new DualLineText (title, string.Empty, i));
                }

                RaisePropertyChanged ("IsRecentRouteVisible");
                return result;
            }
                
        }

        public bool IsRecentRouteVisible 
        {
            get
            {
                return this.recentSelectedLocations != null && this.recentSelectedLocations.Count > 0;
            }
        }

        public RouteInfoViewModel(
            ILocationDataStoreService locationDataStoreService, 
            IKeyValueDataStore keyvalueDatastore, 
            IPageNavigationService pageNavigation,
            IRecentLocationDataStoreService recentLocationDataStore)
            :base(pageNavigation)
        {
            FindRoute = new RelayCommand(FindRouteButtonCommand, CanExecuteFindRoute);
            //For now we have only one restriction. In case we want to add in future more, we will have to select only one.
            //TODO add route optimization
            this.recentLocationDataStoreService = recentLocationDataStore;
            this.locationDataStoreService = locationDataStoreService;
            this.keyvalueDatastore = keyvalueDatastore;
        }

        #region Navigation Overrides

        public override async Task OnAppearingAsync (IDictionary<string, object> args)
        {
            this.recentSelectedLocations = await this.recentLocationDataStoreService.GetRecentlyUsedLocationsAsync ();
            RaisePropertyChanged ("RecentSelectedLocation");

            // check if source is set
            LocationDO cacheObject;
            if (NavigationCache.TryGetObject<RouteWeather.Model.LocationDO>(Constants.AutocompleteSelectedItemCacheParam, out cacheObject)) 
            {
                if (sourceInputActive) 
                {
                    sourceObj = cacheObject;
                    this.Source = GetLocationString (this.sourceObj);
                    FindRoute.RaiseCanExecuteChanged ();
                    sourceInputActive = false;
                }
                else if (destinationInputActive) 
                {
                    destinationObj = cacheObject;
                    this.Destination = GetLocationString (this.destinationObj);
                    FindRoute.RaiseCanExecuteChanged ();
                    destinationInputActive = false;
                }
            }
        }

        public void RecentLocationSelected (int index)
        {
            RecentRoutes recentLocation = this.recentSelectedLocations [index];
            this.sourceObj = recentLocation.Source;
            this.destinationObj = recentLocation.Destination;
            FindRouteButtonCommand ();
        }

        public async Task Autocomplete(bool isSource)
        {
            if(isSource)
            {
                sourceInputActive = true;
                destinationInputActive = false;
            }
            else
            {
                destinationInputActive = true;
                sourceInputActive = false;
            }

            await this.NavigationService.PushAsync (new AutocompletePage());                
        }

        private static string GetLocationString(RouteWeather.Model.LocationDO locationObj)
        {
            return locationObj.City + Constants.CityStateSeperator + locationObj.State;
        }

        private void Reset()
        {
            //reset the navigation cache
            NavigationCache.Reset();
            this.sourceObj = null;
            this.destinationObj = null;
        }
        #endregion

        #region RelayCommand
        public bool CanExecuteFindRoute()
        {
            var isActive = this.sourceObj != null && this.destinationObj != null;
            if (canFetchRoute != isActive) 
            {
                canFetchRoute = isActive;
                this.ChangeButtonBorderColor (isActive);
                RaisePropertyChanged ("FindRoute");
            }
            return canFetchRoute;
        }

        public async void FindRouteButtonCommand()
        {
            NavigationCache.Instance.AddObject(Constants.SourceCacheParam, sourceObj);
            NavigationCache.Instance.AddObject(Constants.DestinationCacheParam, destinationObj);
            await this.recentLocationDataStoreService.AddAsync (sourceObj, destinationObj);
            await this.NavigationService.PushAsync (new RouteSelectionPage ());
        }
            
        #endregion

        #region Private Functions
        private void UpdateDestinationLocationFromCache()
        {
            this.destinationObj = NavigationCache.Instance.GetObject(Constants.DestinationCacheParam) as RouteWeather.Model.LocationDO;
            if(this.destinationObj == null)
            {
                //this can be null in case the user did not select any thing.
                return;
            }

            this.Destination = this.destinationObj.City + ", " + this.destinationObj.State;
        }

        private void UpdateSourceLocationFromCache()
        {
            this.sourceObj = NavigationCache.Instance.GetObject(Constants.SourceCacheParam) as RouteWeather.Model.LocationDO;
            if (this.sourceObj == null)
            {
                //this can be null in case the user did not select any thing.
                return;
            }
            this.Source = this.sourceObj.City + ", " + this.sourceObj.State;
        }

        #endregion
    }
}