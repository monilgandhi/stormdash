﻿using System;
using RouteWeather.CommonLib;
using Model = RouteWeather.Model;
using WeatherApp.Communication;
using GalaSoft.MvvmLight.Command;
using RouteWeather.Services;
using RouteWeather.Pcl.Views;
using RouteWeather.Pcl.Navigation;

namespace RouteWeather.Pcl.ViewModel
{
    public class RouteOption : CustomViewModelBase
    {
        #region Private members
        private RelayCommand selectRoute;
        private bool isSelected;
        private RouteInformation routeInfo;
        private RelayCommand DisplayMapCommand;
        #endregion


        #region Private Functions
        #endregion

        #region Constructor

        internal RouteOption( RouteInformation routeInfo, int index, IPageNavigationService navigation, bool isSelected = false)
            :base (navigation)
        {
            this.isSelected = isSelected;
            this.selectRoute = new RelayCommand (RouteSelected);
            this.Summary = routeInfo.Summary;
            this.RouteDistanceInformation = Utils.FormateEstimatedTime(routeInfo) + " / " +
                Utils.GetDistanceInLocalCulture(routeInfo.TotalDistance) + " " +
                Utils.GetDistanceUnits();
            this.Index = index;
            this.routeInfo = routeInfo;
            this.DisplayMapCommand = new RelayCommand (DisplayMapInitiated);
        }

        #endregion

        #region ViewModel Bindings

        public String RouteDistanceInformation { get; private set; }

        public string Summary 
        {
            get; private set; 
        }

        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                this.isSelected = value;
                RaisePropertyChanged("IsSelected");
                RaisePropertyChanged("IsNotSelected");
            }
        }

        public bool IsNotSelected
        {
            get { return !this.isSelected; }
        }

        public RelayCommand RouteSelectedCommand
        {
            get{ return this.selectRoute; }
        }
        #endregion

        public int Index 
        {
            get;
            private set;
        }

        public RouteInformation RouteDetails 
        {
            get 
            {
                return this.routeInfo;
            }
        }

        public RelayCommand DisplayMap
        {
            get{ return this.DisplayMapCommand; }
        }

        #region Commands
        private void RouteSelected()
        {
            this.IsSelected = true;
        }

        private async void DisplayMapInitiated()
        {
            // navigate to the page
            await this.NavigationService.PushAsync(new MapPage(this.RouteDetails));
        }
        #endregion
    }
}

