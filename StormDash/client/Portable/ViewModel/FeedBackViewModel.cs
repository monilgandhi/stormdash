﻿using System;
using RouteWeather.Pcl.Navigation;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;
using WeatherApp.Communication;
using RouteWeather.Services;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RouteWeather.Pcl.ViewModel
{
    public class FeedBackViewModel : CustomViewModelBase
    {
        private RelayCommand SubmitFeedbackCommand;
        private readonly IFeedbackService feedbackService;

        public bool HasTextChangedAtleastOnce { get; private set; }

        public FeedBackViewModel(IPageNavigationService pageNavigation, IFeedbackService feedbackService)
            :base(pageNavigation)
        {
            this.feedbackService = feedbackService;
            this.SubmitFeedbackCommand = new RelayCommand(SubmitFeedbackAction);
            this.IsInputEnabled = true;
        }

        public string FeedbackText
        { 
            get;
            set;
        }

        public bool IsInputEnabled
        {
            get;
            set;
        }

        public bool IsActivityIndicatorVisible
        {
            get;
            set;
        }

        public bool IsFeedbackPostingInProgress
        {
            get;
            set;
        }

        public string FeedbackEmailPlaceholder
        {
            get {return "Enter Email address (optional)";}
        }

        public string FeedbackEmail
        {
            get;
            set;
        }

        public RelayCommand SubmitFeedback
        {
            get { return this.SubmitFeedbackCommand; }
        }

        public async void SubmitFeedbackAction()
        {
            ToggleInputStatus();
            if(!string.IsNullOrEmpty(FeedbackText))
            {
                var feedBackRequest = new FeedbackPostRequest()
                {
                    Feedback = FeedbackText,
                    FeedbackEmail = FeedbackEmail
                };

                await this.feedbackService.PostFeedbackAsync(feedBackRequest);
            }

            ToggleInputStatus();                
            await this.NavigationService.PopModalAsync(); 
        }

        public override void OnAppearing(IDictionary<string, object> args)
        {
            this.FeedbackText = null;
            RaisePropertyChanged("FeedbackText");
        }

        private void ToggleInputStatus()
        {
            this.IsInputEnabled = !this.IsInputEnabled;
            this.IsFeedbackPostingInProgress = !this.IsFeedbackPostingInProgress;
            this.IsActivityIndicatorVisible = this.IsFeedbackPostingInProgress;

            RaisePropertyChanged("IsInputEnabled");
            RaisePropertyChanged("IsActivityIndicatorVisible");
            RaisePropertyChanged("IsFeedbackPostingInProgress");
        }
    }
}