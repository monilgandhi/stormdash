﻿using System;
using RouteWeather.BusinessLogic.DataOrganizer;
using WeatherApp.Communication;
using System.Collections.Generic;
using RouteWeather.Services;
using System.Threading.Tasks;
using RouteWeather.CommonLib;
using RouteWeather.Model;
using RouteWeather.Pcl.Navigation;
using RouteWeather.Services.DataStore;
using System.IO;
using RouteWeather.Pcl.EventArguments;
using RouteWeather.Services.Log;
using Xamarin.Forms;

namespace RouteWeather.Pcl.ViewModel
{
    public class WeatherConditionViewModel : CustomViewModelBase
    {
        private IWeatherDataOrganizer dataOrganizer;
        private IWeatherService weatherService;
        private RouteInformation route;
        private LocationDO source;
        private LocationDO destination;
        private List<SimpleWeatherConditionViewModel> summaryWeatherCondtionList;
        private bool isWeatherFetchInProgress;
        private string summaryPageTitle;
        private IKeyValueDataStore keyValueDataStore;
        #region Viewmodel Binding

        public List<SimpleWeatherConditionViewModel> SummaryWeatherCondtionList
        {
            get
            {
                return this.summaryWeatherCondtionList;
            }
        }

        public string SummaryPageTitle 
        { 
            get { return this.summaryPageTitle; }
            private set
            {
                this.summaryPageTitle = value;
                RaisePropertyChanged ("SummaryPageTitle");
            }
        }
            
        public bool IsWeatherFetchInProgress 
        {
            get { return this.isWeatherFetchInProgress; }
            private set 
            {
                this.isWeatherFetchInProgress = value;
                RaisePropertyChanged ("IsWeatherFetchInProgress");
            }
        }

        #endregion

        #region Constructor
        public WeatherConditionViewModel(IWeatherDataOrganizer dataOrganizer, IWeatherService weatherService, IPageNavigationService pageNavigation, IKeyValueDataStore keyvalueDataStore)
            : base(pageNavigation)
        {
            this.weatherService = weatherService;
            this.dataOrganizer = dataOrganizer;
            this.keyValueDataStore = keyvalueDataStore;
        }

        #endregion

        #region InterfaceMethods
        private void ResetPage()
        {
            this.summaryWeatherCondtionList = null;
            RaisePropertyChanged("SummaryWeatherCondtionList");
        }

        private async Task ShowRateAppDialog ()
        {
            AppUsage appUsage = await this.keyValueDataStore.GetAppUsageData();

            if (appUsage.IsAppRated) 
            {
                return;
            }

            ++appUsage.UseSinceLastTimeFeedbackPopup;

            if (appUsage.UseSinceLastTimeFeedbackPopup >= AppUsage.InitialiNumberOfUseToTriggerFeedback)
            {
                // reset everything
                appUsage.UseSinceLastTimeFeedbackPopup = 0;
                this.OnShowAppRatingDialog (new ShowAppRatingDialogEventArgs (true));
            }
            else
            {
                this.OnShowAppRatingDialog (new ShowAppRatingDialogEventArgs (false));
            }

            appUsage.LastUseDateTimeStampUtc = Utils.ConvertDateTimetoUnixUTC (DateTime.Now);
            await this.keyValueDataStore.SetAppUsageData (appUsage);
        }

        public async override Task  OnAppearingAsync (IDictionary<string, object> args)
        {
            await ShowRateAppDialog ();
            var newRoute = NavigationCache.Instance.GetObject(Constants.RouteCacheParam) as RouteInformation;
            var newSource = NavigationCache.Instance.GetObject(Constants.SourceCacheParam) as RouteWeather.Model.LocationDO;
            var newDestination = NavigationCache.Instance.GetObject(Constants.DestinationCacheParam) as RouteWeather.Model.LocationDO;

            var selectedTime = NavigationCache.Instance.GetObject(Constants.SelectedDateCacheParam) as DateTime?;

            if (newRoute == null ||
                newSource == null || 
                newDestination == null || 
                selectedTime == null)
            {
                throw new NullReferenceException();    
            }

            var selectedTimeUtc = Utils.ConvertDateTimetoUnixUTC (selectedTime.Value);
            if(IsWeatherFetchRequired(newRoute, newSource, newDestination, selectedTimeUtc))
            {
                this.IsWeatherFetchInProgress = true;
                ResetPage();
                this.route = newRoute;
                this.source = newSource;
                this.destination = newDestination;

                this.SummaryPageTitle = string.Format ("From {0} to {1}", this.source.City, this.destination.City);

                this.route.DateUTC = Utils.ConvertDateTimetoUnixUTC(selectedTime.Value);
                this.dataOrganizer.Reset ();
                this.SummaryPageTitle = this.source.City + " to " + this.destination.City;

                List<LocationInformation> weatherInformation = null;
                try
                {
                    weatherInformation = await  this.weatherService.CalculateWeatherForRoute(this.route);
                    this.dataOrganizer.OrganizeWeatherConditions(weatherInformation, this.source, this.destination);
                    this.SetSummaryData();
                }
                catch (Exception ex)
                {
                    await ClientLogger.Error(this.GetType().ToString(), ex, Device.OS.ToString());
                    OnErrorOccurred("Unble to get weather. Try again later", null);
                }
            }
        }
        #endregion

        private bool IsWeatherFetchRequired(RouteInformation routeInfo, RouteWeather.Model.LocationDO newSource, RouteWeather.Model.LocationDO newDestination, ulong routeTravelTime)
        {
            if (this.route == null || this.source == null || this.destination == null || this.route.DateUTC == 0) 
            {
                return true;
            }

            if (this.route.RoutePoints != routeInfo.RoutePoints || !this.source.Equals(newSource) || !this.destination.Equals(newDestination) || this.route.DateUTC != routeTravelTime) 
            {
                return true;
            }

            return false;
        }

        private void SetSummaryData()
        {
            if (this.dataOrganizer.BadWeatherDictionary == null)
            {
                return;
            }

            if (this.summaryWeatherCondtionList == null) 
            {
                this.summaryWeatherCondtionList = new List<SimpleWeatherConditionViewModel> ();
            }

            if (this.summaryWeatherCondtionList.Count == 0) 
            {
                foreach (var badWeatherModelKeyValuePair in this.dataOrganizer.BadWeatherDictionary) 
                {
                    if (badWeatherModelKeyValuePair.Value == null) 
                    {
                        // TODO log
                    }

                    this.summaryWeatherCondtionList.Add(new SimpleWeatherConditionViewModel(badWeatherModelKeyValuePair.Value.BadWeatherCondition, 
                        badWeatherModelKeyValuePair.Value.BadWeatherIntensity, 
                        badWeatherModelKeyValuePair.Value.Locations, 
                        this.source.City));
                }
            }

            this.IsWeatherFetchInProgress = false;
            this.RaisePropertyChanged("SummaryWeatherCondtionList");
        }

        public override async Task MarkAppRated ()
        {
            await this.keyValueDataStore.MarkAppRated ();
        }
    }
}