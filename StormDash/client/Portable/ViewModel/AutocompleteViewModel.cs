﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RouteWeather.Services;
using RouteWeather.Services.DataStore;
using RouteWeather.Model;
using RouteWeather.CommonLib;
using RouteWeather.Pcl.Navigation;

namespace RouteWeather.Pcl.ViewModel
{
    public class AutocompleteViewModel : CustomViewModelBase
    {
        private List<LocationDO> autocompleteLocations;
        private List<DualLineText> viewAutocompleteLocations;
        private GooglePlacesClient googlePlacesClient;
        private ILocationDataStoreService locationDataStoreService;

        public AutocompleteViewModel(
            IPageNavigationService pageNavigation, 
            ILocationDataStoreService locationDataStore)
            : base(pageNavigation)
        {
            this.autocompleteLocations = new List<LocationDO> ();
            this.googlePlacesClient = new GooglePlacesClient();
            this.ViewAutocompleteLocations = new List<DualLineText> ();
            this.locationDataStoreService = locationDataStore;
        }

        #region Viewmodel Bindings

        public string SearchTerm 
        {
            get;
            set;
        }

        public bool IsAutoCompleteInProgress
        {
            get;
            set;
        }

        public List<DualLineText> ViewAutocompleteLocations
        {
            get { return this.viewAutocompleteLocations; }
            set 
            {
                this.viewAutocompleteLocations = value; 
                RaisePropertyChanged ();
            }
        }

        #endregion
        public override async Task OnAppearingAsync(IDictionary<string,object> args)
        {
            ResetAutoCompleteLocation ();
        }

        public async Task LocationSelected(DualLineText selectedItem)
        {
            if (selectedItem == null) 
            {
                return;
            }

            var selectedLocation = this.autocompleteLocations [selectedItem.Index];
            LocationDO savedLocation = await this.locationDataStoreService.SaveAndGetSelectedLocation (selectedLocation);
            NavigationCache.Instance.AddObject (Constants.AutocompleteSelectedItemCacheParam, savedLocation);
            await this.NavigationService.PopAsync ();
        }

        private void ToggleActivityIndicator()
        {
            this.IsAutoCompleteInProgress = !this.IsAutoCompleteInProgress;
            RaisePropertyChanged("IsAutoCompleteInProgress");
        }

        private async Task<List<LocationDO>> GooglePlacesAutoComplete(string searchTerm)
        {
            return await this.googlePlacesClient.AutoComplete(searchTerm);
        }

        public async void AutoCompleteSearchTerm(object obj, EventArgs args)
        {
            if(String.IsNullOrWhiteSpace(this.SearchTerm))
            {
                // add the option of current location
                ResetAutoCompleteLocation();
                return;
            }

            this.ToggleActivityIndicator();

            string searchTerm = this.SearchTerm;

            bool searchUsingCity = false;
            this.autocompleteLocations = await this.GooglePlacesAutoComplete(searchTerm);
            //this.autocompleteLocations = await Task.Run(() => this.locationDatastoreService.GetCitiesAutoComplete(searchTerm, 5));
            searchUsingCity = true;

            var viewRepresentation = new List<DualLineText> ();
            int index = 0;

            this.ToggleActivityIndicator();

            foreach (var location in this.autocompleteLocations) 
            {
                string supportingText;
                if (searchUsingCity) 
                {
                    supportingText = location.State; 
                }
                else 
                {
                    supportingText = location.State + " ," + location.Zipcode;
                }

                var viewLocation = new DualLineText (location.City, supportingText, index++);
                viewRepresentation.Add (viewLocation);
            }

            ViewAutocompleteLocations = viewRepresentation;
        }

        private void ResetAutoCompleteLocation()
        {
            // TODO For now removing current location. If the user is in different country it will cause problems
            // Also this returns state as "Washington" instead of "WA"
            this.autocompleteLocations = new List<RouteWeather.Model.LocationDO> ();
            ViewAutocompleteLocations = new List<DualLineText> ();
            return;
        }
    }
}