﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight;
using Xamarin.Forms;
using System.Threading.Tasks;
using RouteWeather.Pcl.Common;
using RouteWeather.Pcl.Navigation;
using RouteWeather.Pcl.EventArguments;

namespace RouteWeather.Pcl.ViewModel
{
    public abstract class CustomViewModelBase : ViewModelBase
    {
        protected Color buttonBorderColor;
        protected IPageNavigationService NavigationService { get; private set; }

        public delegate void ViewModelErrorEvent(string message, EventArgs e);
        public event ViewModelErrorEvent ErrorRaised;

        public delegate void ShowAppRatingDialogEvent (object sender, ShowAppRatingDialogEventArgs e);
        public event ShowAppRatingDialogEvent ShowRatingDialog;

        public CustomViewModelBase()
        {
        }

        protected virtual void OnShowAppRatingDialog (ShowAppRatingDialogEventArgs args)
        {
            if (this.ShowRatingDialog != null) 
            {
                this.ShowRatingDialog (this, args);
            }   
        }

        protected virtual void OnErrorOccurred(string message, EventArgs args)
        {
            this.ErrorRaised?.Invoke(message, args);
        }

        public CustomViewModelBase (IPageNavigationService pageNavigation)
        {
            this.NavigationService = pageNavigation;
        }

        public virtual void OnAppearing(IDictionary<string, object> args)
        {
            throw new NotImplementedException ("Child Viewmodel classes need to implement this");
        }

        public virtual void OnDisappearing(IDictionary<string, object> args)
        {
            throw new NotImplementedException ("Child Viewmodel classes need to implement this");
        }

        public virtual Task OnAppearingAsync(IDictionary<string, object> args)
        {
            throw new NotImplementedException ("Child Viewmodel classes need to implement this");
        }

        public Color ButtonBorderColor
        {
            get { return buttonBorderColor; }
        }

        public void ChangeButtonBorderColor(bool isActive)
        {
            AssetManager assetManager;
            if (!AssetManager.TryGetResourceDictionaryInstance (out assetManager)) 
            {
                System.Diagnostics.Debugger.Break ();
            }
            this.buttonBorderColor = assetManager.GetButtonBorderColor(isActive);

            RaisePropertyChanged("ButtonBorderColor");
        }

        public virtual async Task MarkAppRated ()
        {
            throw new NotImplementedException ("Child viewmodel must implement this class");
        }
    }
}