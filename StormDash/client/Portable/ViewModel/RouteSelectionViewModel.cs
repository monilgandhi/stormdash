﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Command;
using RouteWeather.Services;
using System.Threading.Tasks;
using RouteWeather.CommonLib;
using Communication = WeatherApp.Communication;
using System.Linq;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.Model;
using WeatherApp.Communication;
using RouteWeather.Pcl.Navigation;
using RouteWeather.Pcl.Views;
using RouteWeather.Services.Log;
using Xamarin.Forms;

namespace RouteWeather.Pcl.ViewModel
{
    public class RouteSelectionViewModel : CustomViewModelBase
    {
        private LocationDO source;
        private LocationDO destination;
        private List<RouteOption> routeOptions;
        private RelayCommand GetWeatherCommand;
        private DateTime selectedDate;
        private RouteInformation selectedRoute;
        private string sliderTimeValue;
        private int sliderCurrentValue;
        private bool isSliderEnabled;
        private bool isRouteBeingFetched;

        public RouteSelectionViewModel (IPageNavigationService pageNavigation) 
            : base(pageNavigation)
        {
            GetWeatherCommand = new RelayCommand (FetchWeather, CanGetWeather);
            this.routeOptions = new List<RouteOption> ();

            // set event handlers
            this.PropertyChanged += OnDateSelectionChanged;
            this.PropertyChanged += OnSliderValueChanged;

            if (DateTime.Now.Hour == 23) 
            {
                this.SelectedDate = DateTime.Today.AddDays (1);
            }
            else
            {
                this.SelectedDate = DateTime.Today;
            }

        }

        #region Viewmodel Bindings
        public string MinimumDate
        {
            get
            {
                return DateTime.Today.Date.ToString ();
            }
        }

        public string MaximumDate
        {
            get
            {
                return DateTime.Now.AddDays(4).Date.ToString();
            }
        }

        public List<RouteOption> AllRouteOptions
        {
            get
            {
                return this.routeOptions;
            }
            set
            {
                this.routeOptions = value;
                this.RaisePropertyChanged ("AllRouteOptions");
            }    
        }

        public string SelectedDateLabel
        {
            get
            {
                return this.selectedDate.ToString("MMMM d"); 
            }   
        }

        public DateTime SelectedDate
        {
            get
            {
                return this.selectedDate;
            }

            set
            {
                this.selectedDate = value;
                RaisePropertyChanged ("SelectedDate");
                RaisePropertyChanged ("SelectedDateLabel");
            }
        }

        public int SliderMinimumValue 
        {
            get;
            private set;
        }

        public int SliderMaximumValue 
        {
            get
            {
                return 23;
            }
        }

        public bool IsRouteBeingFetched 
        {
            get { return this.isRouteBeingFetched; }
            set 
            {
                this.isRouteBeingFetched = value;
                RaisePropertyChanged ("IsRouteBeingFetched");
            }
        }

        public string SliderTimeValue 
        {
            get
            {
                return this.sliderTimeValue;
            }
            set
            { 
                this.sliderTimeValue = value;
                RaisePropertyChanged ("SliderTimeValue");
            }
        }

        public int SliderValue
        {
            get
            {
                return this.sliderCurrentValue;
            }

            set
            {
                this.sliderCurrentValue = value;
                RaisePropertyChanged ("Slidervalue");
            }
        }

        public bool IsSliderEnabled
        {
            get { return isSliderEnabled; }
            set 
            {
                this.isSliderEnabled = value;
                RaisePropertyChanged ("IsSliderEnabled");
            }
        }

        public RelayCommand GetWeather
        {
            get { return GetWeatherCommand; }
        }
        #endregion

        public async override Task OnAppearingAsync (IDictionary<string, object> args)
        {
            var newSource = NavigationCache.Instance.GetObject (Constants.SourceCacheParam) as RouteWeather.Model.LocationDO;
            var newDestination = NavigationCache.Instance.GetObject (Constants.DestinationCacheParam) as RouteWeather.Model.LocationDO;

            if (this.AllRouteOptions == null || this.AllRouteOptions.Count == 0 || HasSourceDestinationChanged(newSource, newDestination)) 
            {
                this.selectedRoute = null;
                GetWeatherCommand.RaiseCanExecuteChanged ();

                this.IsRouteBeingFetched = true;
                this.AllRouteOptions = null;
                this.source = newSource;
                this.destination = newDestination;
                
                var googleMapsApiClient = new GoogleMapsDirectionApiClient ();
                try
                {
                    List<RouteInformation> routes = await Task.Run(() => googleMapsApiClient.GetDirectionsAsync(this.source, this.destination));
                    if (routes.Count == 0)
                    {
                        // print error
                        await ClientLogger.Error(this.GetType().ToString(), string.Format("No routes found from {0} to {1}", this.source, this.destination.City), Device.OS.ToString());
                    }

                    var routeOptions = new List<RouteOption>();
                    for (int i = 0; i < routes.Count; i++)
                    {
                        routeOptions.Add(new RouteOption(routes[i], i, this.NavigationService));
                    }

                    this.AllRouteOptions = routeOptions;

                    var firstRoute = routes.First();

                    if (firstRoute == null || this.source == null || this.destination == null)
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {
                    await ClientLogger.Error(this.GetType().ToString(), ex, Device.OS.ToString());
                    this.OnErrorOccurred("Unable to get the route. Try again", null);
                }

                this.IsRouteBeingFetched = false;
            }
        }

        public void OnDateSelectionChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "SelectedDate") 
            {
                return;
            }

            this.SliderMinimumValue = Utils.GetDateSliderMinimumValue (this.SelectedDate);

            this.SliderValue = this.SliderMinimumValue;

            if (this.SliderMinimumValue > 22)
            {
                this.SliderMinimumValue = 0;
                this.SliderValue = 23;
                this.IsSliderEnabled = false;
            }
            else
            {
                this.IsSliderEnabled = true;
            }

            RaisePropertyChanged ("IsSliderEnabled");
            RaisePropertyChanged ("SliderMinimumValue");
            RaisePropertyChanged ("SliderValue");
        }

        public void OnSliderValueChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Slidervalue") 
            {
                return;
            }

            this.SliderTimeValue = Utils.ConvertTimeTo12Hour (this.SliderValue);
        }

        public void OnRouteSelected(RouteOption selectedRoute)
        {
            if (selectedRoute == null) 
            {
                return;
            }

            selectedRoute.IsSelected = true;
            this.selectedRoute = selectedRoute.RouteDetails;
            GetWeatherCommand.RaiseCanExecuteChanged ();
            // set every other route to false
            foreach (var route in AllRouteOptions) 
            {
                if (route.Index != selectedRoute.Index) 
                {
                    route.IsSelected = false;
                }
            }
        }

        public async void FetchWeather()
        {
            // store the selected route and date
            NavigationCache.Instance.AddObject(Constants.RouteCacheParam, this.selectedRoute);

            // create actual date time including the time
            DateTime actualRequiredDate = this.selectedDate.AddHours(this.sliderCurrentValue);
            NavigationCache.Instance.AddObject (Constants.SelectedDateCacheParam, actualRequiredDate);
            await this.NavigationService.PushAsync (new SimpleWeatherConditionView ());
        }

        public bool CanGetWeather()
        {
            return this.selectedRoute != null;
        }

        private void CalibrateSourceAndDestinationLatLng(string routePoints)
        {
            var routePointList = Utils.DecodeGooglePolyLines(routePoints);
            // TODO do a check for lat and lon here
            if (routePointList.First () != null) 
            {
                this.source.Latitude = routePointList.First ().Latitude;
                this.source.Longitude = routePointList.First ().Longitude;

                // save the updated source
                NavigationCache.Instance.AddObject (Constants.SourceCacheParam, this.source);
            }

            if (routePointList.Last() != null) 
            {
                this.destination.Latitude = routePointList.Last ().Latitude;
                this.destination.Longitude = routePointList.Last().Longitude;

                // save the updated source
                NavigationCache.Instance.AddObject (Constants.DestinationCacheParam, this.destination);
            }
        }

        private bool HasSourceDestinationChanged(RouteWeather.Model.LocationDO newSource, RouteWeather.Model.LocationDO newDestination)
        {
            if (this.source == null || this.destination == null) 
            {
                return true;
            }

            return !this.source.Equals (newSource) || !this.destination.Equals (newDestination);
        }
    }
}