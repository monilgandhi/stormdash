﻿using System;

namespace RouteWeather.Pcl.ViewModel
{
    public class DualLineText
    {
        public string Title { get; private set; }
        public string Detail { get; private set; }
        public int Index { get; private set; }

        public DualLineText (string title, string detail, int index)
        {
            this.Title = title;
            this.Detail = detail;
            this.Index = index;
        }
    }
}