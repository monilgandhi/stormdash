﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RouteWeather.Pcl.Navigation
{ 
    public class NavigationService : INavigation
    {
        public INavigation Navi { get; private set;}

        public NavigationService(INavigation navi)
        {
            this.Navi = navi;
        }

        public async Task<Page> PopAsync()
        {
            return await Navi.PopAsync();
        }

        public async Task<Page> PopAsync(Boolean animated)
        {
            return await Navi.PopAsync(animated);
        }

        public async Task<Page> PopModalAsync()
        {
            return await Navi.PopModalAsync();
        }

        public async Task<Page> PopModalAsync(Boolean animated)
        {
            return await Navi.PopModalAsync(animated);
        }

        public Task PopToRootAsync()
        {
            return Navi.PopToRootAsync();
        }

        public Task PopToRootAsync(Boolean animated)
        {
            return Navi.PopToRootAsync(animated);
        }

        public Task PushAsync(Page page)
        {
            return Navi.PushAsync(page);
        }

        public Task PushAsync(Page page, Boolean animated)
        {
            return Navi.PushAsync(page, animated);
        }

        public Task PushModalAsync(Page page)
        {
            return Navi.PushModalAsync(page);
        }

        public Task PushModalAsync(Page page, Boolean animated)
        {
            return Navi.PushModalAsync(page, animated);
        }

        public void RemovePage(Page page)
        {
            Navi.RemovePage (page);
        }

        public void InsertPageBefore(Page page, Page before)
        {
            Navi.InsertPageBefore (page, before);
        }

        public IReadOnlyList<Page> NavigationStack
        {
            get { return Navi.NavigationStack; }
        }

        public IReadOnlyList<Page> ModalStack
        {
            get { return Navi.ModalStack; }
        }
    }
}

