﻿using System;
using System.Threading.Tasks;
using RouteWeather.Model;

namespace RouteWeather.Pcl.Location
{
    public interface IClientLocationService
    {
        Task<LocationDO> GetCurrentLocation();
    }
}

