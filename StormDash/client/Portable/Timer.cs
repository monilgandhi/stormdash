﻿/**
 Unfortunately there is not timer class in .net portable dlls. http://stackoverflow.com/questions/12555049/timer-in-portable-library
 https://forums.xamarin.com/discussion/comment/55199/#Comment_55199

There is a bug in this timer code. 
For eg, set the duetime to 5000 and set period to 25000. Then after 15000 call the dispose method. This function will keep throwing TaskCanceledException
**/
using System;
using System.Threading;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Diagnostics;

namespace RouteWeather.Pcl
{
    internal delegate void TimerCallback(object state);
    internal sealed class Timer : CancellationTokenSource, IDisposable
    {
        private int totalPeriodElapsed = 0;
        internal Timer(TimerCallback callback, object state, int dueTime, int period)
        {
            Task.Delay(dueTime, Token).ContinueWith(async (t, s) =>
                {
                    var tuple = (Tuple<TimerCallback, object>)s;
                    totalPeriodElapsed += dueTime;
                    while (!IsCancellationRequested && totalPeriodElapsed <= period)
                    {
                        Task.Run(() => tuple.Item1(tuple.Item2));
                        try
                        {
                            await Task.Delay(dueTime, Token).ConfigureAwait(false);
                        }
                        catch (TaskCanceledException)
                        {
                            Debug.WriteLine("Task was cancelled and there was race condtion");
                        }
                    }

                }, Tuple.Create(callback, state), CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.Default);
        }

        public new void Dispose() 
        { 
            base.Cancel(); 
            this.Dispose();
        }
    }
}

