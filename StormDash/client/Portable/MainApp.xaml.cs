﻿using RouteWeather.CommonLib;
using RouteWeather.Pcl.Views;
using RouteWeather.Pcl.Locator;
using Xamarin.Forms;
using RouteWeather.Pcl.Common;
using RouteWeather.Pcl.Navigation;

namespace RouteWeather.Pcl
{
    public abstract partial class MainApp : Application
    {
        public MainApp()
        {
            this.InitializeComponent();

            // setup resources
            this.SetupResources();
            this.RegisterPlatformSpecificServices();

            // register the navigation service
            var pageNavigation = new PageNavigationService();
            CServiceLocator.RegisterNavigationInstance(pageNavigation);

            // create the main page
            this.MainPage = new NavigationPage(new RouteInfoPage())
            {
                BarBackgroundColor = Color.Black
            };

            // initialize the navinstance with INavigation
            pageNavigation.Initialize(this.MainPage.Navigation);
        }

        protected abstract void RegisterPlatformSpecificServices();
        protected virtual void SetupResources()
        {
            AssetManager assetMgr;
            if (AssetManager.TryGetResourceDictionaryInstance(out assetMgr))
            {
                if (assetMgr == null)
                {
                    return;
                }

                assetMgr.ColorTheme = Theme.Light;
            }
        }
    }
}

