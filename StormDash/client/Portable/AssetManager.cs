﻿using System.Text;
using System;
using System.Diagnostics;
using CommunicationSchemas.Schemas.Protobuf;
using Xamarin.Forms;
using RouteWeather.CommonLib;

namespace RouteWeather.Pcl.Common
{
    public class AssetManager
    {
        #region Private Members
        private const string UncheckedGlyphIcon = "uncheck";
        private const string CheckedGlyphIcon = "check";
        private const string RouteIcon = "route";
        private const string SpeedIcon = "speed";
        private const string MapIcon = "map";
        private const string WeatherAlertIcon = "alert";
        private const string DestinationIcon = "location";
        #endregion
        protected const string PngExt = ".png";

        protected string otherIconBasePath = @"other";
        protected string commonIconBasePath = @"common";
        protected string weatherIconBasePath = @"weathercondition";
        protected string pathSeparator = @"_";

        #region Private Functions
        protected virtual string GetOtherIconPath(string iconName)
        {
            StringBuilder iconsFullPath = new StringBuilder(otherIconBasePath);
            iconsFullPath.Append(pathSeparator + ColorTheme);
            iconsFullPath.Append(pathSeparator + iconName + PngExt);

            return iconsFullPath.ToString().ToLower();
        }

        protected virtual string GetCommonIconPath(string iconName)
        {
            var iconsFullPath = new StringBuilder(commonIconBasePath);
            iconsFullPath.Append(pathSeparator + iconName + PngExt);

            return iconsFullPath.ToString();
        }
        #endregion

        #region Public Members
        public const string ErrorAlertTitle = "Error occurred";
        public const string ErrorAlertCancelButton = "Start Over";

        public static bool TryGetResourceDictionaryInstance(out AssetManager assetMgr)
        {
            assetMgr = null;
            object assetMgrObj;

            if (!Application.Current.Resources.TryGetValue("AssetMgr", out assetMgrObj))
            {
                return false;
            }

            assetMgr = assetMgrObj as AssetManager;

            return assetMgr != null;
        }

        public double XamarinLabelLargeFontSize
        {
            get { return Device.GetNamedSize(NamedSize.Medium, typeof(Label)); }
        }

        public double XamaringLabelMediumFontSize
        {
            get { return Device.GetNamedSize(NamedSize.Small, typeof(Label)); }
        }

        public double XamarinLabelTitleFontSize
        {
            get { return Device.GetNamedSize(NamedSize.Large, typeof(Label)); }
        }

        public FontAttributes XamarinTitleFontAttributesLabel
        {
            get { return FontAttributes.Bold; }
        }

        public Color XamarinAmbientColor
        {
            get
            {
                return Color.FromHex(AmbientColor);
            }
        }

        public Color XamarinPlaceholderTextColor
        {
            get
            {
                return Color.FromHex(PlaceHolderTextColor);
            }
        }

        public Color XamarinSubtitleFontColor
        {
            get
            {
                if (ColorTheme == Theme.Dark)
                {
                    return Color.White;
                }

                return Color.Black;
            }
        }

        public Color BackgroundColor
        {
            get
            {
                if (ColorTheme == Theme.Dark)
                {
                    return Color.Black;
                }

                return Color.White;
            }
        }

        public Color GetButtonBorderColor(bool isActive)
        {
            if (isActive)
            {
                return XamarinAmbientColor;
            }
            return XamarinPlaceholderTextColor;
        }

        public Theme ColorTheme
        {
            get;
            set;
        }
        #endregion

        #region View Model Properties
        public string AmbientColor
        {
            get
            {
                if (ColorTheme == Theme.Dark)
                {
                    return "#3399cc";
                }
                return "#F1A51E";
            }
        }

        public string PlaceHolderTextColor
        {
            get
            {
                if (ColorTheme == Theme.Dark)
                {
                    return "#3a3a3a";
                }
                return "#8c8c8c";
            }
        }

        public string AmbientForegroundFontColor
        {
            get
            {
                if (ColorTheme == Theme.Dark)
                {
                    return "#3a3a3a";
                }
                return "#8c8c8c";
            }            
        }

        public string DisabledButtonColor
        {
            get
            {
                return "#3a3a3a";
            }
        }

        public string ThemeForegroundFontColor
        {
            get
            {
                return "#8c8c8c";
            }
        }

        public string RouteImage
        {
            get
            {
                return GetOtherIconPath(RouteIcon);
            }
        }

        public string DestinationImage
        {
            get
            {
                return GetOtherIconPath(DestinationIcon);
            }
        }

        public string SpeedImage
        {
            get
            {
                return GetOtherIconPath(SpeedIcon);
            }
        }

        public string WeatherTypeImage
        {
            get
            {
                return GetOtherIconPath(WeatherAlertIcon);
            }
        }

        public string CheckedGlyph
        {
            get { return GetOtherIconPath(CheckedGlyphIcon); }
        }

        public string UncheckedGlyph
        {
            get { return GetOtherIconPath(UncheckedGlyphIcon); }
        }

        public string MapButtonIcon
        {
            get { return GetCommonIconPath(MapIcon); }
        }

        public string GetWeatherConditionIconPath(WeatherType condition, ulong arrivalTime, UInt16? cloudCoverPercentage)
        {
            var iconsFullPath = new StringBuilder(this.weatherIconBasePath + this.pathSeparator);
            iconsFullPath.Append(this.ColorTheme.ToString());

            var utcDateTime =
                Utils.ConvertUnixToDateTime(arrivalTime);

            if (Utils.DetermineIfDay(utcDateTime.ToLocalTime()))
            {
                iconsFullPath.Append(this.pathSeparator + "Day");
            }
            else
            {
                iconsFullPath.Append(this.pathSeparator + "Night");
            }
                
            // cloud or not
            if (cloudCoverPercentage != null && cloudCoverPercentage > 20) 
            {
                iconsFullPath.Append (this.pathSeparator + "cloudy");
            }
            else 
            {
                iconsFullPath.Append (this.pathSeparator + "sunny");
            }

            iconsFullPath.Append(this.pathSeparator + condition + ".png");
            Debug.WriteLine ("Icon path: " + iconsFullPath.ToString());
            return iconsFullPath.ToString().ToLower();
        }
        #endregion
    }
}
