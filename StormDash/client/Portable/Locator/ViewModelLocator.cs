﻿using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Ioc;
using RouteWeather.Pcl.ViewModel;

namespace RouteWeather.Pcl.Locator
{
    public class ViewModelLocator
    {
        private static ViewModelLocator locator;
        public static ViewModelLocator Locator
        {
            get
            {
                if (locator == null) 
                {
                    locator = new ViewModelLocator();
                }
                return locator;
            }
        }

        public ViewModelLocator ()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            #region ViewModel
            SimpleIoc.Default.Register<LoadingScreenViewModel>();
            SimpleIoc.Default.Register<RouteInfoViewModel>();
            SimpleIoc.Default.Register<AutocompleteViewModel>();
            SimpleIoc.Default.Register<RouteSelectionViewModel>();
            SimpleIoc.Default.Register<WeatherConditionViewModel>();
            SimpleIoc.Default.Register<FeedBackViewModel>();
            #endregion
        }

        public RouteInfoViewModel RouteInput
        {
            get
            {
                return ServiceLocator.Current.GetInstance<RouteInfoViewModel> ();
            }
        }

        public AutocompleteViewModel AutocompleteViewModel
        {
            get 
            {
                return ServiceLocator.Current.GetInstance<AutocompleteViewModel> ();
            }
        }

        public RouteSelectionViewModel RouteSelectionViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<RouteSelectionViewModel> ();
            }
        }

        public WeatherConditionViewModel WeatherConditionViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WeatherConditionViewModel> ();
            }
        }

        public LoadingScreenViewModel LoadingViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LoadingScreenViewModel>();
            }

        }

        public FeedBackViewModel FeedBackViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FeedBackViewModel>();
            }
                
        }
    }
}

