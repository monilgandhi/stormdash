﻿using System;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using RouteWeather.Services;
using RouteWeather.Services.DataStore;
using Xamarin.Forms;
using RouteWeather.Pcl.Navigation;
using RouteWeather.BusinessLogic.DataOrganizer;
using RouteWeather.Pcl.Location;

namespace RouteWeather.Pcl.Locator
{
    public static class CServiceLocator
    {
        static CServiceLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            #region Services
            Register<ILocationDataStoreService,LocationDataStore>();
            Register<IKeyValueDataStore,KeyValueDataStore>();
            Register<IWeatherService, CustomWeatherService>();
            Register<IWeatherDataOrganizer, WeatherDataOrganizer>();
            Register<IFeedbackService, FeedbackService>();
            Register<IRecentLocationDataStoreService, RecentRouteDataStore> ();
            #endregion
        }

        public static void Register<TInterface,TClass>()
            where TInterface : class
            where TClass : class
        {
            SimpleIoc.Default.Register<TInterface,TClass> ();
        }

        public static void RegisterNavigationInstance(IPageNavigationService instance)
        
        {
            if (instance == null) 
            {
                throw new ArgumentNullException ("instance");
            }

            SimpleIoc.Default.Register<IPageNavigationService>(() => instance);
        }

        public static void RegisterLocationServiceInstance(IClientLocationService instance)

        {
            if (instance == null) 
            {
                throw new ArgumentNullException ("instance");
            }
            SimpleIoc.Default.Register<IClientLocationService>(() => instance);
        }
    }
}