﻿using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;

namespace RouteWeather.Pcl.Common
{
    public static class RouteWeatherUtility
    {
        public static string ReadResourcesFileContentsAsync(string fileName)
        {
            var assembly = typeof(RouteWeatherUtility).GetTypeInfo().Assembly;
            var resourcesList = typeof(RouteWeatherUtility).GetTypeInfo().Assembly.GetManifestResourceNames ();
            Stream stream = assembly.GetManifestResourceStream(fileName);
            string contents = null;
            using (var reader = new System.IO.StreamReader (stream)) {
                contents = reader.ReadToEnd ();
            }
            return contents;
        }
    }
}