﻿using System;
namespace RouteWeather.Pcl.EventArguments
{
    public class ShowAppRatingDialogEventArgs : EventArgs
    {
        public bool ShowDialog { get; private set;}

        public ShowAppRatingDialogEventArgs (bool showDialog)
        {
            this.ShowDialog = showDialog;
        }
    }
}

