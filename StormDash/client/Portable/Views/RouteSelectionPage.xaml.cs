﻿using System;
using RouteWeather.Pcl.Common;
using RouteWeather.Pcl.ViewModel;
using RouteWeather.Pcl.Locator;
using Xamarin.Forms;

namespace RouteWeather.Pcl.Views
{
    public partial class RouteSelectionPage : ContentPage
    {
        private RouteSelectionViewModel viewModel;

        public RouteSelectionPage ()
        {
            InitializeComponent();
            this.viewModel = ViewModelLocator.Locator.RouteSelectionViewModel;
            BindingContext = this.viewModel;

            var tappedGesture = new TapGestureRecognizer ();
            tappedGesture.Tapped += OnDatePickerFocused ;
            this.datePickerLabel.GestureRecognizers.Add (tappedGesture);
            // AddFeedbackButton();
            this.viewModel.ErrorRaised += this.OnErrorRaised;
        }

        protected async void OnErrorRaised(string message, EventArgs e)
        {
            await DisplayAlert(AssetManager.ErrorAlertTitle, message, AssetManager.ErrorAlertCancelButton);
            await this.Navigation.PushAsync(new RouteInfoPage());
        }

        protected async override void OnAppearing ()
        {
            await this.viewModel.OnAppearingAsync (null);
        }

        protected void OnDatePickerFocused(object sender, EventArgs e)
        {
            this.datePicker.Focus ();
        }
            
        private void RouteSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedItem = e.SelectedItem as RouteOption;
            if (selectedItem == null) 
            {
                System.Diagnostics.Debugger.Break ();
            }

            this.viewModel.OnRouteSelected (selectedItem);
        }

        private void AddFeedbackButton()
        {
            var feedBackButton = new ToolbarItem
                {
                    Text = "Feedback"
                };

            feedBackButton.Clicked += async (object sender, EventArgs e) => {
                await this.Navigation.PushModalAsync(new Feedback());
            };
            this.ToolbarItems.Add(feedBackButton);
        }
    }
}