﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using RouteWeather.Pcl.Locator;
using RouteWeather.Pcl.ViewModel;

namespace RouteWeather.Pcl.Views
{
    public partial class LoadingScreen : ContentPage
    {
        private LoadingScreenViewModel vm;
        public LoadingScreen()
        {
            InitializeComponent();
            this.vm = ViewModelLocator.Locator.LoadingViewModel;
            this.BindingContext = this.vm;
        }

        protected override async void OnAppearing()
        {
            await this.vm.OnAppearingAsync(null);
        }
    }
}

