﻿using System;
using System.Collections.Generic;
using RouteWeather.Pcl.Locator;
using RouteWeather.Pcl.ViewModel;
using Xamarin.Forms;

namespace RouteWeather.Pcl.Views
{
    public partial class AutocompletePage : ContentPage
    {
        private AutocompleteViewModel viewModel;
        public AutocompletePage ()
        {
            InitializeComponent ();
            this.viewModel = ViewModelLocator.Locator.AutocompleteViewModel;
            this.BindingContext = ViewModelLocator.Locator.AutocompleteViewModel;

            this.searchTerm.TextChanged += this.viewModel.AutoCompleteSearchTerm;
            this.autocompleteResults.ItemTapped += OnItemSelected;
        }

        protected override async void OnAppearing ()
        {
            this.searchTerm.Text = string.Empty;
            this.searchTerm.Focus ();
            await this.viewModel.OnAppearingAsync (null);
        }

        public async void OnItemSelected(object obj, EventArgs args)
        {
            var listview = obj as ListView;
            var selectedItem = listview.SelectedItem as DualLineText;
            await this.viewModel.LocationSelected (selectedItem);
        }
    }
}

