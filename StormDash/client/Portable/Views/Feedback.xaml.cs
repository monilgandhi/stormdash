﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RouteWeather.Pcl.ViewModel;
using RouteWeather.Pcl.Locator;

namespace RouteWeather.Pcl.Views
{
    public partial class Feedback : ContentPage
    {
        private FeedBackViewModel vm;

        public Feedback()
        {
            InitializeComponent();
            this.vm = ViewModelLocator.Locator.FeedBackViewModel;
            this.BindingContext = this.vm;
        }

        protected override void OnAppearing()
        {
            this.feedbackEmail.Focus();
            this.vm.OnAppearing(null);
        }

        public async void OnFeedBackClicked(object sender, EventArgs e)
        {
            await this.Navigation.PopModalAsync();
        }
    }
}