﻿using System;
using RouteWeather.Pcl.Common;
using Xamarin.Forms;
using RouteWeather.Pcl.ViewModel;
using RouteWeather.Pcl.Locator;

namespace RouteWeather.Pcl.Views
{
    public partial class WeatherConditionView : CarouselPage
    {
        private WeatherConditionViewModel viewModel;

        public WeatherConditionView ()
        {
            //InitializeComponent ();
            this.viewModel = ViewModelLocator.Locator.WeatherConditionViewModel;
            this.BindingContext = this.viewModel;
            this.viewModel.ErrorRaised += this.OnErrorRaised;
        }

        protected async void OnErrorRaised(string message, EventArgs e)
        {
            var buttonPressed = await DisplayAlert(AssetManager.ErrorAlertTitle, message, AssetManager.ErrorAlertCancelButton,string.Empty);
            await this.Navigation.PushAsync(new RouteInfoPage());
        }

        protected async override void OnAppearing ()
        {
            await this.viewModel.OnAppearingAsync (null);
        }

        private void AddFeedbackButton()
        {
            var feedBackButton = new ToolbarItem
                {
                    Text = "Feedback"
                };

            feedBackButton.Clicked += async (object sender, EventArgs e) => {
                await this.Navigation.PushModalAsync(new Feedback());
            };
            this.ToolbarItems.Add(feedBackButton);
        }
    }
}