﻿using System;

using Xamarin.Forms;
using WeatherApp.Communication;

namespace RouteWeather.Pcl.Views
{
    public class MapPage : ContentPage
    {
        public RouteInformation mapRoute; 
        public MapPage (RouteInformation mapRoute)
        {
            this.mapRoute = mapRoute;
        }

        public void Close()
        {
            this.Navigation.PopAsync();
        }
    }
}


