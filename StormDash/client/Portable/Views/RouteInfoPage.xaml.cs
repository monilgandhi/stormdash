﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RouteWeather.Pcl.Locator;
using RouteWeather.Pcl.ViewModel;
using Xamarin.Forms;

namespace RouteWeather.Pcl.Views
{
    public partial class RouteInfoPage : ContentPage
	{
        private RouteInfoViewModel viewModel; 
		public RouteInfoPage ()
		{
			InitializeComponent ();
            // initialize the viewmodel
            this.viewModel = ViewModelLocator.Locator.RouteInput;
            BindingContext = this.viewModel;
            this.sourceInput.Focused += AutocompleteSource;
            this.destinationInput.Focused += AutocompleteDestination;
            this.recentSelectedRoutes.ItemTapped += OnRecentRouteSelected;
		    // AddFeedbackButton();
		}

        public void OnRecentRouteSelected (object obj, EventArgs args)
        {
            var listview = obj as ListView;
            var selectedItem = listview.SelectedItem as DualLineText;
            this.viewModel.RecentLocationSelected (selectedItem.Index);
        }

        private async void AutocompleteSource(object sender, FocusEventArgs e)
        {
            await this.viewModel.Autocomplete(true);                
        }

        private async void AutocompleteDestination(object sender, FocusEventArgs e)
        {
            await this.viewModel.Autocomplete(false);                
        }
            
        protected override async void OnAppearing ()
        {
            this.recentSelectedRoutes.SelectedItem = null;
            if (this.viewModel != null) 
            {
                await this.viewModel.OnAppearingAsync(null);
            }
        }

        private void AddFeedbackButton()
        {
            var feedBackButton = new ToolbarItem
                {
                    Text = "Feedback"
                };

            feedBackButton.Clicked += async (object sender, EventArgs e) => {
                await this.Navigation.PushModalAsync(new Feedback());
            };
            this.ToolbarItems.Add(feedBackButton);
        }
	}
}