﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using RouteWeather.Pcl.Locator;
using RouteWeather.Pcl.ViewModel;
using System.ComponentModel;
using RouteWeather.Pcl.Common;
using RouteWeather.CommonLib;
using RouteWeather.Pcl.EventArguments;

namespace RouteWeather.Pcl.Views
{
    public partial class SimpleWeatherConditionView : ContentPage
    {
        private WeatherConditionViewModel viewModel;

        public SimpleWeatherConditionView ()
        {
            InitializeComponent ();
            this.viewModel = ViewModelLocator.Locator.WeatherConditionViewModel;
            this.BindingContext = this.viewModel;

            this.viewModel.ShowRatingDialog += OnShowRatingDialog;
            // AddFeedbackButton();
        }

        protected async void OnShowRatingDialog (object sender, ShowAppRatingDialogEventArgs args)
        {
            this.viewModel.ShowRatingDialog -= OnShowRatingDialog;

            if (args.ShowDialog) 
            {
                var isReadyToRate = await DisplayAlert ("Rate the App", "Your feedback is very valuable, please take a moment to rate the app", "Sure", "Not now");
                if (isReadyToRate) {
                    await this.viewModel.MarkAppRated ();
                    // mark that this user has rated now
                    var urlStore = Device.OnPlatform (Constants.AppStoreUrl, Constants.AndroidStoreUrl, Constants.WinStoreUrl);
                    Device.OpenUri (new Uri (urlStore));
                }
            }
        }

        protected async override void OnAppearing ()
        {
            await this.viewModel.OnAppearingAsync (null);
        }

        public void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event
            ((ListView)sender).SelectedItem = null; // de-select the row
        }

        private void AddFeedbackButton()
        {
            var feedBackButton = new ToolbarItem
                {
                    Text = "Feedback"
                };

            feedBackButton.Clicked += async (object sender, EventArgs e) => {
                await this.Navigation.PushModalAsync(new Feedback());
            };
            this.ToolbarItems.Add(feedBackButton);
        }
    }
}

