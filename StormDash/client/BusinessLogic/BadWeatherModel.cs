﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommunicationSchemas.Schemas.Protobuf;
using WeatherApp.Communication;

namespace RouteWeather.BusinessLogic.Model
{
    public class BadWeatherModel
    {
        public WeatherIntensity? BadWeatherIntensity
        {
            get;
            protected set;
        }

        public WeatherType BadWeatherCondition
        {
            get;
            protected set;
        }

        public String WeatherCondition
        {
            get { return BadWeatherCondition.ToString(); }
        }

        public List<LocationInformation> Locations { get; protected set; }
        public LocationInformation WeatherConditionFirstCity
        {
            get { return Locations.First(); }
        }

        public LocationInformation WeatherConditionLastCity
        {
            get { return Locations.Last(); }
        }

        public BadWeatherModel(WeatherType weatherType, WeatherIntensity? intensity)
        {
            this.BadWeatherCondition = weatherType;
            this.BadWeatherIntensity = intensity;
        }

        public void AddCity(LocationInformation location)
        {
            if (Locations == null)
            {
                Locations = new List<LocationInformation>();
            }
           Locations.Add(location);
        }

        public static string GetBadWeatherKey(WeatherIntensity intensity, WeatherType type)
        {
            if (intensity == WeatherIntensity.None) 
            {
                return type.ToString ();
            }

            return type.ToString() + '|' + intensity.ToString();
        }
    }
}
