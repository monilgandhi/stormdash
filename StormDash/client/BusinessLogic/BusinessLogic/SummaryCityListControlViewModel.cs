using System;
using System.Collections.Generic;

namespace RouteWeather.ViewModel
{
    public class SummaryCityListControlViewModel
    {
        private bool isLastItem;
        public List<string> CitiesName
        {
            get;
            private set;
        }

        public Double ControlHeight { get; private set; }

        public String ConditionIcon
        {
            get;
            private set;
        }

        public bool IsNotLastItem
        {
            get { return !this.isLastItem; }
        }

        internal void AddCityName(string cityName)
        {
            this.CitiesName.Add(cityName);
        }

        internal SummaryCityListControlViewModel(string conditionIcon,string cityName, bool isLastItem = false)
        {
            this.ConditionIcon = conditionIcon;
            this.CitiesName = new List<string>();
            this.CitiesName.Add(cityName);
            this.isLastItem = isLastItem;
        }
    }
}
