﻿using System;

namespace RouteWeather.BusinessLogic
{
    public struct WeatherElement<T>
    {
        private T value;
        private String name;

        public WeatherElement(String elementName)
        {
            this.value = default(T);
            this.name = elementName;
        }

        public T WeatherElementTrueValue
        {
            get { return this.value;}
            set { this.value = value; }
        }

        #region View Model Bindings
        public String ElementValue
        {
            get
            {
                if (value == null)
                {
                    return "-";
                }
                return this.value.ToString();
            }
        }

        public String ElementName
        {
            get { return this.name; }
        }
        #endregion

    }
}
