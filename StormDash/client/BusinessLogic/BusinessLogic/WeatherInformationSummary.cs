﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.BusinessLogic.Model;
using RouteWeather.ViewModel;
using RouteWeather.CommonLib;
using WeatherApp.Communication;

namespace RouteWeather.BusinessLogic.BusinessLogic
{
    public class WeatherInformationSummary
    {
        private WeatherType weatherType;
        private WeatherIntensity? weatherIntensity;
        private int totalNumberOfLocations;
        private double startDistanceFromSourceInMiles;
        private string sourceCity;
        private DateTime startTime;
        private UInt16? cloudCoverPercent;
        private ulong startTimeUtc;

        public WeatherInformationSummary(WeatherType type, WeatherIntensity? intensity, List<LocationInformation> locations, string sourceCity)
        {
            if (locations == null || locations.Count == 0) 
            {
                throw new ArgumentNullException ("location");
            }

            this.weatherType = type;
            this.weatherIntensity = intensity;
            this.totalNumberOfLocations = locations.Count;
            this.startDistanceFromSourceInMiles = Utils.GetDistanceInLocalCulture (locations.First().DistanceFromStartPoint);
            this.startTime = Utils.ConvertUnixToDateTime (locations.First().ApproximateArrivalTime);
            this.sourceCity = sourceCity;
            this.cloudCoverPercent = Utils.GetAvgCloudCoverPercent (locations);
            this.startTimeUtc = locations.First ().ApproximateArrivalTime;
        }

    }
}