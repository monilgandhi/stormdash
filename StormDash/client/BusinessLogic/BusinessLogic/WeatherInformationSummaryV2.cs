﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.BusinessLogic.Model;
using WeatherApp.Communication;
using RouteWeather.CommonLib;

namespace RouteWeather.BusinessLogic
{
    public class WeatherInformationSummaryV2
    {
        private readonly List<LocationInformation> locationInfo;
        private readonly List<BadWeatherModel> badWeatherModels;

        public WeatherInformationSummaryV2(List<BadWeatherModel> badWeatherModels, List<LocationInformation> locationInfo)
        {
            if (badWeatherModels == null)
            {
                throw new ArgumentNullException("badWeatherModels");
            }

            if (locationInfo == null)
            {
                throw new ArgumentNullException("locationInfo");
            }

            if (locationInfo.Count == 0)
            {
                throw new IndexOutOfRangeException("locationInfo cannot be empty");
            }

            this.locationInfo = locationInfo;
            this.badWeatherModels = badWeatherModels;
            PopulateSummaryData();
        }

        private void PopulateSummaryData()
        {
            SetMaxtemperaturOnRoute();
            SetMintemperaturOnRoute();
            SetAvgPrecipitationChances();
            SetAvgCloudCoverPercent();
            SetMaxSnowFallValue();
            SetBadWeatherSummary();
        }

        private void SetBadWeatherSummary()
        {
            var badWeatherSummaryList = new List<BadWeatherSummaryModel>();

            if (this.badWeatherModels.Count == 0)
            {
                //valid condition
                CreateClearWeather();
                return;
            }

            var cityLocationInfo = this.locationInfo.First ();
            string source = cityLocationInfo.City;

            foreach (var badWeather in this.badWeatherModels)
            {
                var badWeatherModel = new BadWeatherSummaryModel(
                    badWeather.BadWeatherCondition,
                    badWeather.WeatherConditionFirstCity.City, 
                    badWeather.WeatherConditionFirstCity.DistanceFromStartPoint,
                    badWeather.WeatherConditionLastCity.DistanceFromStartPoint,
                    source,
                    cityLocationInfo.ApproximateArrivalTime);
                badWeatherSummaryList.Add(badWeatherModel);
            }
            BadWeatherSummaryList = badWeatherSummaryList;
        }

        private void CreateClearWeather()
        {
            var badWeatherSummaryList = new List<BadWeatherSummaryModel>();

            //if average cloud overcast is higher than 30 then use cloud overcast
            BadWeatherSummaryModel clearWeather;

            // since it is clear weather we consider the time of first city
            if (AverageCloudOvercast.WeatherElementTrueValue > 30)
            {
                clearWeather = new BadWeatherSummaryModel(WeatherType.Cloudy, this.assetManager, this.locationInfo.First().ApproximateArrivalTime);
            }
            else
            {
                clearWeather = new BadWeatherSummaryModel(WeatherType.Clear, this.assetManager, this.locationInfo.First().ApproximateArrivalTime);
            }

            badWeatherSummaryList.Add(clearWeather);

            //assign it to the view model variable to update it and raise an event;
            BadWeatherSummaryList = badWeatherSummaryList;
        }
        private void SetMaxtemperaturOnRoute()
        {
            //set this initially
            var maxTemp = new WeatherElement<short?>("Max Temperature");

            foreach (var locationInfoObj in this.locationInfo)
            {
                if (locationInfoObj == null || locationInfoObj.Weather == null)
                {
                    //TODO log
                    continue;
                }

                if (locationInfoObj.Weather.MaxTemperature == null)
                {
                    //TODO log
                    continue;
                }

                if (maxTemp.WeatherElementTrueValue < locationInfoObj.Weather.MaxTemperature || maxTemp.WeatherElementTrueValue == null)
                {
                    maxTemp.WeatherElementTrueValue = (short)locationInfoObj.Weather.MaxTemperature;
                }
            }

            MaxTemperature = maxTemp;
        }

        private void SetMintemperaturOnRoute()
        {
            //set this initially
            var minTemp = new WeatherElement<short?>("Min Temperature");
            minTemp.WeatherElementTrueValue = null;

            foreach (var locationInfoObj in this.locationInfo)
            {
                if (locationInfoObj == null || locationInfoObj.Weather == null)
                {
                    //TODO log
                    continue;
                }

                if (locationInfoObj.Weather.MinTemperature == null)
                {
                    //TODO log
                    continue;
                }

                if (minTemp.WeatherElementTrueValue > locationInfoObj.Weather.MinTemperature || minTemp.WeatherElementTrueValue == null)
                {
                    minTemp.WeatherElementTrueValue = (short)locationInfoObj.Weather.MinTemperature;
                }
            }
            MinTemperature = minTemp;
        }

        private void SetAvgCloudCoverPercent()
        {
            var cloudOverCastValue = new WeatherElement<ushort?>("Avg Cloud Overcast");
            cloudOverCastValue.WeatherElementTrueValue = Utils.GetAvgCloudCoverPercent (this.locationInfo);

            AverageCloudOvercast = cloudOverCastValue;
        }

        private void SetMaxSnowFallValue()
        {
            //set this initially
            var maxSnow = new WeatherElement<float?>("Max Snow (inches)");
             
            foreach (var locationInfoObj in this.locationInfo)
            {
                if (locationInfoObj == null || locationInfoObj.Weather == null)
                {
                    //TODO log
                    Debugger.Break();
                    continue;
                }

                if (locationInfoObj.Weather.SnowfallValue == null)
                {
                    //TODO log
                    continue;
                }

                if (maxSnow.WeatherElementTrueValue < locationInfoObj.Weather.SnowfallValue || maxSnow.WeatherElementTrueValue == null)
                {
                    maxSnow.WeatherElementTrueValue = (float)locationInfoObj.Weather.SnowfallValue;
                }
            }
            MaxSnowFall = maxSnow;
        }

        private void SetAvgPrecipitationChances()
        {
            uint? avgPercipitationChances = null;
            int validCityCount = 0;
            foreach (var locationInfoObj in this.locationInfo)
            {
                if (locationInfoObj == null || locationInfoObj.Weather == null)
                {
                    //TODO log
                    continue;
                }

                if (locationInfoObj.Weather.PrecipitationChances == null)
                {
                    //TODO log
                    continue;
                }

                if (avgPercipitationChances == null)
                {
                    avgPercipitationChances = 0;
                }

                ++validCityCount;
                avgPercipitationChances += (uint)locationInfoObj.Weather.PrecipitationChances;
            }
            if (validCityCount == 0)
            {
                //divisible by 0
                return;
            }
            var avgPercipitationValue = new WeatherElement<ushort?>("Avg Percipitation %");

            if (avgPercipitationChances != null)
            {
                avgPercipitationValue.WeatherElementTrueValue = (ushort)(avgPercipitationChances / validCityCount);
            }

            AveragePercipitationPercent = avgPercipitationValue;
        }

        # region View Model Bindings

        public List<BadWeatherSummaryModel> BadWeatherSummaryList { get; private set; }
        public WeatherElement<short?> MaxTemperature { get; private set; }

        public WeatherElement<short?> MinTemperature { get; private set; }

        public WeatherElement<ushort?> AverageCloudOvercast{ get;private set;}

        public WeatherElement<ushort?> AveragePercipitationPercent { get; private set; }

        public WeatherElement<float?> MaxSnowFall { get; set; }

        #endregion

    }
}