using System;
using System.Linq;
using RouteWeather.BusinessLogic.Model;
using RouteWeather.CommonLib;

namespace RouteWeather.BusinessLogic
{
    public class WeatherDetailSummaryInformation
    {
        private BadWeatherModel currentBadWeatherModel = null;
        public WeatherDetailSummaryInformation(bool isSelected)
        {
            IsSelected = isSelected;
        }

        public bool IsSelected { get; set; }

        public String StartCityName
        {
            get
            {
                if (this.currentBadWeatherModel == null || this.currentBadWeatherModel.WeatherConditionFirstCity == null)
                {
                    return null;
                }
                return this.currentBadWeatherModel.WeatherConditionFirstCity.City;
            }
        }

        public String Temperature { get; private set; }

        public String BadWeatherCitiesName { get; private set; }

        public String WeatherConditionIcon { get; private set; }

        /// <summary>
        /// This is required by the list box that does the scrolling
        /// </summary>
        public string ScrollIndex { get; private set; }

        public String WeatherCondition { get; private set; }

        public String TotalDistanceBadWeather { get; private set; }


        public void ExtractWeatherDetailInformation(BadWeatherModel selectedWeatherModel)
        {
            if (selectedWeatherModel == null)
            {
                throw new ArgumentNullException("selectedWeatherModel");
            }

            if (selectedWeatherModel.Locations.Count == 0)
            {
                //TODO log error
                return;
            }
            this.currentBadWeatherModel = selectedWeatherModel;

            this.Temperature = this.currentBadWeatherModel.Locations.First().Weather.Temperature + Utils.TemperatureImperialUnit;

            this.BadWeatherCitiesName =  this.currentBadWeatherModel.Locations.First().City + " to " +
                   this.currentBadWeatherModel.Locations.Last().City;

            this.ScrollIndex = Utils.GetDistanceInLocalCulture(
                        this.currentBadWeatherModel.WeatherConditionFirstCity.DistanceFromStartPoint).ToString();
            this.WeatherCondition = this.currentBadWeatherModel.WeatherCondition;

            var totalDistance = this.currentBadWeatherModel.Locations.Last().DistanceFromStartPoint -
                    this.currentBadWeatherModel.Locations.First().DistanceFromStartPoint;

            this.TotalDistanceBadWeather = Utils.GetDistanceInLocalCulture(totalDistance) + " " +
                                       Utils.GetDistanceUnits() + " until " + this.currentBadWeatherModel.Locations.Last().City;

            this.WeatherConditionIcon = "";
        }
    }
}