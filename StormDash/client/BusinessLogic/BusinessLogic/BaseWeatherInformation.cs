﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RouteWeather.BusinessLogic
{
    public abstract class BaseWeatherInformation
    {
        public abstract string UserSelectedFirstCityName
        {
            get;
            set;
        }
    }
}