using System;
using System.Collections.Generic;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.BusinessLogic.Model;
using WeatherApp.Communication;
using RouteWeather.Model;

namespace RouteWeather.BusinessLogic.DataOrganizer
{
    public class WeatherDataOrganizer : IWeatherDataOrganizer
    {
        private List<LocationInformation> weatherInfo;
        private Dictionary<string, BadWeatherModel> badWeatherModelsDictionary;

        public Dictionary<string, BadWeatherModel> BadWeatherDictionary
        {
            get 
            {
                return this.badWeatherModelsDictionary;
            }
        }

        private void DetermineWeatherCondtions()
        {
            var nullLocationInformation = 0;
            
            var badWeatherDictionary = new Dictionary<string,BadWeatherModel>();

            foreach (var locationInformation in this.weatherInfo)
            {
                if (locationInformation == null || locationInformation.Weather == null)
                {
                    //TODO log here
                    ++nullLocationInformation;
                    continue;
                }

                List<WeatherCondition> weatherConditionsForLocation = null;

                if (locationInformation.Weather.ThreeHourlyWeatherSummary != null)
                {
                    weatherConditionsForLocation =
                        locationInformation.Weather.ThreeHourlyWeatherSummary.WeatherConditions;
                }

                if (weatherConditionsForLocation != null && weatherConditionsForLocation.Count > 0)
                {
                    foreach (var weatherCondtion in weatherConditionsForLocation) {
                        BadWeatherModel existingModel = null;

                        var badWeatherKey = BadWeatherModel.GetBadWeatherKey (weatherCondtion.weatherIntensity, weatherCondtion.weatherType);

                        if (!badWeatherDictionary.TryGetValue (badWeatherKey, out existingModel)) 
                        {
                            // this weather condition is not present
                            existingModel = new BadWeatherModel (weatherCondtion.weatherType, weatherCondtion.weatherIntensity);
                            badWeatherDictionary.Add (badWeatherKey, existingModel);
                        }

                        existingModel.AddCity (locationInformation);  
                    }
                    
                } 
                else
                {
                    var weatherType = WeatherType.Clear;
                    if (locationInformation.Weather.CloudCoverPercentage > 30) 
                    {
                        // this is cloudy weather. For now just report cloudy
                        weatherType = WeatherType.Cloudy;
                    }

                    BadWeatherModel exisitingBadWeatherModel;
                    if (!badWeatherDictionary.TryGetValue (weatherType.ToString(), out exisitingBadWeatherModel)) 
                    {
                        // this weather condition is not present
                        exisitingBadWeatherModel = new BadWeatherModel (weatherType, null);
                        badWeatherDictionary.Add (weatherType.ToString(), exisitingBadWeatherModel);
                    }

                    exisitingBadWeatherModel.AddCity (locationInformation);
                }
                    
            }

            if (nullLocationInformation == this.weatherInfo.Count)
            {
                throw new ArgumentException("All the location are null");
            }

            this.badWeatherModelsDictionary = new Dictionary<string, BadWeatherModel> (badWeatherDictionary);
        }

        #region Interface Methods
 
        public void OrganizeWeatherConditions(List<LocationInformation> weatherInfoResponse, LocationDO source, LocationDO destination)
        {
            if (weatherInfoResponse == null)
            {
                throw new ArgumentNullException("weatherInfoResponse");
            }

            if (source == null) 
            {
                throw new ArgumentNullException("source");
            }

            if (destination == null) 
            {
                throw new ArgumentNullException("destination");
            }

            if (weatherInfoResponse.Count == 0)
            {
                throw new IndexOutOfRangeException("Empty weather data");
            }
            
            this.weatherInfo = weatherInfoResponse;
            DetermineWeatherCondtions();
        }

        public void Reset()
        {
            this.weatherInfo = null;
        }

        #endregion
    }
}
