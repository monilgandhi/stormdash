﻿using System.Collections.Generic;
using WeatherApp.Communication;
using RouteWeather.Model;
using RouteWeather.BusinessLogic.Model;

namespace RouteWeather.BusinessLogic.DataOrganizer
{
    public interface IWeatherDataOrganizer
    {
        Dictionary<string, BadWeatherModel> BadWeatherDictionary { get; }
        void OrganizeWeatherConditions(List<LocationInformation> weatherInfo, LocationDO source, LocationDO destination);
        void Reset();
    }
}
