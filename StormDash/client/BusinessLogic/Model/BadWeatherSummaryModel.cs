﻿using System;
using System.Text;
using CommunicationSchemas.Schemas.Protobuf;
using RouteWeather.CommonLib;

namespace RouteWeather.BusinessLogic.Model
{
    public struct BadWeatherSummaryModel
    {
        /// <summary>
        /// Constructor.
        /// Here this() is required because When a property is specified as an automatically implemented property, 
        /// a hidden backing field is automatically available for the property, and the accessors are implemented to 
        /// read from and write to that backing field.
        /// </summary>
        /// <param name="weatherCondition"> Weather condition</param>
        /// <param name="firstCityName"></param>
        /// <param name="distanceFromSource"></param>
        /// <param name="distanceOflastCityFromSource"></param>
        /// <param name="source"></param>
        /// <param name="weatherIcon"></param>
        public BadWeatherSummaryModel(
            WeatherType weatherCondition, 
            String firstCityName, 
            double distanceFromSource, 
            double distanceOflastCityFromSource,
            string source,
            ulong firstCityArrivalTime) : this()
        {
            //this is the check to make sure that all the arguments are provided except for clear and cloudy weather.
            if ((weatherCondition != WeatherType.Clear && weatherCondition != WeatherType.Cloudy) &&
                (String.IsNullOrEmpty(firstCityName) || 
                 String.IsNullOrEmpty(source) ||
                 distanceOflastCityFromSource.Equals(0)))
            {
                throw new ArgumentException("Weather is not clear nor cloudy and one of the arguments is missing");
            }

            // the distance from source should be 0 only if the weather is bad at first city
            if (source != firstCityName && distanceFromSource.Equals(0))
            {
                throw new ArgumentException("Distance from source is 0");
            }

            this.WeatherConditionSummary = "";
            this.SetSummaryTextAndIcon(
                weatherCondition,
                source,
                firstCityName,
                distanceOflastCityFromSource,
                distanceFromSource,
                firstCityArrivalTime);
        }

        public BadWeatherSummaryModel(WeatherType weathercondtion, ulong arrivalTime)
            : this(weathercondtion, "", 0,0,"", arrivalTime)
        {}

        private void SetSummaryTextAndIcon(
            WeatherType weatherCondition,
            String sourceCity,
            String firstCityName,
            double distanceOflastCityFromSource,
            double distanceFromSource,
            ulong firstCityTimeOfArrival)
        {
            var weatherConditionString = new StringBuilder();
            if (weatherCondition == WeatherType.Clear)
            {
                WeatherConditionSummary = "Weather is Clear";
                //BadWeatherIcon = assetManager.GetWeatherConditionIconPath(WeatherType.Clear, firstCityTimeOfArrival, (ushort?)0); 
                return;
            }

            // TODO temp
            if (weatherCondition == WeatherType.Cloudy)
            {
                WeatherConditionSummary = "Weather is Cloudy";
                //BadWeatherIcon = assetManager.GetWeatherConditionIconPath(WeatherType.Cloudy, firstCityTimeOfArrival, (ushort?)21); 
                return;
            }
            
            weatherConditionString.Append("at ");
            weatherConditionString.Append(firstCityName);
            weatherConditionString.Append(" ( ");
            if(distanceFromSource > 0)
            {
                weatherConditionString.Append(Utils.GetDistanceInLocalCulture(distanceFromSource));
                weatherConditionString.Append(" miles from ");
                weatherConditionString.Append(sourceCity);
            }
            else
            {
                weatherConditionString.Append("for ");
                weatherConditionString.Append(Utils.GetDistanceInLocalCulture(distanceOflastCityFromSource));
                weatherConditionString.Append(" miles");
            }
            weatherConditionString.Append(" )");
            WeatherConditionSummary = weatherConditionString.ToString();
            BadWeatherIcon = assetManager.GetWeatherConditionIconPath (weatherCondition, firstCityTimeOfArrival, (ushort?)21); 
        }

        public string WeatherConditionSummary { get; private set; }
        public string BadWeatherIcon { get; private set; }
    }
}
