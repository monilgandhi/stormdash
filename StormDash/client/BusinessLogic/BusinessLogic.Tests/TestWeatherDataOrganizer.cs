﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunicationSchemas.Schemas.Protobuf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RouteWeather.BusinessLogic.DataOrganizer;
using RouteWeather.BusinessLogic.Model;
using RouteWeather.Model;
using WeatherApp.Communication;

namespace RouteWeather.BusinessLogic.Tests
{
    [TestClass]
    public class TestWeatherDataOrganizer
    {
        private IWeatherDataOrganizer weatherOrganizer;
        private LocationDO source;
        private LocationDO destination;

        [TestInitialize]
        public void InitializeTest()
        {
            this.weatherOrganizer = new WeatherDataOrganizer();
            this.source = new LocationDO()
            {
                City = "Seattle",
                Latitude = 47.609f,
                Longitude = -122.33f,
                State = "WA",
                Zipcode = 98109
            };

            this.destination = new LocationDO()
            {
                City = "Redmond",
                Latitude = 47.6694f,
                Longitude = -122.124f,
                State = "WA",
                Zipcode = 98052
            };

        }

        private void Execute(List<LocationInformation> weatherInfo)
        {
            this.weatherOrganizer.OrganizeWeatherConditions(weatherInfo, this.source, this.destination);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullLocationData()
        {
            this.Execute(null);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void TestEmptyList()
        {
            this.Execute(new List<LocationInformation>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EmptyWeatherData()
        {
            Assert.IsNull(this.weatherOrganizer.BadWeatherDictionary);
            var weatherConditions = Utilities.ReadAndGenerateLocationTestData(@"TestFiles\EmptyWeatherLocation.xml");

            //because of xml serializer this is always assigned to an empty object
            weatherConditions[0].Weather = null;
            this.Execute(weatherConditions);
        }

        [TestMethod]
        public void OnlyCloudyWeather()
        {
            var weatherCondition =
                Utilities.ReadAndGenerateLocationTestData(@"TestFiles\OnlyCloudyWeather.xml");

            Assert.IsNull(this.weatherOrganizer.BadWeatherDictionary);
            this.Execute(weatherCondition);
            Assert.IsNotNull(this.weatherOrganizer.BadWeatherDictionary, "Bad weather cannot be null");
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.Count == 1, "Only cloudy weather");

            BadWeatherModel cloudyBadWeatherModel;
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.TryGetValue(WeatherType.Cloudy.ToString(), out cloudyBadWeatherModel), "Rain weather should be present");
            ValidateBadWeatherModel("Seattle", WeatherType.Cloudy, cloudyBadWeatherModel, 5);

        }

        [TestMethod]
        public void CloudyAndRainyEntireRoute()
        {
            var weatherCondition =
                Utilities.ReadAndGenerateLocationTestData(@"TestFiles\CloudyAndRainyEntireRoute.xml");

            Assert.IsNull(this.weatherOrganizer.BadWeatherDictionary);
            this.Execute(weatherCondition);
            Assert.IsNotNull(this.weatherOrganizer.BadWeatherDictionary, "Bad weather cannot be null");
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.Count == 1, "Weather will reported as Rain_Showers");

            BadWeatherModel rainBadWeatherModel;
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.TryGetValue(WeatherType.Rain_Showers + "|" + WeatherIntensity.Light, out rainBadWeatherModel), "Rain weather should be present");
            ValidateBadWeatherModel("Seattle", WeatherType.Rain_Showers, rainBadWeatherModel, 5);
        }

        [TestMethod]
        public void PartlyCloudyPartlyRain()
        {
            var weatherCondition =
                Utilities.ReadAndGenerateLocationTestData(@"TestFiles\PartlyCloudyAndPartlyRain.xml");

            Assert.IsNull(this.weatherOrganizer.BadWeatherDictionary);
            this.Execute(weatherCondition);
            Assert.IsNotNull(this.weatherOrganizer.BadWeatherDictionary, "Bad weather cannot be null");
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.Count == 2, "Cloudy and Rain_showers weather need to be present");

            BadWeatherModel cloudyBadWeatherModel, rainBadWeatherModel;
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.TryGetValue(WeatherType.Cloudy.ToString(), out cloudyBadWeatherModel), "Cloudy weather should be present");
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.TryGetValue(WeatherType.Rain_Showers + "|" + WeatherIntensity.Light, out rainBadWeatherModel), "Rain weather should be present");
            ValidateBadWeatherModel("Seattle", WeatherType.Cloudy, cloudyBadWeatherModel, 2);
            ValidateBadWeatherModel("Spokane", WeatherType.Rain_Showers, rainBadWeatherModel, 3);
        }

        [TestMethod]
        public void EmptyThreeHourWeather()
        {
            var weatherCondition =
                Utilities.ReadAndGenerateLocationTestData(@"TestFiles\EmptyThreeHourWeather.xml");

            Assert.IsNull(this.weatherOrganizer.BadWeatherDictionary);
            this.Execute(weatherCondition);
            Assert.IsNotNull(this.weatherOrganizer.BadWeatherDictionary, "Bad weather cannot be null");
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.Count == 1, "Clear weather need to be present");

            BadWeatherModel clearBadWeatherModel;
            Assert.IsTrue(this.weatherOrganizer.BadWeatherDictionary.TryGetValue(WeatherType.Clear.ToString(), out clearBadWeatherModel), "Cloudy weather should be present");
            ValidateBadWeatherModel("Seattle", WeatherType.Clear, clearBadWeatherModel, 5);
        }

        private void ValidateBadWeatherModel(string expectedFirstCity, WeatherType expectedWeatherType, BadWeatherModel objectBeValidated, int expectedLocationCount)
        {
            Assert.IsNotNull(objectBeValidated, "Value of the badweather object");
            Assert.AreEqual(expectedWeatherType, objectBeValidated.BadWeatherCondition, "Comparing the type of the badweather");
            Assert.IsNotNull(objectBeValidated.WeatherConditionFirstCity, "First city cannot be null");

            LocationInformation firstCity = objectBeValidated.WeatherConditionFirstCity;
            Assert.IsFalse(string.IsNullOrWhiteSpace(firstCity.City), "First city name cannot be null");

            Assert.AreEqual(expectedFirstCity, firstCity.City, "First city should be seattle");

            Assert.AreEqual(expectedLocationCount, objectBeValidated.Locations.Count, "Total number of cities should be 5");
        }
    }
}
