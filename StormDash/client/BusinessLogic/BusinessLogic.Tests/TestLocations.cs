﻿using System.Collections.Generic;
using System.Xml.Serialization;
using WeatherApp.Communication;

namespace RouteWeather.BusinessLogic.Tests
{
    [XmlRoot("Locations")]
    public class TestLocations
    {
        [XmlElement("LocationInformation")]
        public List<LocationInformation> locationInfo;
    }
}