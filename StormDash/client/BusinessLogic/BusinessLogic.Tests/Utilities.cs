﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using WeatherApp.Communication;

namespace RouteWeather.BusinessLogic.Tests
{
    static class Utilities
    {
        public static List<LocationInformation> ReadAndGenerateLocationTestData(string filePath)
        {
            if (String.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath))
            {
                throw new ArgumentException("filepath does not exists");
            }

            TestLocations testLocationObject = null;
            var xmlSerializer = new XmlSerializer(typeof(TestLocations));
            using (var fs = new FileStream(filePath, FileMode.Open))
            {
                var xmlReader = XmlReader.Create(fs);
                testLocationObject = xmlSerializer.Deserialize(xmlReader) as TestLocations;
            }
            
            if (testLocationObject == null)
            {
                return null;
            }
            return testLocationObject.locationInfo;
        }
    }
}
