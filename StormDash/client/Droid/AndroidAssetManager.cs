﻿using Xamarin.Forms;
using System.Text;
using RouteWeather.Pcl.Common;

namespace RouteWeather.Droid.Common
{
    public class AndroidAssetManager : AssetManager
    {
        public AndroidAssetManager()
        {
            otherIconBasePath = @"other";
            commonIconBasePath = @"common";
            weatherIconBasePath = @"weathercondition";
            pathSeparator = @"_";
        }
    }
}

