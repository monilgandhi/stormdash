﻿using System;
using System.ComponentModel;
using RouteWeather.Pcl.Renderer;
using RouteWeather.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.Graphics;

[assembly: ExportRenderer (typeof (HorizontalScrollingList), typeof (HorizontalScrollingListRenderer))]
namespace RouteWeather.Droid.Renderer
{
    public class HorizontalScrollingListRenderer : ScrollViewRenderer
    {
        private HorizontalScrollingList cList;
        private float selectionBoundXStart;
        private float selectionBoundXEnd;

        public override void Draw (Android.Graphics.Canvas canvas)
        {
            base.Draw (canvas);

            // determine the position of the lines
            var midpoint = this.Width/2;

            var paint = new Paint();
            paint.SetARGB(255, 200, 255, 0);
            paint.SetStyle(Paint.Style.Stroke);
            paint.StrokeWidth = 4;

            int[] locationinWindow = new int[2];
            int[] locationInScreen = new int[2];
            this.GetLocationInWindow (locationinWindow);
            this.GetLocationOnScreen (locationInScreen);

            selectionBoundXStart = this.GetX () + midpoint - 30;
            selectionBoundXEnd = this.GetX () + midpoint + 30; 
            // draw the top line
            // canvas.DrawLine(selectionBoundXStart, this.GetY() - 50, selectionBoundXEnd, this.GetY() - 50, paint);
        }

        protected override void OnElementChanged (VisualElementChangedEventArgs e)
        {
            base.OnElementChanged (e);

            e.NewElement.PropertyChanged += OnElementPropertyChanged;
            this.cList = e.NewElement as HorizontalScrollingList;
        }

        protected void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var childItem = GetChildAt (0);
            if (childItem != null)
            {
                childItem.HorizontalScrollBarEnabled = false;
            }
        }
    }
}


