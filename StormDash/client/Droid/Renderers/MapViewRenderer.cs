﻿using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.App;
using Android.Content;
using RouteWeather.Droid.Activities;
using RouteWeather.Pcl.Views;
using WeatherApp.Communication;
using System.Collections.Generic;
using Android.OS;

[assembly:ExportRenderer(typeof(RouteWeather.Pcl.Views.MapPage), typeof(RouteWeather.Droid.Renderer.MapViewRenderer))]

namespace RouteWeather.Droid.Renderer
{
    public class MapViewRenderer : PageRenderer
    {
        private MapPage mapPage;
        protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged (e);
            // get the view
            this.mapPage = e.NewElement as MapPage;

            // this is a ViewGroup - so should be able to load an AXML file and FindView<>
            var activity = this.Context as Activity;

            var mapActivity = new Intent (activity, typeof (MapViewActivity));
            mapActivity.PutExtra ("route", mapPage.mapRoute.RoutePoints);
            activity.StartActivity (mapActivity);
        }
    }
}