using RouteWeather.Droid.Services;
using RouteWeather.Pcl;
using RouteWeather.Pcl.Locator;
using RouteWeather.Services.DataStore;
using RouteWeather.Pcl.Common;
using RouteWeather.CommonLib;
using RouteWeather.Droid.Common;

namespace RouteWeather.Droid
{
    public class AndroidApplication : MainApp
    {
        protected override void RegisterPlatformSpecificServices()
        {
            CServiceLocator.Register<ISqliteConnection, DroidSqliteConnection>();
            //register the location service 
            var clientLocationService = new ClientLocationService();
            CServiceLocator.RegisterLocationServiceInstance(clientLocationService);
        }

        protected override void SetupResources()
        {
            AssetManager assetMgr;
            if (AssetManager.TryGetResourceDictionaryInstance(out assetMgr))
            {
                AndroidAssetManager androidAssetManager = assetMgr as AndroidAssetManager;
                if (assetMgr == null || androidAssetManager == null)
                {
                    assetMgr = new AndroidAssetManager();
                    AndroidApplication.Current.Resources.Remove("AssetMgr");
                    AndroidApplication.Current.Resources.Add("AssetMgr", assetMgr);
                }
                
                assetMgr.ColorTheme = Theme.Dark;
            }
        }
    }
}