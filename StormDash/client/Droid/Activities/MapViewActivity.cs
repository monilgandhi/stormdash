﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using System.Threading.Tasks;
using Android.Gms.Maps.Model;
using RouteWeather.CommonLib;
using Android.Graphics;
using RouteWeather.Pcl.Common;

namespace RouteWeather.Droid.Activities
{
    [Activity (Label = "MapViewActivity")]			
    public class MapViewActivity : Activity, IOnMapReadyCallback
    {
        private MapFragment mapFragment;
        private GoogleMap map;
        private string polyLine;

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            // Create your application here
            this.Initialize ();

            if (map == null) 
            {
                this.mapFragment.GetMapAsync (this);
            }

            this.polyLine = Intent.GetStringExtra ("route");

            if (string.IsNullOrWhiteSpace (polyLine)) 
            {
                System.Diagnostics.Debugger.Break ();
                // TODO error
            }

            SetContentView(Resource.Layout.MapView);
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            this.map = googleMap;
            this.SetupMapView ();
        }

        private void Initialize()
        {
            this.mapFragment = FragmentManager.FindFragmentByTag ("map") as MapFragment;
            if (this.mapFragment == null) 
            {
                var googleMapOptions = new GoogleMapOptions ()
                    .InvokeMapType (GoogleMap.MapTypeNormal)
                    .InvokeZoomControlsEnabled (false);

                var fragTx = FragmentManager.BeginTransaction ();
                this.mapFragment = MapFragment.NewInstance (googleMapOptions);
                fragTx.Add (Resource.Id.map, this.mapFragment, "map");
                fragTx.Commit ();
            }
        }

        private void SetupMapView()
        {
            if (map != null) 
            {
                var polylineOptions = new PolylineOptions();

                var latlngPoints = ConverttoLatLng ();
                var latlngArrayList = new Java.Util.ArrayList (latlngPoints);
                polylineOptions.AddAll (new Java.Util.ArrayList(latlngPoints));

                polylineOptions.InvokeColor (Color.Aqua.ToArgb ());
                polylineOptions.InvokeWidth (15);

                this.map.AddPolyline (polylineOptions);

                var latlngBoundsBuilder = LatLngBounds.InvokeBuilder ().Include (latlngPoints.First ());
                latlngBoundsBuilder.Include (latlngPoints.Last ());

                var cameraPosition = CameraUpdateFactory.NewLatLngBounds(latlngBoundsBuilder.Build(), 20);
                this.map.MoveCamera (cameraPosition);
            }
        }

        private List<LatLng> ConverttoLatLng()
        {
            var routeCoordinates = Utils.DecodeGooglePolyLines (this.polyLine);

            if (routeCoordinates == null || routeCoordinates.Count == 0) 
            {
                System.Diagnostics.Debugger.Break ();
                // TODO error
            }

            var result = new List<LatLng> ();
            foreach (var point in routeCoordinates) 
            {
                result.Add (new LatLng(point.Latitude, point.Longitude));
            }

            return result;
        }
    }
}