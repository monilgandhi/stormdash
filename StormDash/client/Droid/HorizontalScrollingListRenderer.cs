﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using RouteWeather.Pcl.Renderer;
using RouteWeather.Droid.Renderer;
using Android.Graphics.Drawables.Shapes;
using System.Text;

[assembly: ExportRendererAttribute(typeof(HorizontalScrollingList), typeof(HorizontalScrollingListRenderer))]
namespace RouteWeather.Droid.Renderer
{
    public class HorizontalScrollingListRenderer : ScrollViewRenderer
    {
        public override void Draw (Android.Graphics.Canvas canvas)
        {
            HorizontalScrollingList hsl = (HorizontalScrollingList)this.Element;

            var sb = new StringBuilder ();
            foreach (var text in hsl.ItemSource) 
            {
                sb.Append (text);
                sb.Append ("\t");
            }

            var label = new Label () 
            {
                Text = sb.ToString ()
            };

            hsl.Content = label;
        }
    }
}

