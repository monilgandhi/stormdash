﻿using System;
using System.IO;
using RouteWeather.Services.DataStore;
using SQLite;

namespace RouteWeather.Droid
{
    public class DroidSqliteConnection : ISqliteConnection
    {
        public SQLiteAsyncConnection GetConnection(string databaseName)
        {
            var sqliteFilename = databaseName + ".db3";
            string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, sqliteFilename);
            // Create the connection
            var conn = new SQLiteAsyncConnection(path);
            // Return the database connection
            return conn;
        }
    }
}

