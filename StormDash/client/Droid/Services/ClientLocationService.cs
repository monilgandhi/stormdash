﻿using System;
using System.Linq;
using Android.Locations;
using System.Threading.Tasks;
using RouteWeather.Pcl.Location;
using Android.App;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Gms.Common;
using RouteWeather.Model;

namespace RouteWeather.Droid.Services
{
    public class ClientLocationService: IClientLocationService
    {
        private GoogleApiClient apiClient;
        private bool isGoogleApiInitialized;
        public ClientLocationService()
        {
            InitializeGoogleApiClient ();
        }

        public async Task<LocationDO> GetCurrentLocation()
        {
            if (!this.isGoogleApiInitialized)
            {
                // try to initialzie again
                this.InitializeGoogleApiClient();    
            }

            if (!this.isGoogleApiInitialized)
            {
                // TODO log warning
                return null;
            }

            Criteria locationCriteria = new Criteria();
            locationCriteria.Accuracy = Accuracy.Fine;
            locationCriteria.PowerRequirement = Power.High;

            if (!this.apiClient.IsConnected) 
            {
                // TODO error
                return null;
            }

            var googlePlayServiceLocation = LocationServices.FusedLocationApi.GetLastLocation (this.apiClient);

            if (googlePlayServiceLocation == null) 
            {
                return null;
            }

            Address address = await GetAddressofLocation (googlePlayServiceLocation);

            if (string.IsNullOrWhiteSpace (address?.Locality))
            {
                // TODO error
                return null;
            }

            uint zip = 0;
            uint.TryParse(address.PostalCode, out zip);

            return new LocationDO () 
            {
                Latitude = (float)address.Latitude,
                Longitude = (float)address.Longitude,
                City = address.Locality,
                State = address.AdminArea,
                Zipcode = zip
            };
        }

        private async Task<Address> GetAddressofLocation(Location androidLocationObject)
        {
            Geocoder geocdr = new Geocoder (Application.Context);
            var addressList = await geocdr.GetFromLocationAsync(androidLocationObject.Latitude, androidLocationObject.Longitude, 5);
            if (addressList == null || addressList.Count == 0)
            {
                return null;
            }

            // return the first one
            return addressList.First();
        }

        private void InitializeGoogleApiClient()
        {
            if (!IsGooglePlayServicesInstalled ())
            {
                this.isGoogleApiInitialized = false;
            }

            // TODO check for gogle play services
            this.apiClient = new GoogleApiClient.Builder(Application.Context).AddApi(LocationServices.API).Build();
            try
            {
                this.apiClient.Connect();
                this.isGoogleApiInitialized = true;
            }
            catch (Exception)
            {
                // TODO error
                this.isGoogleApiInitialized = false;
            }
        }

        private bool IsGooglePlayServicesInstalled()
        {
            int queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable (Application.Context);
            if (queryResult != ConnectionResult.Success) 
            {
                return false;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError (queryResult))
            {
                string errorString = GoogleApiAvailability.Instance.GetErrorString (queryResult);
                // TODO Show error dialog to let user debug google play services
            }

            return true;
        }
    }
}

