﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace RouteWeather.Droid
{
    [Activity (Label = "Storm Dash", Icon = "@drawable/icon", ConfigurationChanges = ConfigChanges.ScreenSize, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			LoadApplication (new AndroidApplication());
		} 
	}
}