using RouteWeather.iOS.Services;
using RouteWeather.Pcl;
using RouteWeather.Pcl.Locator;
using RouteWeather.Services.DataStore;

namespace RouteWeather.iOS
{
    public class iOSApplication : MainApp
    {
        protected override void RegisterPlatformSpecificServices()
        {
            CServiceLocator.Register<ISqliteConnection, iOSSqliteConnection>();
        }
    }
}